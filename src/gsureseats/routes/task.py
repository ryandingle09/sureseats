import datetime
import json
import logging
import os
import requests
import sys

from uuid import UUID

from flask import abort, jsonify, redirect, request, url_for, Blueprint

from google.appengine.api import memcache, taskqueue
from google.appengine.api.images import get_serving_url
from google.appengine.ext import deferred, ndb

from gsureseats import tx
from gsureseats.admin import notify_admin
from gsureseats.feeds import sureseats, theaters
from gsureseats.heuristics import transform_movies, transform_schedules
from gsureseats.models import Event, Feed, Listener, Movie, Theater, TheaterOrganization
from gsureseats.orgs import AYALA_MALLS, GLOBE, ROTTEN_TOMATOES
from gsureseats.settings import NOTIFICATION_RETRY_THRESHOLD
from gsureseats.tasks import activate_movies_nowshowing, deactivate_movies_comingsoon, expire_movies, unexpire_movies, unexpire_movies_comingsoon



log = logging.getLogger(__name__)
task = Blueprint('task', __name__, template_folder='task_templates')

THEATERORG_UUID = {str(AYALA_MALLS): AYALA_MALLS}


####################
#
# Task Payload & Task Group Entities
#
####################

class HeuristicsTaskPayload(ndb.Model):
    payload = ndb.PickleProperty()


class TaskGroupControl(ndb.Model):
    feed_count = ndb.IntegerProperty()
    completion_count = ndb.IntegerProperty(default=0)
    payloads = ndb.PickleProperty()

    @classmethod
    def launch_singleton_taskgroup(cls, org_uuid, movie_dictionary_list):
        ctrl = cls()
        ctrl.payloads = {}
        ctrl.pack_payload(str(org_uuid), movie_dictionary_list)
        ctrl.launch_heuristics()

    def incr_and_check_completion(self):
        self.completion_count += 1
        self.put()

        if self.completion_count >= self.feed_count:
            self.launch_heuristics()

    def pack_payload(self, org_id, movie_dictionary_list):
        self.payloads[org_id] = movie_dictionary_list
        self.put()

    def launch_heuristics(self):
        taskqueue.add(url=url_for('.run_movies_heuristics'), params={'ctrl_key': self.key.urlsafe()})


@ndb.transactional
def pack_taskgroup_payload(ctrl_key, org_uuid, payload):
    ctrl = ndb.Key(urlsafe=ctrl_key).get(use_cache=False, use_memcache=False)
    ctrl.pack_payload(str(org_uuid), payload)
    ctrl.incr_and_check_completion()

def create_task_payload(p):
    payload = HeuristicsTaskPayload(payload=p)
    payload.put()

    return payload.key.urlsafe()

def drop_task_payload(k):
    ndb.Key(urlsafe=k).delete()

def get_task_payload(k):
    p = ndb.Key(urlsafe=k).get()

    return p.payload if p else None


###############
#
# Error Handlers
#
###############

@task.errorhandler(Exception)
def handle_task_error(e):
    logging.exception(e)
    retry_count = -1

    if 'X-AppEngine-TaskRetryCount' in request.headers:
        retry_count = int(request.headers['X-AppEngine-TaskRetryCount'])

    if retry_count == -1 or retry_count >= NOTIFICATION_RETRY_THRESHOLD:
        notify_admin("backend task", e, sys.exc_info())

    res = jsonify(message='Task error', details=e.message)
    res.status_code = 500

    return res

@task.route('/')
def task_listing():
    return jsonify([])


###############
#
# Movie Tasks via CRON
#
###############

@task.route('/fetch/movies')
def fetch_all_movie_feeds():
    ctrl = TaskGroupControl()
    ctrl.feed_count = 2
    ctrl.payloads = {}
    ctrl.put()
    ctrl_key = ctrl.key.urlsafe()

    taskqueue.add(url=url_for('.fetch_movies_sureseats'), params=dict(ctrl_key=ctrl_key))
    taskqueue.add(url=url_for('.fetch_movies_advisory_ratings'), params=dict(ctrl_key=ctrl_key))

    return jsonify(status='ok')

@task.route('/fetch/movies/ayala', methods=['POST'])
def fetch_movies_sureseats():
    log.debug("fetch_movies_sureseats, start fetching Ayala Malls movies via SureSeats...")

    ctrl_key = request.values['ctrl_key']
    #compare_movies()
    movies_nowshowing = sureseats.read_movies_now_showing()
    movies_comingsoon = sureseats.read_movies_coming_soon()
    movies = movies_nowshowing + movies_comingsoon

    log.info('movies_nowshowing: {}'.format(movies_nowshowing))
    log.info('movies_comingsoon: {}'.format(movies_comingsoon))

    pack_taskgroup_payload(ctrl_key, AYALA_MALLS, movies)

    return jsonify(status='ok')

@task.route('/fetch/movies/advisory-ratings', methods=['POST'])
def fetch_movies_advisory_ratings():
    ctrl_key = request.values['ctrl_key']
    advisory_ratings = sureseats.read_advisory_ratings_for_movies() # in the future, load this from a different source.

    # FIXME: even if this is coming from Sureseats and should be tagged as org AYALA_MALLS, we can't (because of the semantics of TaskGroupControl).
    pack_taskgroup_payload(ctrl_key, GLOBE, advisory_ratings)

    return jsonify(status='ok')

@task.route('/heuristics/movies', methods=['POST'])
def run_movies_heuristics():
    ctrl_key = request.values['ctrl_key']
    ctrl = ndb.Key(urlsafe=ctrl_key).get()

    if ctrl:
        transform_movies(ctrl.payloads)
        ctrl.key.delete()
        refresh_movies()

        log.info('transform_movies_payload: {}'.format(ctrl.payloads))

        return jsonify(status='ok')
    return jsonify(status='skipped')


###############
#
# Theater & Cinema Tasks via CRON
#
###############

@task.route('/fetch/theaters/ayala')
def fetch_theaters_ayala():
    log.debug("fetch_theaters_ayala, start fetching Ayala Malls theaters via SureSeats...")

    deferred.defer(theaters.read_theaters, _queue='theatersqueue')

    return jsonify(status='ok')

@task.route('/fetch/theaters/ayala2')
def fetch_theaters_ayala2():
    log.debug("fetch_theaters_ayala2, start fetching Ayala Malls theaters via SureSeats...")

    deferred.defer(theaters.read_theaters2, _queue='theatersqueue')

    return jsonify(status='ok')


###############
#
# Schedule Tasks via CRON
#
###############

@task.route('/fetch/schedules/ayala')
def fetch_schedules_sureseats():
    log.debug("fetch_schedules_sureseats, start fetching Ayala Malls schedules via SureSeats...")

    scheds = sureseats.read_schedules()
    payload = create_task_payload(dict(schedule_dictionary_list=scheds))

    taskqueue.add(url=url_for('.run_schedules_heuristics'), queue_name='schedulesqueue', params={'payload': payload})

    return jsonify(status='ok')

@task.route('/fetch/schedules2/ayala')
def fetch_schedules2_sureseats():
    log.debug("fetch_schedules2_sureseats, start fetching Ayala Malls schedules2 via SureSeats...")

    scheds = sureseats.read_schedules2()
    payload = create_task_payload(dict(schedule_dictionary_list=scheds))

    taskqueue.add(url=url_for('.run_schedules_heuristics'), queue_name='schedulesqueue', params={'payload': payload})

    return jsonify(status='ok')

@task.route('/heuristics/schedules', methods=['POST'])
def run_schedules_heuristics():
    payload_key = request.values['payload']
    payload = get_task_payload(payload_key)

    if payload:
        transform_schedules(**payload)
        drop_task_payload(payload_key)
        refresh_movies(queue_name='schedulesqueue')

    return jsonify(status='ok')


###############
#
# Refresh Movies Tasks via CRON
#
###############

@task.route('/refresh-movies')
def refresh_movies(queue_name='default'):
    activate_movies_nowshowing() # ensure that all now showing movies are marked is_showing = True.
    deactivate_movies_comingsoon() # ensure that all coming soon movies are marked is_showing = False.

    taskqueue.add(url=url_for('.refresh_movies_expire'), queue_name=queue_name)
    taskqueue.add(url=url_for('.refresh_movies_unexpire'), queue_name=queue_name)
    taskqueue.add(url=url_for('.refresh_movies_unexpire_comingsoon'), queue_name=queue_name)

    return jsonify(status='ok')

@task.route('/refresh-movies/expire', methods=['POST'])
def refresh_movies_expire():
    expire_movies()

    return jsonify(status='ok')

@task.route('/refresh-movies/unexpire', methods=['POST'])
def refresh_movies_unexpire():
    unexpire_movies()

    return jsonify(status='ok')

@task.route('/refresh-movies/unexpire-coming-soon', methods=['POST'])
def refresh_movies_unexpire_comingsoon():
    unexpire_movies_comingsoon()

    return jsonify(status='ok')


###############
#
# Transaction Tasks
#
###############

@task.route('/process_tx', methods=['POST'])
def process_tx():
    log.debug("Got request: %s" % request.values)
    state = tx.next_state(request.values)
    log.debug('TX process exit-----------------------------------')

    return jsonify(state=tx.to_state_str(state))

@task.route('/reap_tx', methods=['POST'])
def reap_tx():
   device_id = request.values['device_id']
   tx_id = request.values['tx_id']
   log.debug('Reaping outstanding transactions which have exceeded their timeout values: tx %s via device %s' % (tx_id, device_id))
   id_reaped = (device_id, tx_id)
   tx.reap_stale_tx(device_id, tx_id)

   return jsonify(reaped=id_reaped)

@task.route('/requery_tx', methods=['POST'])
def requery_tx():
    device_id = request.values['device_id']
    tx_id = request.values['tx_id']

    log.info("requery_tx, requery transactions, tx %s via device %s" % (tx_id, device_id))

    id_requery = (device_id, tx_id)
    tx.requery_stale_tx(device_id, tx_id)

    return jsonify(requery=id_requery)

@task.route('/callback/trigger_listeners', methods=['GET', 'POST'])
def trigger_callback_listeners():
    channel_name = request.values['channel_name']
    log.info("Channel name: {}".format(channel_name))
    version = os.environ['CURRENT_VERSION_ID'].split('.')
    major_version = version[0]
    listeners = Listener.get_listeners_by_version(channel_name, major_version)
    log.info("Listeners: {}".format(listeners))
    outstanding = Event.get_events(channel_name)

    for l in listeners:
        log.info("Processing listener callback: {} for channel: {}".format(l.callback, outstanding))
        l.callback(outstanding)

    ndb.put_multi(outstanding)

    return jsonify(status='ok')


###############
#
# Test Case Tasks
#
###############

@task.route('/process_testcase', methods=['POST'])
def process_testcase():
    log.info("Test Case!, process_testcase...")

    movies = Movie.query(Movie.is_expired==False, Movie.is_showing==True).fetch()

    for movie in movies:
        log.info("Movie, canonical_title, %s..." % movie.canonical_title)

    return jsonify(status='ok')
