from .admin import web
from .api import api
from .callback import cb as callback
from .task import task
from .testcase import testcase
