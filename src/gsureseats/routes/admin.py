import ast
import calendar
import datetime
import json
import logging
import pickle
import pytz
import StringIO

from collections import OrderedDict
from decimal import Decimal
from operator import attrgetter
from uuid import uuid4 as uuid_gen

from flask import Blueprint, jsonify, render_template, flash, redirect, url_for, request, abort, make_response
from flask.ext.bootstrap import Bootstrap

from google.appengine.ext import ndb
from google.appengine.api import memcache

from gsureseats.admin.forms import (CinemaForm, ClientForm, ClientEditForm, EditReservationForm, FeedForm, FileUploadForm,
        MovieForm, PesoPayPaymentSettingsForm, PesoPayPaymentSettingsEditForm, PesoPayPaymentSettingsCinemaForm, ScheduleForm,
        ScheduleSlotForm, SystemSettingsForm, TheaterForm, TheaterOrgForm)
from gsureseats.heuristics import movies
from gsureseats.models import (Client, ClientInitiatedPaymentSettings, ClientInitiatedPaymentSettingsCinema, Device, Feed,
        FeedCorrelation, Movie, ReservationTransaction, Schedule, ScheduleTimeSlot, Theater, TheaterOrganization)
from gsureseats.orgs import AYALA_MALLS, GLOBE, ROTTEN_TOMATOES
from gsureseats.settings import SystemSettings
from gsureseats.tx import state, to_state_str, update
from gsureseats.util import admin, id_encoder, parse_cast


tz = pytz.timezone('Asia/Manila')
log = logging.getLogger(__name__)
web = Blueprint('admin_web', __name__, template_folder='admin_templates')

THEATER_ORGANIZATIONS = {str(AYALA_MALLS): 'Ayala Malls'}
FEEDS = {str(AYALA_MALLS): 'Sureseats', str(GLOBE): 'Globe', str(ROTTEN_TOMATOES): 'Rotten Tomatoes'}

ALLOWED_EXTENSIONS = set(['csv', 'txt'])
ACTIVE_STATES = [state.TX_START, state.TX_PREPARE, state.TX_STARTED, state.TX_RESERVATION_HOLD, state.TX_RESERVATION_OK,
        state.TX_PAYMENT_HOLD, state.TX_CLIENT_PAYMENT_HOLD, state.TX_GENERATE_TICKET, state.TX_FINALIZE, state.TX_FINALIZE_HOLD,
        state.TX_PREPARE_ERROR, state.TX_RESERVATION_ERROR, state.TX_PAYMENT_ERROR]


@web.route('/')
def admin_index():
    return render_template('index.html')


###############
#
# Theaters CMS/Dashboard
#
###############

@web.route('/theaterorganizations/')
def theaterorg_index():
    theaterorgs = dict([(t.key.id(), t.name) for t in TheaterOrganization.query().fetch(projection=['name'])])

    for org_uuid in THEATER_ORGANIZATIONS:
        if org_uuid not in theaterorgs:
            theaterorgs[org_uuid] = THEATER_ORGANIZATIONS[org_uuid]

    ordered_theaterorgs = OrderedDict(sorted(theaterorgs.items(), key=lambda(k,v):(v,k)))

    return render_template('theaterorg_index.html', theaterorgs=ordered_theaterorgs)

@web.route('/theaterorganizations/<org_id>/', methods=['GET', 'POST'])
def theaterorg_update_and_show(org_id):
    theaterorg_key = ndb.Key(TheaterOrganization, org_id)
    theaterorg = theaterorg_key.get()

    if theaterorg is None:
        theaterorg = TheaterOrganization(name=THEATER_ORGANIZATIONS[org_id])
        theaterorg.key = ndb.Key(TheaterOrganization, org_id)
        theaterorg.put()

    form = TheaterOrgForm(obj=theaterorg)
    theaters = Theater.query(ancestor=theaterorg_key).fetch(projection=['name'])

    if request.method == 'POST' and form.validate_on_submit():
        convenience_fees = {}

        convenience_fees['credit-card'] = form.convenience_fee_creditcard.data if form.convenience_fee_creditcard.data else '0.00'
        convenience_fees['mpass'] = form.convenience_fee_mpass.data if form.convenience_fee_mpass.data else '0.00'
        convenience_fees['reserve'] = form.convenience_fee_reserve.data if form.convenience_fee_reserve.data else '0.00'

        snackbar_fees = {}
        snackbar_fees['snackbar-fee'] = form.convenience_fee_snackbar.data if form.convenience_fee_snackbar.data else '0.00'
        theaterorg.snackbar_fees_type = form.convenience_fee_snackbar_type.data

        theaterorg.name = form.name.data
        theaterorg.pubkey = form.pubkey.data
        theaterorg.convenience_fees = convenience_fees
        theaterorg.snackbar_fees = snackbar_fees
        theaterorg.ticket_type = form.ticket_type.data
        theaterorg.sort_priority = form.sort_priority.data
        theaterorg.is_published = form.is_published.data
        theaterorg.is_active = form.is_active.data
        theaterorg.put()

        flash("Success! Updated Theater Organization!")

        return redirect(url_for('.theaterorg_update_and_show', org_id=org_id))

    if theaterorg.convenience_fees:
        form.convenience_fee_creditcard.data = theaterorg.convenience_fees['credit-card'] if 'credit-card' in theaterorg.convenience_fees else ''
        form.convenience_fee_mpass.data = theaterorg.convenience_fees['mpass'] if 'mpass' in theaterorg.convenience_fees else ''
        form.convenience_fee_reserve.data = theaterorg.convenience_fees['reserve'] if 'reserve' in theaterorg.convenience_fees else ''

    if theaterorg.snackbar_fees:
        form.convenience_fee_snackbar.data = theaterorg.snackbar_fees['snackbar-fee'] if 'snackbar-fee' in theaterorg.snackbar_fees else ''
        form.convenience_fee_snackbar_type.data = theaterorg.snackbar_fees_type

    return render_template('theaterorg_show.html', org_id=org_id, theaterorg=theaterorg, form=form, theaters=theaters)

@web.route('/theaters/')
def theater_index():
    theaterorg_key = ndb.Key(TheaterOrganization, str(AYALA_MALLS))
    theaters = Theater.query(ancestor=theaterorg_key).fetch()

    return render_template('theater_index.html', theaters=theaters)

@web.route('/theaters/<theater_id>/', methods=['GET', 'POST'])
def theater_update_and_show(theater_id):
    theater = admin.get_theater(theater_id)
    form = TheaterForm(obj=theater)

    if request.method == 'POST' and form.validate_on_submit():
        form.populate_obj(theater)

        if form.latitude.data and form.longitude.data:
            theater.location = ndb.GeoPt(lat=form.latitude.data, lon=form.longitude.data)
        else:
            theater.location = None

        theater.put()

        if theater.allow_buy_ticket or theater.allow_reserve_ticket:
            admin.update_org_client_control(theater_id, theater.key.parent().id(), theater.allow_buy_ticket, theater.allow_reserve_ticket)

        flash("Success! Updated Theater!")

    if theater.location:
        form.longitude.data = theater.location.lon
        form.latitude.data = theater.location.lat

    today = datetime.date.today()
    schedules = Schedule.query(Schedule.show_date>=today, ancestor=theater.key).order(-Schedule.show_date).fetch(projection=['cinema', 'show_date'])

    return render_template('theaters_show.html', form=form, theater=theater, schedules=schedules, theater_id=theater_id)

@web.route('/theaters/<theater_id>/cinemas/', methods=['GET','POST'])
def cinema_new(theater_id):
    form = CinemaForm()
    theater = admin.get_theater(theater_id)

    if request.method == 'POST' and form.validate_on_submit() and admin.check_cinema(form.name.data, theater_id):
        admin.create_cinema_params(form, theater_id)

        flash("Success! Created New Cinema!")

        return redirect(url_for('.theater_update_and_show', theater_id=theater_id))
    elif not admin.check_cinema(form.name.data, theater_id):
        form.errors['generic'] = 'Error! Cinema Already Exists!'

    return render_template('cinema_new.html', form=form, theater_id=theater_id, theater_name=theater.name)

@web.route('/theaters/<theater_id>/cinemas/<cinema_name>/', methods=['GET','POST'])
def cinema_update_and_show(theater_id, cinema_name):
    theater = admin.get_theater(theater_id)
    cinema = admin.get_cinema_entity(theater, cinema_name)
    seatmap = ','.join(str(x) for x in cinema.seat_map)
    seat_rows = [','.join(row) for row in cinema.seat_map]
    form = CinemaForm(obj=cinema)
    form.seat_map.choices = zip(seat_rows, seat_rows)

    if request.method == 'POST' and form.validate_on_submit():
        form.populate_obj(cinema)

        cinema.seat_map = admin.add_seats(form.addseats.data, form.seat_map.data)
        cinema.seat_count = str(len(filter(lambda seat: seat != '', sum(seat_map, []))))
        cinema.location = ndb.GeoPt(form.latitude.data, form.longitude.data) if form.latitude.data and form.longitude.data else None

        theater.put()

        return redirect(url_for('.cinema_update_and_show', theater_id=theater_id, cinema_name=cinema.name))
    else:
        today = datetime.date.today()
        schedules = Schedule.query(Schedule.show_date>=today, Schedule.cinema==cinema_name,
                ancestor=theater.key).order(-Schedule.show_date).fetch(projection=['show_date'])

        form.longitude.data = cinema.location.lon if cinema.location else None
        form.latitude.data = cinema.location.lat if cinema.location else None
        form.seat_map.data = seat_rows

    return render_template('cinema_show.html', form=form, theater_id=theater_id, theater_name=theater.name,
            cinema=cinema, schedules=schedules, seatmap=seatmap)


###############
#
# Movies, Feeds, and FeedCorrelations CMS/Dashboard
#
###############

@web.route('/movies/')
def movie_index():
    cursor = ndb.Cursor(urlsafe=request.args.get('cursor'))
    queryset = Movie.query(Movie.is_active==True).order(-Movie.release_date)
    movies_page, next_cursor, more = queryset.fetch_page(100, start_cursor=cursor)

    return render_template('movie_index.html', movies_page=movies_page, next_cursor=next_cursor, more=more)

@web.route('/movies/in-active/')
def movie_inactive_index():
    cursor = ndb.Cursor(urlsafe=request.args.get('cursor'))
    queryset = Movie.query(Movie.is_active==False).order(-Movie.release_date)
    movies_page, next_cursor, more = queryset.fetch_page(100, start_cursor=cursor)

    return render_template('movie_index.html', movies_page=movies_page, next_cursor=next_cursor, more=more)

@web.route('/movies/new/', methods=['GET', 'POST'])
def movie_new():
    form = MovieForm()

    if request.method == 'POST' and form.validate_on_submit():
        admin.create_movie(form)

        flash("Success! Created New Movie!")

        return redirect(url_for('.movie_index'))

    return render_template('movie_new.html', form=form)

@web.route('/movies/<movie_id>/', methods=['GET', 'POST'])
def movie_update_and_show(movie_id):
    movie_key = ndb.Key(Movie, movie_id)
    movie = movie_key.get()
    form = MovieForm(obj=movie)

    sureseats_key = ndb.Key(Feed, str(AYALA_MALLS))
    sureseat_mappings = filter(lambda corr: corr.org_key == sureseats_key, movie.movie_correlation)
    cast = unicode(','.join(x for x in movie.cast))
    correlation_titles = movies.get_all_correlation_titles(form.canonical_title.data)

    if request.method == 'POST' and form.validate_on_submit() and admin.check_movie(movie.correlation_title, correlation_titles, movie_key):
        form.populate_obj(movie)

        if correlation_titles != movie.correlation_title:
            movie.correlation_title = correlation_titles

        movie.canonical_title = movies.get_canonical_title(form.canonical_title.data)
        movie.cast = parse_cast(form.cast_list.data)
        movie.put()

        flash("Success! Updated Movie Information!")

        return redirect(url_for('.movie_update_and_show', movie_id=movie.key.id()))
    else:
        form.cast_list.data = cast

        if not admin.check_movie(movie.correlation_title, correlation_titles, movie_key):
            form.errors['generic'] = 'Error! The movie already exists!'

    today = datetime.date.today()
    schedules = Schedule.query(Schedule.slots.movie==movie_key, Schedule.show_date>=today).order(-Schedule.show_date).fetch(projection=['show_date', 'cinema'])
    schedule_parents = set([s.key.parent() for s in schedules])
    theater_names = {}
    theaters = ndb.get_multi(schedule_parents)

    for t_k, t in zip(schedule_parents, theaters):
        theater_names[t_k] = t.org_theater_code

    schedule_theaters = [theater_names[s.key.parent()] for s in schedules]
    schedule_pairs = zip(schedules, schedule_theaters)

    return render_template('movie_show.html', form=form, movie_id=movie_id, movie=movie, cast=cast,
            schedules=schedule_pairs, sureseat_mappings=sureseat_mappings)

@web.route('/feeds/')
def feed_index():
    feeds = dict([(f.key.id(), f.name) for f in Feed.query().fetch(projection=['name'])])

    for f in FEEDS:
        if f not in feeds:
            feeds[f] = FEEDS[f]

    return render_template('feed_index.html', feeds=feeds)

@web.route('/feeds/<feed_id>/', methods=['GET', 'POST'])
def feed_update_and_show(feed_id):
    feed_key = ndb.Key(Feed, feed_id)
    feed = feed_key.get()

    if feed is None:
        feed = Feed(name=FEEDS[feed_id])
        feed.key = ndb.Key(Feed, feed_id)
        feed.image_density = 'mdpi'
        feed.put()

    form = FeedForm(obj=feed)

    if request.method == 'POST' and form.validate_on_submit():
        form.populate_obj(feed)
        feed.put()

        flash("Success! Updated Feed Information!")

        return redirect(url_for('.feed_update_and_show', feed_id=feed_id))

    return render_template('feed_show.html', form=form, feed_id=feed_id, feed=feed)

@web.route('/movies/<movie_id>/feed_correlations/<feed_name>~<corr_id>~<variant>/delete', methods=['POST'])
def feed_correlation_delete(movie_id, feed_name, corr_id, variant):
    feed_ids = {'sureseats': str(AYALA_MALLS), 'globe': str(GLOBE), 'rottentomatoes': str(ROTTEN_TOMATOES)}
    movie_key = ndb.Key(Movie, movie_id)
    movie = movie_key.get()
    feed_key = ndb.Key(Feed, feed_ids[feed_name])
    feed = feed_key.get()
    feed_correlations = movie.movie_correlation

    if variant == 'None':
        variant = None

    if variant and variant is not None:
        variant = variant.replace('_', '/')

    corr = FeedCorrelation(movie_id=corr_id, org_key=feed_key, variant=variant)

    if request.method == 'POST':
        movie.movie_correlation.remove(corr)
        movie.put()

        flash("Success! Deleted Feed Correlation %s, %s!" % (feed.name, corr_id))

    return redirect(url_for('.movie_update_and_show', movie_id=movie_id))


###############
#
# Schedules CMS/Dashboard
#
###############

@web.route('/theaters/<theater_id>/schedules/<schedule_id>/')
def schedule_show(theater_id, schedule_id):
    theater = admin.get_theater(theater_id)
    schedule_key = ndb.Key(Schedule, schedule_id, parent=theater.key)
    schedule = schedule_key.get()

    return render_template('schedule_show.html', schedule_id=schedule_id, theater_id=theater_id,
            theater_name=theater.name, schedule=schedule)


###############
#
# System Settings, Client Initiated Settings, and Clients CMS/Dashboard
#
###############

@web.route('/settings/', methods=['GET', 'POST'])
def system_knobs():
    settings = SystemSettings.get_settings()
    form = SystemSettingsForm(obj=settings)

    if request.method == 'POST' and form.validate_on_submit():
        form.populate_obj(settings)
        settings.put()

        flash("Success! Updated System Settings!")

        return redirect(url_for('.system_knobs'))

    return render_template('knobs.html', settings=settings, form=form)

def get_paymentsettings_theaters(payment_identifier):
    payment_settings = ClientInitiatedPaymentSettings.query(ClientInitiatedPaymentSettings.payment_identifier==payment_identifier).fetch(keys_only=True)

    return payment_settings

def get_active_and_allowebuyticket_theaters():
    theaters = Theater.query(Theater.is_active==True, Theater.is_published==True, Theater.allow_buy_ticket==True).fetch()

    return map(lambda x: {'key': x.key.urlsafe(), 'name': x.name}, theaters)

@web.route('/payment_settings/<payment_identifier>/')
def payment_settings_index(payment_identifier):
    if payment_identifier not in ['pesopay']:
        log.warn("ERROR!, payment_settings_index, abort, payment_identifier not supported...")

        abort(404)

    payment_settings = ClientInitiatedPaymentSettings.query(ClientInitiatedPaymentSettings.payment_identifier==payment_identifier).fetch()

    return render_template('payment_settings_index.html', payment_settings=payment_settings, payment_identifier=payment_identifier)

@web.route('/payment_settings/<payment_identifier>/new/', methods=['GET', 'POST'])
def payment_settings_new(payment_identifier):
    if payment_identifier not in ['pesopay']:
        log.warn("ERROR!, payment_settings_new, abort, payment_identifier not supported...")

        abort(404)

    form = PesoPayPaymentSettingsForm()
    payment_settings = get_paymentsettings_theaters(payment_identifier)
    parents_urlstrings = map(lambda s: s.parent().urlsafe(), payment_settings)
    theaters_allowed = get_active_and_allowebuyticket_theaters()
    theaters_without_paymentsettings = filter(lambda t: t['key'] not in parents_urlstrings, theaters_allowed)
    form.theater.choices = [(t['key'], t['name']) for t in theaters_without_paymentsettings]

    if request.method == 'POST' and form.validate_on_submit() and payment_identifier == 'pesopay':
        payment_settings = ClientInitiatedPaymentSettings(id=payment_identifier, parent=ndb.Key(urlsafe=form.theater.data))
        payment_settings.payment_identifier = payment_identifier
        payment_settings.default_params = {
                'currCode': form.currency_code.data,
                'payType': form.pay_type.data
        }
        payment_settings.settings = {
                'pesopay::merchant-id': form.merchand_id.data,
                'pesopay::merchant-hash-secret': form.hash_secret.data,
                'default-form-target': form.default_form_target.data
        }
        payment_settings.put()

        flash("Success! Created New PesoPay Payment Settings!")

        return redirect(url_for('.payment_settings_update_and_show', payment_identifier=payment_identifier,
                payment_settings_id=payment_settings.key.urlsafe()))

    return render_template('payment_settings_new.html', form=form, payment_identifier=payment_identifier)

@web.route('/payment_settings/<payment_identifier>/<payment_settings_id>/', methods=['GET', 'POST'])
def payment_settings_update_and_show(payment_identifier, payment_settings_id):
    if payment_identifier not in ['pesopay']:
        log.warn("ERROR!, payment_settings_update_and_show, abort, payment_identifier not supported...")

        abort(404)

    payment_settings_key = ndb.Key(urlsafe=payment_settings_id)
    payment_settings = payment_settings_key.get()

    if not payment_settings:
        log.warn("ERROR!, payment_settings_update_and_show, abort, payment_settings not found...")

        abort(404)

    form = PesoPayPaymentSettingsEditForm()

    if request.method == 'POST' and form.validate_on_submit() and payment_identifier == 'pesopay':
        payment_settings.default_params['currCode'] = form.currency_code.data
        payment_settings.default_params['payType'] = form.pay_type.data
        payment_settings.settings['pesopay::merchant-id'] = form.merchand_id.data
        payment_settings.settings['pesopay::merchant-hash-secret'] = form.hash_secret.data
        payment_settings.settings['default-form-target'] = form.default_form_target.data
        payment_settings.payment_identifier = payment_identifier
        payment_settings.put()

        flash("Success!, Updated PesoPay Payment Settings!")

        return redirect(url_for('.payment_settings_update_and_show', payment_identifier=payment_identifier,
                payment_settings_id=payment_settings.key.urlsafe()))

    form.payment_identifier.data = payment_settings.payment_identifier
    form.currency_code.data = payment_settings.default_params.get('currCode', None)
    form.pay_type.data = payment_settings.default_params.get('payType', None)
    form.merchand_id.data = payment_settings.settings.get('pesopay::merchant-id', None)
    form.hash_secret.data = payment_settings.settings.get('pesopay::merchant-hash-secret', None)
    form.default_form_target.data = payment_settings.settings.get('default-form-target', None)

    return render_template('payment_settings_show.html', form=form, payment_settings=payment_settings,
            payment_identifier=payment_identifier)

@web.route('/payment_settings/<payment_identifier>/<payment_settings_id>/cinema/new/', methods=['GET', 'POST'])
def payment_settings_cinema_new(payment_identifier, payment_settings_id):
    if payment_identifier not in ['pesopay']:
        log.warn("ERROR!, payment_settings_cinema_new, abort, payment_identifier not supported...")

        abort(404)

    form = PesoPayPaymentSettingsCinemaForm()
    payment_settings_key = ndb.Key(urlsafe=payment_settings_id)
    payment_settings = payment_settings_key.get()

    if not payment_settings:
        log.warn("ERROR!, payment_settings_cinema_new, abort, payment_settings not found...")

        abort(404)

    if payment_settings.check_cinema_settings(form.cinema.data):
        if request.method == 'POST' and form.validate_on_submit() and payment_identifier == 'pesopay':
            cinema_payment_settings = ClientInitiatedPaymentSettingsCinema()
            cinema_payment_settings.cinema = form.cinema.data
            cinema_payment_settings.payment_identifier = payment_settings.payment_identifier
            cinema_payment_settings.default_params = {
                    'currCode': form.currency_code.data,
                    'payType': form.pay_type.data
            }
            cinema_payment_settings.settings = {
                    'pesopay::merchant-id': form.merchand_id.data,
                    'pesopay::merchant-hash-secret': form.hash_secret.data,
                    'default-form-target': form.default_form_target.data
            }

            payment_settings.cinema_settings.append(cinema_payment_settings)
            payment_settings.put()

            flash("Success! Created New PesoPay Payment Settings for specific Cinema!")

            return redirect(url_for('.payment_settings_update_and_show', payment_identifier=payment_identifier,
                    payment_settings_id=payment_settings.key.urlsafe()))
    elif not payment_settings.check_cinema_settings(form.cinema.data):
        form.errors['generic'] = 'Error! Cinema Payment Settings Already Exists!'

    return render_template('payment_settings_cinema_new.html', form=form, payment_settings=payment_settings,
            payment_identifier=payment_identifier)

@web.route('/payment_settings/<payment_identifier>/<payment_settings_id>/cinema/<cinema_name>/', methods=['GET', 'POST'])
def payment_settings_cinema_update_and_show(payment_identifier, payment_settings_id, cinema_name):
    if payment_identifier not in ['pesopay']:
        log.warn("ERROR!, payment_settings_cinema_update_and_show, abort, payment_identifier not supported...")

        abort(404)

    payment_settings_key = ndb.Key(urlsafe=payment_settings_id)
    payment_settings = payment_settings_key.get()

    if not payment_settings:
        log.warn("ERROR!, payment_settings_cinema_update_and_show, abort, payment_settings not found...")

        abort(404)

    cinema_payment_settings = payment_settings.get_cinema_settings(cinema_name)

    if not cinema_payment_settings:
        log.warn("ERROR!, payment_settings_cinema_update_and_show, abort, cinema_payment_settings not found...")

        abort(404)

    form = PesoPayPaymentSettingsCinemaForm()

    if payment_settings.check_cinema_settings(form.cinema.data, cinema_name):
        if request.method == 'POST' and form.validate_on_submit() and payment_identifier == 'pesopay':
            cinema_payment_settings.cinema = form.cinema.data
            cinema_payment_settings.payment_identifier = payment_settings.payment_identifier
            cinema_payment_settings.default_params['currCode'] = form.currency_code.data
            cinema_payment_settings.default_params['payType']  = form.pay_type.data
            cinema_payment_settings.settings['pesopay::merchant-id'] = form.merchand_id.data
            cinema_payment_settings.settings['pesopay::merchant-hash-secret'] = form.hash_secret.data
            cinema_payment_settings.settings['default-form-target'] = form.default_form_target.data
            payment_settings.put()

            flash("Success!, Updated PesoPay Payment Settings for specific Cinema!")

            return redirect(url_for('.payment_settings_update_and_show', payment_identifier=payment_identifier,
                    payment_settings_id=payment_settings.key.urlsafe()))
    elif not payment_settings.check_cinema_settings(form.cinema.data, cinema_name):
        form.errors['generic'] = 'Error! Cinema Payment Settings Already Exists!'

    form.cinema.data = cinema_payment_settings.cinema
    form.currency_code.data = cinema_payment_settings.default_params.get('currCode', None)
    form.pay_type.data = cinema_payment_settings.default_params.get('payType', None)
    form.merchand_id.data = cinema_payment_settings.settings.get('pesopay::merchant-id', None)
    form.hash_secret.data = cinema_payment_settings.settings.get('pesopay::merchant-hash-secret', None)
    form.default_form_target.data = cinema_payment_settings.settings.get('default-form-target', None)

    return render_template('payment_settings_cinema_show.html', form=form, payment_settings=payment_settings,
            payment_identifier=payment_identifier, cinema_name=cinema_name)

@web.route('/payment_settings/<payment_identifier>/<payment_settings_id>/cinema/<cinema_name>/delete/', methods=['POST'])
def payment_settings_cinema_delete(payment_identifier, payment_settings_id, cinema_name):
    if payment_identifier not in ['pesopay']:
        log.warn("ERROR!, payment_settings_cinema_update_and_show, abort, payment_identifier not supported...")

        abort(404)

    payment_settings_key = ndb.Key(urlsafe=payment_settings_id)
    payment_settings = payment_settings_key.get()

    if not payment_settings:
        log.warn("ERROR!, payment_settings_cinema_update_and_show, abort, payment_settings not found...")

        abort(404)

    cinema_payment_settings = payment_settings.get_cinema_settings(cinema_name)

    if not cinema_payment_settings:
        log.warn("ERROR!, payment_settings_cinema_update_and_show, abort, cinema_payment_settings not found...")

        abort(404)

    payment_settings.cinema_settings.remove(cinema_payment_settings)
    payment_settings.put()

    flash("Success!, Deleted PesoPay Payment Settings for specific Cinema!")

    return redirect(url_for('.payment_settings_update_and_show', payment_identifier=payment_identifier,
            payment_settings_id=payment_settings.key.urlsafe()))

@web.route('/clients/')
def clients_index():
    clients = Client.query().fetch()

    return render_template('clients_index.html', clients=clients)

@web.route('/clients/new/', methods=['GET', 'POST'])
def clients_new():
    form = ClientForm()

    if request.method == 'POST' and form.validate_on_submit():
        if not Client.get_by_id(form.id.data):
            client = Client()
            client.key = ndb.Key(Client, form.id.data)
            client.name = form.name.data
            client.pubkey = form.pubkey.data
            client.put()

            flash("Success! Registered client ID: %s!" % client.key.id())

            return redirect(url_for('.clients_update_and_show', client_id=client.key.id()))
        else:
            form.errors['generic'] = "Error! A client with ID '%s' has already been registered!" % form.id.data

    return render_template('clients_new.html', form=form)

@web.route('/clients/<client_id>/', methods=['GET', 'POST'])
def clients_update_and_show(client_id):
    client = Client.get_by_id(client_id)

    if not client:
        log.warn("ERROR!, clients_update_and_show, client_id not found...")

        abort(404)

    form = ClientEditForm(obj=client)

    if request.method == 'POST' and form.validate_on_submit():
        form.populate_obj(client)
        client.put()

        flash("Success! Updated client ID: %s!" % client.key.id())

        return redirect(url_for('.clients_update_and_show', client_id=client_id))

    return render_template('clients_show.html', form=form, client=client)

@web.route('/clients/<client_id>/verify-unregister-client/', methods=['POST'])
def clients_unregister_verify(client_id):
    client = Client.get_by_id(client_id)

    if not client:
        log.warn("ERROR!, clients_unregister_verify, client_id not found...")

        abort(404)

    return render_template('clients_unregister_verify.html', client=client)

@web.route('/clients/<client_id>/unregister-client/', methods=['DELETE'])
def clients_unregister(client_id):
    client = Client.get_by_id(client_id)

    if not client:
        log.warn("ERROR!, clients_unregister, client_id not found...")

        abort(404)

    flash("Success! Client with ID '%s' unregistered!" % client.key.id())

    client.key.delete()

    return redirect(url_for('.clients_index'))


###############
#
# Transactions CMS/Dashboard
#
###############

@web.route('/transactions/<transaction_type>/')
def transactions_index(transaction_type):
    if transaction_type.upper() not in ['BUY', 'RESERVE']:
        log.warn("ERROR!, transactions_index, abort, transaction_type not supported...")

        abort(404)

    a_cursor = ndb.Cursor(urlsafe=request.args.get('a_cursor'))
    r_cursor = ndb.Cursor(urlsafe=request.args.get('c_cursor'))
    d_cursor = ndb.Cursor(urlsafe=request.args.get('d_cursor'))
    c_cursor = ndb.Cursor(urlsafe=request.args.get('c_cursor'))

    active_transactions = ReservationTransaction.query(ReservationTransaction.is_cancelled==False,
            ReservationTransaction.state.IN(ACTIVE_STATES), ReservationTransaction.transaction_type==transaction_type.upper()
            ).order(-ReservationTransaction.date_created, ReservationTransaction.key)
    reaped_transactions = ReservationTransaction.query(ReservationTransaction.is_cancelled==False,
            ReservationTransaction.state==state.TX_CANCELLED, ReservationTransaction.transaction_type==transaction_type.upper()
            ).order(-ReservationTransaction.date_created)
    done_transactions = ReservationTransaction.query(ReservationTransaction.is_cancelled==False,
            ReservationTransaction.state==state.TX_DONE, ReservationTransaction.transaction_type==transaction_type.upper()
            ).order(-ReservationTransaction.date_created)
    cancelled_transactions = ReservationTransaction.query(ReservationTransaction.is_cancelled==True,
            ReservationTransaction.transaction_type==transaction_type.upper()).order(-ReservationTransaction.date_created)

    active_transactions_page, next_a_cursor, a_more = active_transactions.fetch_page(25, start_cursor=a_cursor)
    reaped_transactions_page, next_r_cursor, r_more = reaped_transactions.fetch_page(25, start_cursor=c_cursor)
    done_transactions_page, next_d_cursor, d_more = done_transactions.fetch_page(25, start_cursor=d_cursor)
    cancelled_transactions_page, next_c_cursor, c_more = cancelled_transactions.fetch_page(25, start_cursor=c_cursor)

    return render_template('transactions_index.html', active_txs=active_transactions_page,
            reaped_txs=reaped_transactions_page, done_txs=done_transactions_page,
            cancelled_txs=cancelled_transactions_page, a_cursor=a_cursor, r_cursor=r_cursor,
            d_cursor=d_cursor, c_cursor=c_cursor, next_a_cursor=next_a_cursor, next_r_cursor=next_r_cursor,
            next_d_cursor=next_d_cursor, next_c_cursor=next_c_cursor, a_more=a_more, r_more=r_more,
            d_more=d_more, c_more=c_more, transaction_type=transaction_type)

@web.route('/transactions/<transaction_type>/state/<transaction_state_str>/')
def transactions_list(transaction_type, transaction_state_str):
    if transaction_type.upper() not in ['BUY', 'RESERVE']:
        log.warn("ERROR!, transactions_list, abort, transaction_type not supported...")

        abort(404)

    cursor = ndb.Cursor(urlsafe=request.args.get('cursor'))
    transaction_state_str = transaction_state_str.upper()

    if transaction_state_str == 'TX_ACTIVE':
        log.debug("transactions_list, TX_ACTIVE...")

        transactions = ReservationTransaction.query(ReservationTransaction.transaction_type==transaction_type.upper(),
                ReservationTransaction.state.IN(ACTIVE_STATES), ReservationTransaction.is_cancelled==False
                ).order(-ReservationTransaction.date_created, ReservationTransaction.key)
    elif transaction_state_str == 'TX_REFUNDED':
        log.debug("transactions_list, TX_REFUNDED...")

        transactions = ReservationTransaction.query(ReservationTransaction.transaction_type==transaction_type.upper(),
                ReservationTransaction.state==state.TX_DONE, ReservationTransaction.is_cancelled==True
                ).order(-ReservationTransaction.date_created)
    else:
        log.debug("transactions_list, %s..." % transaction_state_str)

        transaction_state = getattr(state, transaction_state_str) if hasattr(state, transaction_state_str) else None
        transactions = ReservationTransaction.query(ReservationTransaction.transaction_type==transaction_type.upper(),
                ReservationTransaction.state==transaction_state).order(-ReservationTransaction.date_created)

    transactions_page, next_cursor, more = transactions.fetch_page(100, start_cursor=cursor)

    return render_template('transactions_list.html', cursor=cursor, transactions=transactions_page, next_cursor=next_cursor,
            more=more, transaction_type=transaction_type, transaction_state_str=transaction_state_str)

@web.route('/transactions/<transaction_type>/<transaction_id>/')
def transaction_show(transaction_type, transaction_id):
    if transaction_type.upper() not in ['BUY', 'RESERVE']:
        log.warn("ERROR!, transaction_show, abort, transaction_type not supported...")

        abort(404)

    transaction = admin.get_transaction(transaction_id)
    email = transaction.payment_info['email'] if transaction.payment_info and 'email' in transaction.payment_info else None

    return render_template('transactions_show.html', transaction=transaction, transaction_type=transaction_type, email=email)
