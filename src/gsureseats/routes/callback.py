import sys
import logging

from itertools import groupby
from operator import attrgetter

from google.appengine.api import taskqueue
from google.appengine.ext.ndb import Key, put_multi
from google.appengine.runtime.apiproxy_errors import OverQuotaError

from flask import Blueprint, jsonify, redirect, url_for, request, make_response, json, Response

from gsureseats import models, tx
from gsureseats.admin import notify_admin
from gsureseats.exceptions.api import *
from gsureseats.routes.auth import authenticate_request


log = logging.getLogger(__name__)
cb = Blueprint('callback', __name__, template_folder='callback_templates')

AUTH_THEATER_ORGANIZATION = 'X-Globe-SureSeats-Auth-TheaterOrg'
AUTH_TIMESTAMP = 'X-Globe-SureSeats-Auth-Timestamp'
AUTH_SIGNATURE = 'X-Globe-SureSeats-Auth-Signature'


###############
#
# Error Handlers
#
###############

@cb.errorhandler(APIException)
def handle_api_error(e):
    res = jsonify(message=e.message, error_code=e.error_code, details=e.details)
    res.status_code = e.code

    return res

@cb.errorhandler(OverQuotaError)
def handle_quota_error(e):
    res = jsonify(message='The backend has reached a resource quota. Please try again later.',  error_code='SYSTEM_OVER_QUOTA', details=e.message)
    res.status_code = 500
    notify_admin(e, sys.exc_info())

    return res

@cb.errorhandler(Exception)
def handle_system_error(e):
    log.exception(e)

    res = jsonify(message='There has been a system error. Please try again later.', error_code='SYSTEM_GENERIC_ERROR', details=e.message)
    res.status_code = 500
    notify_admin(e, sys.exc_info())

    return res

@cb.before_request
def check_callback_auth():
    log.debug("entered check_callback_auth()...")

    if AUTH_THEATER_ORGANIZATION in request.headers and AUTH_TIMESTAMP in request.headers and AUTH_SIGNATURE in request.headers:
        theater_org = request.headers[AUTH_THEATER_ORGANIZATION]
        auth_ts = request.headers[AUTH_TIMESTAMP]
        org = Key(models.TheaterOrganization, theater_org).get()

        if org:
            log.debug("check_callback_auth, authentication params: TheaterOrg: %s, Timestamp: %s, Signature: %s...", theater_org, auth_ts, request.headers[AUTH_SIGNATURE])

            authenticate_request(org.pubkey, auth_ts, request.headers[AUTH_SIGNATURE])
        else:
            raise ClientAuthException('Client authentication failed.')
    else:
        log.error("check_callback_auth, no authentication headers, callback not allowed...")

        raise ClientAuthException('Theater Organization authentication required.')

@cb.route('/<channel_name>', methods=['POST'])
def post_event(channel_name):
    log.debug("post_event, channel: %s...", channel_name)

    evs = request.json

    if not evs:
        log.debug("post_event, did not get body...")

        raise BadValueException('body', 'You must supply events info.')

    if 'events' not in evs:
        log.debug("post_event, did not get events key...")

        raise BadValueException('events', 'Missing events key for body.')

    log.debug("getting events...")

    events = evs['events']
    org_uuid = request.headers[AUTH_THEATER_ORGANIZATION]
    org = Key(models.TheaterOrganization, org_uuid)

    log.debug("events: %s...", len(events))
    log.debug("theaterorg: %s...", org_uuid)

    def to_event(e):
        ev = models.Event(parent=org)
        ev.event_channel = channel_name
        ev.org_id = e['id']
        ev.timestamp = e['timestamp']

        if 'payload' in e:
            ev.payload = e['payload']

        return ev

    batch = [to_event(e) for e in events]

    put_multi(batch)

    log.debug("batch events: {}...".format(batch))

    listeners = models.Listener.get_listeners(channel_name)
    listener_count = listeners.count()

    if listener_count != 0:
        version_key = attrgetter('version')
        by_version = groupby(sorted(listeners, key=version_key), version_key)

        log.debug("triggering %d listeners...", listener_count)

        for version,_ in by_version:
            log.debug("triggering callback listener for channel: {0} version {1}...".format(channel_name, version))

            taskqueue.add(url=url_for('task.trigger_callback_listeners'), queue_name='transactions',
                    target=version, params={'channel_name': channel_name})

    res = Response(status='204 No Content')
    res.status_code=204

    return make_response(res)
