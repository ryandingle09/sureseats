import base64
import logging
import time

from Crypto.Hash import SHA
from Crypto.PublicKey import RSA
from Crypto.Signature import PKCS1_v1_5

from flask import request

from gsureseats.admin import notify_admin
from gsureseats.exceptions.api import ClientAuthException


log = logging.getLogger(__name__)

TIMESTAMP_MAX_INTERVAL = 10
CLIENT_TIMESTAMP_MAX_INTERVAL = 10


def authenticate_request(caller_keytext, auth_ts, b64sig):
    current_ts = int(time.time())

    # check that authentication timestamp is an integer.
    try:
        ts = int(auth_ts)
    except ValueError:
        log.warn("ERROR!, authenticate_request, invalid timestamp value: %s..." % auth_ts)

        raise ClientAuthException('Client authentication failed.')

    if current_ts - ts > TIMESTAMP_MAX_INTERVAL:
        log.warn("ERROR!, authenticate_request, timestamp too old; possible replay attack...")

        raise ClientAuthException('Client authentication failed, timestamp too old.')

    # automatic ClientAuthException when no key attached.
    if not caller_keytext or caller_keytext == '':
        log.warn("ERROR!, authenticate_request, missing caller_keytext...")

        raise ClientAuthException('Client authentication failed.')

    sig = b64sig.decode('base64')

    sorted_params = sorted(request.args.items(multi=True))
    sorted_params = ["%s=%s" % (k,v) for k,v in sorted_params]
    sorted_params = reduce(lambda a,b: a+b, sorted_params, '')

    log.debug("authenticate_request, got params: %s..." % request.args.items(multi=True))

    text = auth_ts + request.method + request.script_root + request.path + sorted_params

    log.debug("authenticate_request, request: %s...", request)

    if request.data:
        text += SHA.new(request.data).hexdigest()

    log.debug("authenticate_request, calculated text for auth sig: %s..." % text)

    caller_pubkey = RSA.importKey(caller_keytext).publickey()
    verifier = PKCS1_v1_5.new(caller_pubkey)
    text_hash = SHA.new(text)

    if not verifier.verify(text_hash, sig):
        log.warn("ERROR!, authenticate_request, client failed auth check, signature didn't check out...")

        raise ClientAuthException('Client authentication failed.')

def client_authenticate_request(public_key, auth_ts, b64sig):
    current_ts = int(time.time())

    # check that authentication timestamp is an integer.
    try:
        ts = int(auth_ts)
    except ValueError:
        log.warn("ERROR!, client_authenticate_request, invalid timestamp value: %s..." % auth_ts)

        raise ClientAuthException('Client authentication failed.')

    interval_ts = current_ts - ts

    log.info("client_authenticate_request, current_ts: %s..." % current_ts)
    log.info("client_authenticate_request, ts: %s..." % ts)
    log.info("client_authenticate_request, interval: %s..." % interval_ts)

    if interval_ts > CLIENT_TIMESTAMP_MAX_INTERVAL:
        log.warn("ERROR!, client_authenticate_request, timestamp too old, possible replay attack...")

        raise ClientAuthException('Client authentication failed, timestamp too old.')
    else:
        log.debug("client_authenticate_request, timestamp validation success...")

    if not public_key or public_key == '':
        log.warn("ERROR!, client_authenticate_request, missing public_key...")

        raise ClientAuthException('Client authentication failed.')

    sig = base64.b64decode(b64sig)

    log.info("client_authenticate_request, encoded b64sig: %s..." % b64sig)
    log.info("client_authenticate_request, decoded b64sig: %s..." % sig)
    log.debug("client_authenticate_request, got parameters: %s..." % request.args.items(multi=True))

    sorted_params = sorted(request.args.items(multi=True))
    sorted_params = ["%s=%s" % (k, v) for k, v in sorted_params]
    sorted_params = reduce(lambda a, b: a+b, sorted_params, '')
    text = auth_ts + request.method + request.script_root + request.path + sorted_params

    log.info("client_authenticate_request, request: %s..." % request)

    if request.data:
        log.info("client_authenticate_request, request.data: %s..." % request.data)

        text += SHA.new(request.data).hexdigest()

    log.info("client_authenticate_request, calculated text for authentication signature: %s..." % text)
    log.info("client_authenticate_request, public_key: %s..." % public_key)

    caller_pubkey = RSA.importKey(public_key).publickey()
    verifier = PKCS1_v1_5.new(caller_pubkey)
    text_hash = SHA.new(text)

    log.info("client_authenticate_request, text_hash: %s..." % text_hash)
    log.info("client_authenticate_request, signature: %s..." % sig)

    if not verifier.verify(text_hash, sig):
        log.warn("ERROR!, client_authenticate_request, text_hash and signature verification failed...")

        raise ClientAuthException('Client authentication failed.')

    log.debug("client_authenticate_request, client authentication success...")
