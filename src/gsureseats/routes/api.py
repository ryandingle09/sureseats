import logging
import sys
import time
import traceback

from datetime import date, datetime

from flask import jsonify, redirect, render_template, request, Blueprint

from google.appengine.ext import ndb
from google.appengine.ext.ndb import (get_multi, transactional, AND, OR, Key, GenericProperty,
        FloatProperty, IntegerProperty, ComputedProperty, BooleanProperty, DateProperty, KeyProperty)
from google.appengine.runtime.apiproxy_errors import OverQuotaError

from gsureseats import models, orgs, tx
from gsureseats.admin import notify_admin
from gsureseats.exceptions.api import *
from gsureseats.feeds.available_seats import get_available_seats
from gsureseats.feeds.sureseats import (cancel_transaction, forgot_password, login, mpass_balance, mpass_loading,
        register, update_account, read_mpass_transaction_history, read_transaction_history)
from gsureseats.routes.auth import client_authenticate_request
from gsureseats.routes.deprecation import deprecated
from gsureseats.routes.util import (apply_sort, check_filter_and_sort_args, do_filter, do_get_distance, do_get_schedules,
        do_get_schedules_theaters, do_location_search, get_theater_info_common, get_theater_seatmap_common, org_with_theaters,
        query_model, query_model_ids, resolve_dotted_name, theaterorg_theater_safety_check, to_theater_key)
from gsureseats.settings import ENFORCE_AUTH
from gsureseats.util import admin
from gsureseats.util.id_encoder import decode_uuid, encode_uuid, encoded_theater_id, decoded_theater_id

from gsureseats.tx.payment import GCashPayment

log = logging.getLogger(__name__)
api = Blueprint('api', __name__, template_folder='api_templates')

DEVICE_ID = 'X-Globe-SureSeats-DeviceId'
AUTH_CLIENT_ID = 'X-Globe-SureSeats-Auth-ClientId'
AUTH_TIMESTAMP = 'X-Globe-SureSeats-Auth-Timestamp'
AUTH_SIGNATURE = 'X-Globe-SureSeats-Auth-Signature'


def abort(status_code, message='Error', error_code='ERROR_UNKNOWN', details=None, headers={}):
    raise APIException(status_code, message, error_code, details)

@transactional
def _start_tx(device_id, tx_info):
    transaction = tx.start(device_id, tx_info)
    tx.reschedule(transaction)

    return transaction

@transactional
def _start_tx_stopgap(device_id, tx_info):
    transaction = tx.start_stopgap(device_id, tx_info)
    tx.reschedule(transaction)

    return transaction


###############
#
# Error and Before Request Handlers
#
###############

@api.errorhandler(AlreadyCancelledException)
def handle_already_cancelled_error(e):
    res = jsonify(message=e.message, error_code=e.error_code, details=e.details)
    res.status_code = e.code

    for m in e.details:
        res.headers.add('Allow', m)

    return res

@api.errorhandler(APIException)
def handle_api_error(e):
    res = jsonify(message=e.message, error_code=e.error_code, details=e.details)
    res.status_code = e.code

    return res

@api.errorhandler(OverQuotaError)
def handle_quota_error(e):
    res = jsonify(message='The backend has reached a resource quota. Please try again later.', error_code='SYSTEM_OVER_QUOTA', details=e.message)
    res.status_code = 500
    notify_admin("API Version 1", e, sys.exc_info())

    return res

@api.errorhandler(Exception)
def handle_system_error(e):
    res = jsonify(message='There has been a system error. Please try again later.', error_code='SYSTEM_GENERIC_ERROR', details=e.message)
    res.status_code = 500
    notify_admin("API Version 1", e, sys.exc_info())

    log.exception(e)

    return res

@api.before_request
def check_client_auth():
    log.debug("check_client_auth, api version1...")
    log.info("check_client_auth, api version1, request.headers: %s..." % request.headers)

    if AUTH_CLIENT_ID in request.headers and AUTH_TIMESTAMP in request.headers and AUTH_SIGNATURE in request.headers:
        client_id = request.headers[AUTH_CLIENT_ID]
        auth_ts = request.headers[AUTH_TIMESTAMP]
        client = ndb.Key(models.Client, client_id).get()

        if client:
            log.info("check_client_auth, client_id: %s, auth_ts: %s" % (client_id, auth_ts))

            client_authenticate_request(client.pubkey, auth_ts, request.headers[AUTH_SIGNATURE])
        else:
            log.warn("ERROR!, check_client_auth, client authentication failed...")

            raise ClientAuthException('Client authentication failed.')
    else:
        log.debug('check_client_auth, missing authentication headers...')

        if ENFORCE_AUTH:
            log.warn("ERROR, check_client_auth, client authentication required...")

            abort(401, error_code='AUTH_REQUIRED', message='Client authentication required.')

@api.before_request
def check_device_id():
    log.debug("check_device_id, api version1...")

    if DEVICE_ID not in request.headers:
        raise DeviceIdRequiredException()

@api.before_request
def log_caller_info():
    ua = '(None)'
    client_id = '(Not given)'

    if 'User-Agent' in request.headers:
        ua = request.headers['User-Agent']
        
    if AUTH_CLIENT_ID in request.headers:
        client_id = request.headers[AUTH_CLIENT_ID]

    log.info("log_caller_info, api version1, User-Agent: %s..." % ua)
    log.info("log_caller_info, api version1, Client ID: %s..." % client_id)


###############
#
# API Meta
#
###############

@api.route('/')
def api_meta():
    return jsonify({'version': '1.0', 'api-version': '1.0'})


###############
#
# Movies Endpoints
#
###############

@api.route('/movies/')
def get_movies():
    filters, movie_list = query_model(models.Movie, 'movie', request)

    return jsonify({'filters': filters, 'results': movie_list})

@api.route('/movies/ids/')
def get_movies_ids():
    filters, movie_ids = query_model_ids(models.Movie, 'movie', request)

    return jsonify({'filters': filters, 'results': movie_ids})

@api.route('/movies/<movie_id>/')
def get_movie(movie_id):
    movie = Key(models.Movie, movie_id).get()

    if not movie:
        raise NotFoundException(details={'movie_id': movie_id})

    return jsonify({'id': movie_id, 'movie': movie.to_entity()})


###############
#
# Theater Organizations Endpoints
#
###############

@api.route('/theaterorgs/')
def get_theaterorgs():
    check_filter_and_sort_args(models.TheaterOrganization, request.args)
    queryset = models.TheaterOrganization.query()
    filters, queryset = do_filter(models.TheaterOrganization, queryset, request.args)
    theaterorg_list = [{'id': org.key.id(), 'theater_organization': org_with_theaters(org)} for org in queryset]

    return jsonify({'filters': filters, 'results': theaterorg_list})

@api.route('/theaterorgs/<theaterorg_id>/')
def get_theaterorg_info(theaterorg_id):
    org_key = Key(models.TheaterOrganization, theaterorg_id)
    org = org_key.get()

    if not org:
        raise NotFoundException(details={'theaterorg_id': theaterorg_id})

    return jsonify({'id': theaterorg_id, 'theater_organization': org_with_theaters(org)})

@api.route('/theaterorgs/<theaterorg_id>/theaters/')
def get_theaterorg_theaters(theaterorg_id):
    org_key = Key(models.TheaterOrganization, theaterorg_id)
    org = org_key.get()

    if not org:
        raise NotFoundException(details={'theaterorg_id': theaterorg_id})

    if (('location.lon' in request.args and 'location.lat' in request.args) or
            ('cinemas.location.lon' in request.args and 'cinemas.location.lat' in request.args)):
        lat, lon, theater_keys = do_location_search(request)
        org_theater_keys = models.Theater.query(models.Theater.is_published==True, models.Theater.is_active==True, ancestor=org_key).fetch(keys_only=True)
        matched_keys = set(theater_keys).intersection(set(org_theater_keys))
        theater_list = do_get_distance(lat, lon, matched_keys, request)
        filters = {'match': {'location.lat': lat, 'location.lon': lon}}
    else:
        check_filter_and_sort_args(models.Theater, request.args)
        q = models.Theater.query(models.Theater.is_published==True, models.Theater.is_active==True, ancestor=org_key)
        filters, q = do_filter(models.Theater, q, request.args)
        theater_list = [{'id': encoded_theater_id(x.key), 'theater': x.to_entity()} for x in q]

    return jsonify({'filters': filters, 'results': theater_list})

@api.route('/theaterorgs/<theaterorg_id>/theaters/<ignored>~<int:theater_id>/')
def get_theaterorg_theater_info(theaterorg_id, ignored, theater_id):
    theaterorg_theater_safety_check(theaterorg_id, ignored, theater_id)

    return get_theater_info_common(theaterorg_id, ignored, theater_id)

@api.route('/theaterorgs/<theaterorg_id>/theaters/<uncoded_id>/')
def dummy_theaterorg_theater_info(theaterorg_id, uncoded_id):
    raise NotFoundException(details={'theaterorg_id': theaterorg_id, 'theater_id': uncoded_id})


###############
#
# Theaters Endpoints
#
###############

@api.route('/theaters/')
def get_theaters():
    if (('location.lon' in request.args and  'location.lat' in request.args) or
            ('cinemas.location.lon' in request.args  and 'cinemas.location.lat' in request.args)):
        lat, lon, theater_keys = do_location_search(request)
        theater_list = do_get_distance(lat, lon, theater_keys, request)
        filters = {'match': {'location.lat': lat, 'location.lon': lon}}
    else:
        filters, theater_list = query_model(models.Theater, 'theater', request, id_encoder=encoded_theater_id)

    return jsonify({'filters': filters, 'results': theater_list})

@api.route('/theaters/<enc_org_uuid>~<int:theater_id>/')
def get_theater_info(enc_org_uuid, theater_id):
    org_uuid = decode_uuid(enc_org_uuid)

    return get_theater_info_common(str(org_uuid), enc_org_uuid, theater_id)

@api.route('/theaters/<uncoded_id>/')
def dummy_theater_info(uncoded_id):
    raise NotFoundException(details={'theater_id': uncoded_id})


###############
#
# Schedules and Available Seats Endpoints
#
###############

@api.route('/schedules/')
def get_schedules():
    return do_get_schedules()

@api.route('/theaterorgs/<theaterorg_id>/theaters/<ignored>~<int:theater_id>/schedules/')
def get_theaterorg_theater_schedules(theaterorg_id, ignored, theater_id):
    theaterorg_theater_safety_check(theaterorg_id, ignored, theater_id)

    return get_theater_schedules(ignored, theater_id)

@api.route('/theaters/<enc_org_uuid>~<int:theater_id>/schedules/')
def get_theater_schedules(enc_org_uuid, theater_id):
    return do_get_schedules(theater_pair=(enc_org_uuid,theater_id))

@api.route('/movies/<movie_id>/schedules/')
def get_movie_schedules(movie_id):
    return do_get_schedules(movie_id=movie_id)

@api.route('/movies/<movie_id>/schedules/theaters/')
def get_movie_schedules_theaters(movie_id):
    return do_get_schedules_theaters(movie_id=movie_id)

@api.route('/available_seats/')
def translate_and_get_available_seats():
    log.debug("translate_and_get_available_seats, api version1...")

    if 'theater' not in request.args:
        log.warn("ERROR!, translate_and_get_available_seats, missing parameter theater...")

        raise BadValueException('theater', 'You must specify a theater.')
    if 'schedule' not in request.args:
        log.warn("ERROR!, translate_and_get_available_seats, missing parameter schedule...")

        raise BadValueException('schedule', 'You must specify a schedule.')
    if 'time' not in request.args:
        log.warn("ERROR!, translate_and_get_available_seats, missing parameter time...")

        raise BadValueException('time', 'You must specify a time.')

    available = []
    remaining_seats = 0
    error_message = 'Unable to load the seats due to an unexpected error. Please try again later.'

    enc_theater_id = request.args['theater']
    sched_id_raw = request.args['schedule']
    sched_id = sched_id_raw.split(',')[0]
    sched_time = datetime.strptime(request.args['time'], models.TIME_FORMAT).time()
    theater_key = to_theater_key(enc_theater_id)
    theater = theater_key.get()

    if not theater:
        log.warn("ERROR!, translate_and_get_available_seats, theater is not found...")

        raise NotFoundException(details={'theater': enc_theater_id})

    log.info("translate_and_get_available_seats, theater.name: %s..." % theater.name)
    log.info("translate_and_get_available_seats, theater.org_theater_code: %s..." % theater.org_theater_code)
    log.info("translate_and_get_available_seats, theater.allow_buy_ticket: %s..." % theater.allow_buy_ticket)
    log.info("translate_and_get_available_seats, theater.allow_reserve_ticket: %s..." % theater.allow_reserve_ticket)

    if not theater.allow_buy_ticket and not theater.allow_reserve_ticket:
        log.warn("ERROR!, translate_and_get_available_seats, buy ticket and reserve ticket is not allowed...")

        res = {'theater': enc_theater_id, 'schedule': sched_id, 'available_seat_map': available,
                'available_seats': remaining_seats, 'error_message': error_message}

        return jsonify(res)

    theater_code = theater.org_theater_code
    schedule = Key(models.Schedule, sched_id, parent=theater.key).get()

    if not schedule:
        log.warn("ERROR!, translate_and_get_available_seats, schedule is not found...")

        raise NotFoundException(details={'schedule': sched_id})

    sched_slot = None

    for sl in schedule.slots:
        if sl.start_time == sched_time:
            sched_slot = sl

            break

    if not sched_slot:
        log.warn("ERROR!, translate_and_get_available_seats, schedule slot is not found...")

        raise NotFoundException(details={'time': request.args['time']})

    if schedule.show_date < date.today():
        log.warn("ERROR!, translate_and_get_available_seats, schedule is expired...")

        raise BadValueException('schedule', 'Expired schedule.')

    sched_cinema = schedule.cinema
    sched_slot_code = sched_slot.feed_code
    seating_type = sched_slot.seating_type

    log.info("translate_and_get_available_seats, sched_cinema: %s..." % sched_cinema)
    log.info("translate_and_get_available_seats, sched_slot_code: %s..." % sched_slot_code)
    log.info("translate_and_get_available_seats, seating_type: %s..." % seating_type)

    # theater organization API lookup here; for now, hard-coded to use SureSeats API directly.
    if str(theater_key.parent().id()) == str(orgs.AYALA_MALLS):
        log.debug("translate_and_get_available_seats, SureSeats: get_available_seats...")

        if theater_code == 'ATC' and sched_cinema in ['1']:
            log.warn("ATC! add suffix 1, update theater_code, %s, cinema, %s..." % (theater_code, sched_cinema))

            theater_code = '%s1' % theater_code
        elif theater_code == 'ATC' and sched_cinema in ['2', '3', '4', '5']:
            log.warn("ATC! add suffix 2, update theater_code, %s, cinema, %s..." % (theater_code, sched_cinema))

            theater_code = '%s2' % theater_code

        status, available, remaining_seats = get_available_seats(theater_key.parent().id(), theater_code, sched_slot_code)
    else:
        log.warn("ERROR!, translate_and_get_available_seats, missing third party available seats endpoint...")

        status = 'error'

    if status == 'success':
        error_message = ''

    available_seats = {'theater': enc_theater_id, 'schedule': sched_id, 'available_seat_map': available,
            'available_seats': remaining_seats, 'error_message': error_message}

    return jsonify(available_seats)


###############
#
# SureSeats Log-In, Transaction History, and M-Pass Transaction History Endpoints
#
###############

@api.route('/sureseats/register/', methods=['POST'])
def sureseats_register():
    payload = request.json

    if not payload:
        log.warn("ERROR!, sureseats_register, missing register payload...")

        raise BadValueException('body', 'You must supply register information.')

    if 'username' not in payload:
        log.warn("ERROR!, sureseats_register, username is required...")

        raise BadValueException('body', 'Username is required.')

    if 'password' not in payload:
        log.warn("ERROR!, sureseats_register, password is required...")

        raise BadValueException('body', 'Password is required.')

    username = payload.pop('username')
    password = payload.pop('password')
    customer_details = register(username, password, **payload)

    return jsonify({'customer': customer_details})

@api.route('/sureseats/update_account/', methods=['POST'])
def sureseats_update_account():
    payload = request.json

    if not payload:
        log.warn("ERROR!, sureseats_update_account, missing update account payload...")

        raise BadValueException('body', 'You must supply update account information.')

    if 'customer_id' not in payload:
        log.warn("ERROR!, sureseats_update_account, customer_id is required...")

        raise BadValueException('body', 'Customer ID is required.')

    if 'username' not in payload:
        log.warn("ERROR!, sureseats_update_account, username is required...")

        raise BadValueException('body', 'Username is required.')

    customer_id = payload.pop('customer_id')
    username = payload.pop('username')
    password = payload.pop('password', '')
    customer_details = update_account(username, password, customer_id, **payload)

    return jsonify({'customer': customer_details})

@api.route('/sureseats/login/', methods=['POST'])
def sureseats_login():
    payload = request.json

    if not payload:
        log.warn("ERROR!, sureseats_login, missing login payload...")

        raise BadValueException('body', 'You must supply login information.')

    if 'username' not in payload:
        log.warn("ERROR!, sureseats_login, username is required...")

        raise BadValueException('body', 'Username is required.')

    if 'password' not in payload:
        log.warn("ERROR!, sureseats_login, password is required...")

        raise BadValueException('body', 'Password is required.')

    username = payload.pop('username')
    password = payload.pop('password')
    customer_details = login(username, password)

    return jsonify({'customer': customer_details})

@api.route('/sureseats/forgot_password/', methods=['POST'])
def sureseats_forgot_password():
    payload = request.json

    if not payload:
        log.warn("ERROR!, sureseats_forgot_password, missing forgot password payload...")

        raise BadValueException('body', 'You must supply forgot password information.')

    if 'email' not in payload:
        log.warn("ERROR!, sureseats_forgot_password, email is required...")

        raise BadValueException('body', 'Email is required.')

    customer_details = forgot_password(payload['email'])

    return jsonify({'customer': customer_details})


@api.route('/sureseats-gmovies/cancel_transaction/', methods=['POST'])
def sureseats_cancel_transaction_gmovies():
    payload = request.json

    if not payload:
        log.warn("ERROR!, sureseats_cancel_transaction, missing cancel transaction payload...")
        raise BadValueException('body', 'You must supply cancel transaction information.')

    if 'customer_type' not in payload:
        log.warn("ERROR!, sureseats_cancel_transaction, customer_type is required...")
        raise BadValueException('body', 'Customer type is required.')

    if 'claim_code' not in payload:
        log.warn("ERROR!, sureseats_cancel_transaction, claim_code is required...")
        raise BadValueException('body', 'Claim Code is required.')

    customer_id = payload.pop('customer_id', '')
    customer_type = payload.pop('customer_type')
    customer_type = customer_type.upper()
    claim_code = payload.pop('claim_code')

    if customer_type == 'MEMBER' and not customer_id:
        log.warn("ERROR!, sureseats_cancel_transaction, Member, Customer ID is required...")
        raise BadValueException('body', 'Customer ID is required.')

    cancel_details = cancel_transaction(customer_id, customer_type, claim_code)
    return jsonify({'cancel_transaction': cancel_details})


@api.route('/sureseats/cancel_transaction/', methods=['POST'])
def sureseats_cancel_transaction():
    payload = request.json

    if not payload:
        log.warn("ERROR!, sureseats_cancel_transaction, missing cancel transaction payload...")
        raise BadValueException('body', 'You must supply cancel transaction information.')

    if 'customer_type' not in payload:
        log.warn("ERROR!, sureseats_cancel_transaction, customer_type is required...")
        raise BadValueException('body', 'Customer type is required.')

    if 'claim_code' not in payload:
        log.warn("ERROR!, sureseats_cancel_transaction, claim_code is required...")
        raise BadValueException('body', 'Claim Code is required.')

    customer_id = payload.pop('customer_id', '')
    customer_type = payload.pop('customer_type')
    customer_type = customer_type.upper()
    claim_code = payload.pop('claim_code')

    try:
        payment_method = str(payload.pop('payment_method'))
        if payment_method.lower() == 'gcash':
            try:
                transaction = tx.query_by_claimcode(claim_code)
                e = GCashPayment(transaction.payment_info)
                e.do_refund_tx(transaction)
            except Exception as e:
                log.debug("ERROR!, GCash cancel cannot be completed.")
                log.debug(e)
                log.debug(traceback.format_exc())

    except Exception as e:
        log.warn("WARNING, sureseats_cancel_transaction, payment_method is null")

    if customer_type == 'MEMBER' and not customer_id:
        log.warn("ERROR!, sureseats_cancel_transaction, Member, Customer ID is required...")

        raise BadValueException('body', 'Customer ID is required.')

    cancel_details = cancel_transaction(customer_id, customer_type, claim_code)
    return jsonify({'cancel_transaction': cancel_details})


@api.route('/sureseats/mpass_loading/', methods=['POST'])
def sureseats_mpass_loading():
    payload = request.json

    if not payload:
        log.warn("ERROR!, sureseats_mpass_loading, mpass mpass loading payload...")

        raise BadValueException('body', 'You must supply mpass loading information.')

    if 'customer_id' not in payload:
        log.warn("ERROR!, sureseats_mpass_loading, Customer ID is required...")

        raise BadValueException('body', 'Customer ID is required.')

    if 'load_code' not in payload:
        log.warn("ERROR!, sureseats_mpass_loading, load_code is required...")

        raise BadValueException('body', 'Load Code is required.')

    customer_id = payload.pop('customer_id')
    load_code = payload.pop('load_code')
    reload_details = mpass_loading(customer_id, load_code)

    return jsonify({'mpass_loading': reload_details})

@api.route('/sureseats/mpass_balance/', methods=['POST'])
def sureseats_mpass_balance():
    payload = request.json

    if not payload:
        log.warn("ERROR!, sureseats_mpass_balance, missing mpass balance payload...")

        raise BadValueException('body', 'You must supply mpass loading information.')

    if 'customer_id' not in payload:
        log.warn("ERROR!, sureseats_mpass_balance, Customer ID is required...")

        raise BadValueException('body', 'Customer ID is required.')

    if 'mpass_card' not in payload:
        log.warn("ERROR!, sureseats_mpass_balance, mpass_card is required...")

        raise BadValueException('body', 'MPass Card is required.')

    customer_id = payload.pop('customer_id')
    mpass_card = payload.pop('mpass_card')
    balance_details = mpass_balance(customer_id, mpass_card)

    return jsonify({'mpass_balance': balance_details})

@api.route('/sureseats/transaction_history/', methods=['POST'])
def sureseats_transaction_history():
    payload = request.json

    if not payload:
        log.warn("ERROR!, sureseats_transaction_history, missing transaction history payload...")

        raise BadValueException('body', 'You must supply transaction history information.')

    if 'customer_id' not in payload:
        log.warn("ERROR!, sureseats_transaction_history, customer_id is required...")

        raise BadValueException('body', 'Customer ID is required.')

    transaction_history = read_transaction_history(payload['customer_id'])

    return jsonify({'transaction_history': transaction_history})

@api.route('/sureseats/transaction_history/mpass/', methods=['POST'])
def sureseats_mpass_transaction_history():
    payload = request.json

    if not payload:
        log.warn("ERROR!, sureseats_mpass_transaction_history, missing mpass transaction history payload...")

        raise BadValueException('body', 'You must supply customer information.')

    if 'customer_id' not in payload:
        log.warn("ERROR!, sureseats_mpass_transaction_history, customer_id is required...")

        raise BadValueException('body', 'Customer ID is required.')

    mpass_txhistory = read_mpass_transaction_history(payload['customer_id'])

    return jsonify({'mpass_transaction_history': mpass_txhistory})


###############
#
# Transactions Endpoints
#
###############

@api.route('/tx/', methods=['GET'])
def get_device_txs():
    is_cancelled_filter = None
    state_filter = None
    transaction_type_filter = None
    device_id = request.headers[DEVICE_ID]
    device_key = Key(models.Device, device_id)
    q = models.ReservationTransaction.query(ancestor=device_key)

    if 'is_cancelled' in request.args and request.args['is_cancelled']:
        is_cancelled = True if request.args['is_cancelled'].lower() == 'true' else False
        q = q.filter(models.ReservationTransaction.is_cancelled==is_cancelled)
        is_cancelled_filter = is_cancelled
    else:
        q = q.filter(models.ReservationTransaction.is_cancelled==False)

    if 'states' in request.args:
        state_strs = request.args.getlist('states')
        states = filter(None, [getattr(tx.state, s) if hasattr(tx.state, s) else None for s in state_strs])
        log.debug("States to match: %s" % states)
        q = q.filter(models.ReservationTransaction.state.IN(states))
        state_filter = state_strs
    else:
        q = q.filter(models.ReservationTransaction.state != tx.state.TX_DONE)

    if 'transaction_types' in request.args:
        transaction_types = [t.upper() for t in request.args.getlist('transaction_types')]
        q = q.filter(models.ReservationTransaction.transaction_type.IN(transaction_types))
        transaction_type_filter = transaction_types

    if 'sort' in request.args:
        sort_cols = [col if col[0] != '-' else col[1:] for col in request.args.getlist('sort')]
        check = filter(lambda tup: not tup[1], [(k, resolve_dotted_name(models.ReservationTransaction, k)) for k in sort_cols])

        if len(check) > 0:
            unknown_sort_args = [k for k, v in check]

            raise InvalidOrderPropertyException(unknown_sort_args)

        q = apply_sort(q, models.ReservationTransaction, request.args.getlist('sort'))

    txs = [k.id() for k in q.fetch(keys_only=True)]

    return jsonify({'states': state_filter, 'transaction_types': transaction_type_filter, 'transactions': txs})

@api.route('/tx/', methods=['POST'])
def start_tx():
    device_id = request.headers[DEVICE_ID]

    try:
        log.info("start_tx, request.json: {}...".format(request.json))

        tx_info = request.json
    except Exception as e:
        log.info("start_tx, request.data: {}...".format(request.data))
        log.warn("ERROR!, start_tx, malformed request.json...")
        log.error(e)

        data = unicode(request.data, 'latin-1')
        tx_info = json.loads(data)
    log.info("start_tx, tx_info: {}...".format(tx_info))

    if not tx_info:
        log.warn("ERROR!, start_tx, missing tx_info...")

        raise BadValueException('body', 'You must supply transaction information.')

    transaction = _start_tx(device_id, tx_info)

    return jsonify({'id': transaction.key.id(), 'transaction': transaction.to_entity()})

@api.route('/tx/<tx_id>/', methods=['PUT'])
def update_tx_info(tx_id):
    device_id = request.headers[DEVICE_ID]

    try:
        log.info("update_tx_info, request.json: {}...".format(request.json))

        tx_info = request.json
    except Exception as e:
        log.info("update_tx_info, request.data: {}...".format(request.data))
        log.warn("ERROR!, update_tx_info, malformed request.json...")
        log.error(e)

        data = unicode(request.data, 'latin-1')
        tx_info = json.loads(data)

    log.info("update_tx_info, tx_info: {}...".format(tx_info))

    if not tx_info:
        raise BadValueException('body', 'You must supply transaction information.')

    transaction = tx.update(device_id, tx_id, tx_info)

    return jsonify({'id' : tx_id, 'transaction': transaction.to_entity()})

@api.route('/tx/<tx_id>/', methods=['DELETE'])
def cancel_tx(tx_id):
    device_id = request.headers[DEVICE_ID]
    tx.cancel(device_id, tx_id)

    return jsonify({'id' : tx_id, 'cancelled': True})

@api.route('/tx/<tx_id>/', methods=['GET'])
def get_tx_info(tx_id):
    device_id = request.headers[DEVICE_ID]
    transaction = tx.query_info(device_id, tx_id)

    if not transaction:
        raise NotFoundException(details={'tx_id': tx_id})

    return jsonify({'id': tx_id, 'transaction': transaction.to_entity()})

@api.route('/tx/<tx_id>/state/', methods=['GET'])
def get_tx_state(tx_id):
    device_id = request.headers[DEVICE_ID]
    transaction = tx.query_info(device_id, tx_id)
    state, msg = tx.query_state(device_id, tx_id)

    if not transaction:
        raise NotFoundException(details={'tx_id': tx_id})

    if (state, msg) == (None, None):
        raise NotFoundException(details={'tx_id': tx_id})

    tx_state = tx.to_state_str(state)

    if msg:
        log.info("get_tx_state, state: %s, message: %s..." % (tx_state, msg))

        return jsonify({'state': tx_state, 'message': msg})

    log.info("get_tx_state, state: %s..." % tx_state)

    return jsonify({'state': tx_state})

@api.route('/tx/<tx_id>/state/', methods=['PUT'])
def force_tx_state(tx_id):
    abort(501)

@api.route('/tx/<tx_id>/client-initiate/', methods=['GET'])
def client_initiate_payment(tx_id):
    device_id = request.headers[DEVICE_ID]
    transaction = tx.query_info(device_id, tx_id)

    if not transaction:
        raise NotFoundException(details={'tx_id': tx_id})

    state, msg = tx.query_state(device_id, tx_id)

    if state != tx.state.TX_CLIENT_PAYMENT_HOLD:
        raise TransactionConflictException([tx.state.TX_CLIENT_PAYMENT_HOLD])

    payment_info = transaction.workspace['payment::engine'] # FIXME: leaked constant.
    reference = transaction.reservation_reference
    org_id = transaction.workspace['theaters.org_id'][0] # FIXME: hardcoded reference.

    if org_id == str(orgs.AYALA_MALLS):
        theater_code = transaction.workspace['theaters.org_theater_code'][0]
        reference = "%s-%s" % (theater_code, reference)

    payment_info.form_parameters['amount'] = transaction.workspace['RSVP:totalamount'] # override amount specified.
    payment_info.form_parameters['secureHash'] = payment_info.generate_hash(reference) # FIXME: hardcoded hash key and calculation.

    return render_template('client_initiated_payment.html', tx=transaction, reference=reference, payment_info=payment_info)

@api.route('/tx-dummy/', methods=['POST'])
def dummy_start_tx():
    tx_info = request.json

    log.info("dummy_start_tx, tx_info: {}...".format(tx_info))

    if not tx_info:
        log.warn("ERROR!, dummy_start_tx, missing tx_info...")

        raise BadValueException('body', 'You must supply transaction information.')

    transaction = admin.dummy_tx_info(tx_info)

    return jsonify({'id': 'dummy', 'transaction': transaction})
