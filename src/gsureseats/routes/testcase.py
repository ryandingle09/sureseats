import logging
import sys

from datetime import datetime
from random import randint

from google.appengine.api import memcache
from google.appengine.api import taskqueue
from google.appengine.ext import ndb
from google.appengine.ext.ndb import transactional
from google.appengine.runtime.apiproxy_errors import OverQuotaError

from flask import jsonify, request, Blueprint

from gsureseats import tx
from gsureseats.admin import notify_admin
from gsureseats.exceptions.api import *


log = logging.getLogger(__name__)
testcase = Blueprint('testcase', __name__, template_folder='')

MAX_QUEUES = 4
DEVICE_ID = 'X-Globe-SureSeats-DeviceId'


def _incr_queue_id():
    while True:
        queue_id = memcache.get('testcase_queue_id')

        if queue_id == None:
            memcache.set('testcase_queue_id', 1)

            return 1

        new_queue_id = (queue_id % MAX_QUEUES) + 1

        memcache.set('testcase_queue_id', new_queue_id)

        return new_queue_id

@transactional
def _start_tx(device_id, tx_info):
    transaction = tx.start(device_id, tx_info)
    tx.reschedule(transaction)

    return transaction


###############
#
# API Meta
#
###############

@testcase.route('/')
def api_meta():
    return jsonify({'version': '1.0', 'api-version': '1.0'})


###############
#
# Error and Before Request Handlers
#
###############

@testcase.errorhandler(AlreadyCancelledException)
def handle_already_cancelled_error(e):
    res = jsonify(message=e.message, error_code=e.error_code, details=e.details)
    res.status_code = e.code

    for m in e.details:
        res.headers.add('Allow', m)

    return res

@testcase.errorhandler(APIException)
def handle_api_error(e):
    res = jsonify(message=e.message, error_code=e.error_code, details=e.details)
    res.status_code = e.code

    return res

@testcase.errorhandler(OverQuotaError)
def handle_quota_error(e):
    res = jsonify(message='The backend has reached a resource quota. Please try again later.', error_code='SYSTEM_OVER_QUOTA', details=e.message)
    res.status_code = 500
    notify_admin("Test Case API", e, sys.exc_info())

    return res

@testcase.errorhandler(Exception)
def handle_system_error(e):
    res = jsonify(message='There has been a system error. Please try again later.', error_code='SYSTEM_GENERIC_ERROR', details=e.message)
    res.status_code = 500
    notify_admin("Test Case API", e, sys.exc_info())

    log.exception(e)

    return res

@testcase.before_request
def check_device_id():
    log.debug("check_device_id, test case api...")

    if DEVICE_ID not in request.headers:
        raise DeviceIdRequiredException()

@testcase.before_request
def log_caller_info():
    ua = '(None)'
    client_id = '(Not given)'

    if 'User-Agent' in request.headers:
        ua = request.headers['User-Agent']

    log.info("log_caller_info, test case api, User-Agent: %s..." % ua)
    log.info("log_caller_info, test case api, Client ID: %s..." % client_id)


###############
#
# Test Case APIs
#
###############

@testcase.route('/tx/', methods=['POST'])
def start_tx():
    device_id = request.headers[DEVICE_ID]
    tx_info = request.json

    if device_id != 'TEST-CASE-STRESS-TEST':
        log.warn("ERROR!, test case, device_id not allowed...")

        raise BadValueException('header', 'You must supply correct device_id.')

    if not tx_info:
        log.warn("ERROR!, test case, start_tx, missing tx_info...")

        raise BadValueException('body', 'You must supply transaction information.')

    schedules = [
        ('24 Mar 2017 11:00', 'Reserved Seating'),
        ('24 Mar 2017 14:00', 'Reserved Seating'),
        ('24 Mar 2017 16:00', 'Reserved Seating'),
        ('24 Mar 2017 18:00', 'Reserved Seating'),
        ('24 Mar 2017 20:00', 'Reserved Seating')
    ]
    seats = [
        'A1', 'A2', 'A3', 'A4', 'A5', 'A6', 'A7', 'A8', 'A9', 'A10', 'A11', 'A12', 'A13', 'A14', 'A15', 'A16', 'A17', 'A18', 'A19', 'A20', 'A21', 'A22', 'A23', 'A24', 'A25', 'A26',
        'B1', 'B2', 'B3', 'B4', 'B5', 'B6', 'B7', 'B8', 'B9', 'B10', 'B11', 'B12', 'B13', 'B14', 'B15', 'B16', 'B17', 'B18', 'B19', 'B20', 'B21', 'B22', 'B23', 'B24', 'B25', 'B26', 'B27',
        'C1', 'C2', 'C3', 'C4', 'C5', 'C6', 'C7', 'C8', 'C9', 'C10', 'C11', 'C12', 'C13', 'C14', 'C15', 'C16', 'C17', 'C18', 'C19', 'C20', 'C21', 'C22', 'C23', 'C24', 'C25', 'C26', 'C27', 'C28',
        'D1', 'D2', 'D3', 'D4', 'D5', 'D6', 'D7', 'D8', 'D9', 'D10', 'D11', 'D12', 'D13', 'D14', 'D15', 'D16', 'D17', 'D18', 'D19', 'D20', 'D21', 'D22', 'D23', 'D24', 'D25', 'D26', 'D27', 'D28', 'D29',
        'E1', 'E2', 'E3', 'E4', 'E5', 'E6', 'E7', 'E8', 'E9', 'E10', 'E11', 'E12', 'E13', 'E14', 'E15', 'E16', 'E17', 'E18', 'E19', 'E20', 'E21', 'E22', 'E23', 'E24', 'E25', 'E26', 'E27', 'E28', 'E29', 'E30',
        'F1', 'F2', 'F3', 'F4', 'F5', 'F6', 'F7', 'F8', 'F9', 'F10', 'F11', 'F12', 'F13', 'F14', 'F15', 'F16', 'F17', 'F18', 'F19', 'F20', 'F21', 'F22', 'F23', 'F24', 'F25', 'F26', 'F27', 'F28', 'F29', 'F30', 'F31',
        'G1', 'G2', 'G3', 'G4', 'G5', 'G6', 'G7', 'G8', 'G9', 'G10', 'G11', 'G12', 'G13', 'G14', 'G15', 'G16', 'G17', 'G18', 'G19', 'G20', 'G21', 'G22', 'G23', 'G24', 'G25', 'G26', 'G27', 'G28', 'G29', 'G30', 'G31', 'G32',
        'H1', 'H2', 'H3', 'H4', 'H5', 'H6', 'H7', 'H8', 'H9', 'H10', 'H11', 'H12', 'H13', 'H14', 'H15', 'H16', 'H17', 'H18', 'H19', 'H20', 'H21', 'H22', 'H23', 'H24', 'H25', 'H26', 'H27', 'H28', 'H29', 'H30', 'H31',
        'I1', 'I2', 'I3', 'I4', 'I5', 'I6', 'I7', 'I8', 'I9', 'I10', 'I11', 'I12', 'I13', 'I14', 'I15', 'I16', 'I17', 'I18', 'I19', 'I20', 'I21', 'I22', 'I23', 'I24', 'I25', 'I26', 'I27', 'I28', 'I29', 'I30', 'I31', 'I32',
        'J1', 'J2', 'J3', 'J4', 'J5', 'J6', 'J7', 'J8', 'J9', 'J10', 'J11', 'J12', 'J13', 'J14', 'J15', 'J16', 'J17', 'J18', 'J19', 'J20', 'J21', 'J22', 'J23', 'J24', 'J25', 'J26', 'J27', 'J28', 'J29', 'J30', 'J31', 'J32', 'J33',
        'K1', 'K2', 'K3', 'K4', 'K5', 'K6', 'K7', 'K8', 'K9', 'K10', 'K11', 'K12', 'K13', 'K14', 'K15', 'K16', 'K17', 'K18', 'K19', 'K20', 'K21', 'K22', 'K23', 'K24', 'K25', 'K26', 'K27', 'K28', 'K29', 'K30', 'K31', 'K32', 'K33', 'K34',
        'L1', 'L2', 'L3', 'L4', 'L5', 'L6', 'L7', 'L8', 'L9', 'L10', 'L11', 'L12', 'L13', 'L14', 'L15', 'L16', 'L17', 'L18', 'L19', 'L20', 'L21', 'L22', 'L23', 'L24', 'L25', 'L26', 'L27', 'L28', 'L29', 'L30', 'L31', 'L32', 'L33',
        'M1', 'M2', 'M3', 'M4', 'M5', 'M6', 'M7', 'M8', 'M9', 'M10', 'M11', 'M12', 'M13', 'M14', 'M15', 'M16', 'M17', 'M18', 'M19', 'M20', 'M21', 'M22', 'M23', 'M24', 'M25', 'M26', 'M27', 'M28', 'M29', 'M30', 'M31', 'M32', 'M33', 'M34', 'M35', 'M36',
        'N1', 'N2', 'N3', 'N4', 'N5', 'N6', 'N7', 'N8', 'N9', 'N10', 'N11', 'N12', 'N13', 'N14', 'N15', 'N16', 'N17', 'N18', 'N19', 'N20', 'N21', 'N22', 'N23', 'N24', 'N25', 'N26', 'N27', 'N28', 'N29'
    ]

    payload = {}
    payload['payment'] = tx_info['payment']
    payload['reservations'] = tx_info['reservations']
    payload['user'] = tx_info['user']
    payload['discount'] = tx_info['discount']
    payload['platform'] = tx_info['platform']
    payload['transaction_type'] = tx_info['transaction_type']

    schedules_random_index = randint(0, 4)
    selected_schedule = schedules[schedules_random_index][0] if schedules_random_index <= len(schedules) else schedules[-1][0]
    selected_seating_type = schedules[schedules_random_index][1] if schedules_random_index <= len(schedules) else schedules[-1][1]

    current_datetime = datetime.now()
    selected_datetime = datetime.strptime(selected_schedule, '%d %b %Y %H:%M')

    if current_datetime > selected_datetime:
        selected_schedule = schedules[-1][0]
        selected_seating_type = schedules[-1][1]

    for r in payload['reservations']:
        seats_random_index = randint(0, 430)
        selected_seats = seats[seats_random_index] if seats_random_index <= len(seats) else seats[-1]

        r['seat'] = selected_seats if selected_seating_type == 'Reserved Seating' else ''
        r['show_datetime'] = selected_schedule

    transaction = _start_tx(device_id, payload)

    return jsonify({'id': transaction.key.id(), 'transaction': transaction.to_entity()})

@testcase.route('/tx/<tx_id>/', methods=['GET'])
def get_tx_info(tx_id):
    device_id = request.headers[DEVICE_ID]
    transaction = tx.query_info(device_id, tx_id)

    if device_id != 'TEST-CASE-STRESS-TEST':
        raise BadValueException('header', 'You must supply correct device_id.')

    if not transaction:
        raise NotFoundException(details={'tx_id': tx_id})

    return jsonify({'id' : tx_id, 'transaction': transaction.to_entity()})

@testcase.route('/tx/<tx_id>/state/', methods=['GET'])
def get_tx_state(tx_id):
    device_id = request.headers[DEVICE_ID]
    transaction = tx.query_info(device_id, tx_id)
    state, msg = tx.query_state(device_id, tx_id)

    if device_id != 'TEST-CASE-STRESS-TEST':
        raise BadValueException('header', 'You must supply correct device_id.')

    if not transaction:
        raise NotFoundException(details={'tx_id': tx_id})

    if (state, msg) == (None, None):
        raise NotFoundException(details={'tx_id': tx_id})

    tx_state = tx.to_state_str(state)

    if msg:
        return jsonify({'state': tx_state, 'message': msg})

    return jsonify({'state': tx_state})

# @testcase.route('/test-queue/')
# def test_queue():
#     queue_id = _incr_queue_id()
#     queue_name = 'ztcqueue%d' % queue_id
#
#     taskqueue.add(url=url_for('task.process_testcase'), queue_name=queue_name)
#
#     return jsonify(status='ok')
