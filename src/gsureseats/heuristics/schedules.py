from __future__ import absolute_import

import datetime
import logging

from decimal import Decimal
from operator import attrgetter
from uuid import uuid4 as uuid_gen

from google.appengine.api import memcache
from google.appengine.ext import deferred, ndb

from gsureseats import models, orgs
from gsureseats.admin import schedule_heuristics_notification
from gsureseats.heuristics import movies


log = logging.getLogger(__name__)

BATCH_SIZE = 100


def transform(schedule_dictionary_list):
    schedules = []
    schedule_batch = []
    correlator = ScheduleCorrelation()

    try:
        for schedule_dictionary in schedule_dictionary_list:
            schedule = correlator.convert_schedule(schedule_dictionary)

            schedule_batch.append(schedule)
            schedules.append(schedule)

            if len(schedule_batch) == BATCH_SIZE:
                ndb.put_multi(filter(None, schedule_batch))
                deferred.defer(delete_nonexistent_scheduleslot_feed_code, schedules, correlator.NEW_SCHEDULETIMESLOT_FEED_CODES, _queue='schedulesqueue')

                schedule_batch = []

        schedule_heuristics_notification(new_schedules=correlator.NEW_SCHEDULE_ADDED,
                existing_from_cache=correlator.EXISTING_SCHEDULE_FROM_CACHE,
                existing_from_datastore=correlator.EXISTING_SCHEDULE_FROM_DATASTORE,
                existing_from_datastore_variants=correlator.EXISTING_SCHEDULES_FROM_DATASTORE_WITH_VARIANTS,
                cached_by_code=correlator.CACHE_MATCH_BY_CODE,
                fetch_by_code=correlator.DS_MATCH_BY_CODE,
                fetch_variants=correlator.DS_MATCH_SCHEDULE_WITH_VARIANT,
                fetch_no_variants=correlator.DS_MATCH_SCHEDULE_WITHOUT_VARIANT,
                multi_errors=correlator.TOO_MANY_MATCHES)
    except Exception as e:
        log.warn("ERROR!, transform...")
        log.error(e)

    if len(schedule_batch) > 0:
        ndb.put_multi(filter(None, schedule_batch))
        deferred.defer(delete_nonexistent_scheduleslot_feed_code, schedules, correlator.NEW_SCHEDULETIMESLOT_FEED_CODES, _queue='schedulesqueue')
    log.info('schedules_in: {}'.format(schedules))
    return schedules

def get_movie_entity(uuid, movie_title):
    movie = query_movie_with_title(movie_title)

    # fallback: create the movie entry. uncomment if needed, but for now we will not use this to avoid duplicate movies.
    # if not movie:
    #     log.warn("Creating entity for '%s'; couldn't find match in datastore" % movie_title)
    #
    #     movie_dict = {'movie_title': movie_title}
    #     org_key = ndb.Key(models.Feed, str(uuid))
    #     movie = movies.correlate_dict_to_movie_entity(org_key, movie_dict)
    #
    #     if movie:
    #         movie.put()
    #         cache_movie_title(movie)

    return movie

def get_movie_entity_by_feed_correlation(uuid, movie_id):
    feed_key = ndb.Key(models.Feed, str(uuid))
    queryset = models.Movie.query(models.Movie.movie_correlation.org_key==feed_key, models.Movie.movie_correlation.movie_id==movie_id)
    movie = queryset.get()

    return movie

def parse_screening(screening_date_time):
    screening_date_time = screening_date_time.replace('MN', 'AM').replace('NN', 'PM')

    try:
        screening = datetime.datetime.strptime(screening_date_time, '%m/%d/%Y %I:%M:%S %p')
    except ValueError:
        try:
            screening = datetime.datetime.strptime(screening_date_time, '%m/%d/%Y %I:%M %p')
        except ValueError:
            screening = None

    return screening

def to_cache_key(*tup):
    matcher_string = ':'.join([unicode(o) for o in tup])

    return "schedule::matcher(%d)[%s]" % (len(tup), matcher_string)

def query_movie_with_title(title):
    correlation_titles = movies.get_all_correlation_titles(title)
    cached_titles = filter(None, [memcache.get("movie::correlation_title::%s" % t) for t in correlation_titles]) # check cache against correlation title.

    if cached_titles:
        return ndb.Key(urlsafe=cached_titles[0]).get()

    # hit datastore, query against correlation title.
    queryset = models.Movie.query(models.Movie.correlation_title.IN(correlation_titles), models.Movie.is_active==True)

    if queryset.count() == 1:
        movie = queryset.get()

        for t in correlation_titles:
            memcache.set("movie::correlation_title::%s" % t, movie.key.urlsafe())

        return movie

    return None

def cache_movie_title(movie):
    memcache.set("movie::correlation_title::%s" % movie.correlation_title, movie.key.urlsafe())

def delete_nonexistent_scheduleslot_feed_code(schedules, feed_codes):
    try:
        for schedule in filter(None, schedules):
            slots = [slot for slot in schedule.slots]

            for slot in slots:
                if slot.feed_code not in feed_codes:
                    log.warn("DELETE!, ScheduleSlot, feed_code: %s, movie_id: %s, start_time: %s..." % (slot.feed_code, slot.movie.id(), slot.start_time))

                    schedule.slots.remove(slot)

        ndb.put_multi(filter(None, schedules))
    except Exception, e:
        log.warn("ERROR!, delete_nonexistent_scheduleslot_feed_code, something went wrong...")
        log.error(e)

def rename_seating_type(seat_type, theater, uuid):
    if str(uuid) == str(orgs.AYALA_MALLS):
        if seat_type == 'Guaranteed Seats':
            seat_type = 'Reserved Seating'

    return seat_type


class ScheduleCorrelation:
    def __init__(self):
        self.NEW_SCHEDULE_ADDED = 0
        self.EXISTING_SCHEDULE_FROM_CACHE = 0
        self.EXISTING_SCHEDULE_FROM_DATASTORE = 0
        self.EXISTING_SCHEDULES_FROM_DATASTORE_WITH_VARIANTS = 0
        self.CACHE_MATCH_BY_CODE = 0
        self.DS_MATCH_BY_CODE = 0
        self.DS_MATCH_SCHEDULE_WITH_VARIANT = 0
        self.DS_MATCH_SCHEDULE_WITHOUT_VARIANT = 0
        self.TOO_MANY_MATCHES = 0

        self.NOT_FOUND_MOVIES = []
        self.NEW_SCHEDULETIMESLOT_FEED_CODES = []

    def convert_schedule(self, s):
        schedule_code = str(s['id'])
        show_date_time = parse_screening(s['screening'])
        movie_id = s.get('movie_id', None)
        movie_title = s['movie_title']
        theater_code = s.get('theater_code', None)
        cinema_id = s.get('cinema_id', None)
        cinema_code = s.get('cinema_code', None)
        cinema_name = s['cinema_name']
        price = Decimal(s['price'])
        seat_type = s.get('seat_type', None)
        variant = s.get('variant', None)
        uuid = s['uuid']

        if show_date_time is None:
            log.debug('SKIP!, schedule_code: %s - theater_code: %s - cinema: %s - movie: %s' % (schedule_code, theater_code, cinema_name, movie_title))
            log.error('ERROR!, Wrong Screening Time Format: %s' % s['screening'])

            return None

        if movie_title in self.NOT_FOUND_MOVIES:
            log.debug("SKIP!, theater_code %s with movie %s is not found in the previous schedule convertion..." % (theater_code, movie_title))

            return None

        theaterorg_key = ndb.Key(models.TheaterOrganization, str(uuid))
        theaterorg = theaterorg_key.get()

        if not theaterorg or theaterorg is None:
            log.debug("SKIP!, we can't match theaterorg...")

            return None

        movie = get_movie_entity_by_feed_correlation(uuid, movie_id) if movie_id else get_movie_entity(uuid, movie_title)
        theater = query_theater_with_code(theater_code, theaterorg=theaterorg)

        if not movie or movie is None:
            log.debug("SKIP!, we can't match movie...")

            self.NOT_FOUND_MOVIES.append(movie_title)

            return None

        if not theater or theater is None:
            log.debug("SKIP!, we can't match theater...")

            return None

        has_cinema = query_cinema_in_theater(theater, cinema_name, cinema_code)

        if not has_cinema or has_cinema is None:
            log.debug("SKIP!, we can't match cinema...")

            return None

        if not cinema_code:
            cinema_code = cinema_name

        show_date = show_date_time.date()
        show_time = show_date_time.time()
        seat_type = rename_seating_type(seat_type, theater, uuid)

        log.debug("Looking for (%s, %s, %s, %s, %s, %s, %s, %s, %s)" % (schedule_code, show_date, show_time, movie_id, theaterorg.name, theater_code, cinema_code, variant, seat_type))

        cached_sched_key_tuple = memcache.get(to_cache_key(show_date, movie_id, variant, theaterorg.name, theater_code, cinema_code, seat_type))

        if cached_sched_key_tuple:
            log.debug("Schedule cache hit (%s, %s, %s, %s, %s, %s, %s, %s, %s)" % (schedule_code, show_date, show_time, movie_id, theaterorg.name, theater_code, cinema_code, variant, seat_type))

            self.EXISTING_SCHEDULE_FROM_CACHE += 1

            schedule = ndb.Key(urlsafe=cached_sched_key_tuple).get()

            if schedule_code not in schedule._feed_codes():
                schedule.slots.append(models.ScheduleTimeSlot(feed_code=schedule_code, start_time=show_time,
                        movie=movie.key, variant=variant, seating_type=seat_type, price=price))
            else:
                slot = schedule.get_feed_code(schedule_code)
                slot.feed_code = schedule_code
                slot.start_time = show_time
                slot.movie = movie.key
                slot.variant = variant
                slot.seating_type = seat_type
                slot.price = price

            log.debug("Cached Schedule: %r", schedule)
        else:
            schedule = models.Schedule.find_schedule(theater.key, cinema_code, show_date)

            if not schedule:
                self.NEW_SCHEDULE_ADDED += 1

                schedule = models.Schedule.create_schedule(cinema=cinema_code, parent=theater.key, show_date=show_date)
                schedule.slots = [models.ScheduleTimeSlot(feed_code=schedule_code, start_time=show_time,
                        movie=movie.key, variant=variant, seating_type=seat_type, price=price)]
                schedule.put()

                log.debug("Caching (%s, %s, %s, %s, %s, %s, %s, %s, %s)" % (schedule_code, show_date, show_time, movie_id, theaterorg.name, theater_code, cinema_code, variant, seat_type))

                memcache.set(to_cache_key(show_date, movie_id, variant, theaterorg.name, theater_code, cinema_code, seat_type), schedule.key.urlsafe())
            else:
                self.EXISTING_SCHEDULE_FROM_DATASTORE += 1

                if schedule_code not in schedule._feed_codes():
                    schedule.slots.append(models.ScheduleTimeSlot(feed_code=schedule_code, start_time=show_time,
                            movie=movie.key, variant=variant, seating_type=seat_type, price=price))
                else:
                    slot = schedule.get_feed_code(schedule_code)
                    slot.feed_code = schedule_code
                    slot.start_time = show_time
                    slot.movie = movie.key
                    slot.variant = variant
                    slot.seating_type = seat_type
                    slot.price = price

        self.NEW_SCHEDULETIMESLOT_FEED_CODES.append(schedule_code)

        return schedule

    def query_for_schedule_correlation(self, theater_key, movie_key, show_date, variant, theater_code, cinema_code, seat_type):
        return models.Schedule.find_schedule(theater_key, cinema_code, show_date)

    def query_theater_with_code(self, theater_code, theaterorg=None):
        if theaterorg and theaterorg is not None:
            cache_key = "theaterorg::%s::theater::theater_code::%s" % (theaterorg.key.id(), theater_code)
        else:
            cache_key = "theater::theater_code::%s" % theater_code

        theater_key = memcache.get(cache_key)

        if theater_key:
            self.CACHE_MATCH_BY_CODE += 1

            return ndb.Key(urlsafe=theater_key).get()

        log.debug("Matching against theater with code %s" % theater_code)

        if theaterorg and theaterorg is not None:
            queryset = models.Theater.query(models.Theater.org_theater_code==theater_code, ancestor=theaterorg.key)
        else:
            queryset = models.Theater.query(models.Theater.org_theater_code==theater_code)

        if queryset.count() == 0:
            return None

        theater = queryset.get()
        self.DS_MATCH_BY_CODE += 1

        memcache.set(cache_key, theater.key.urlsafe())

        return theater

    def query_cinema_in_theater(self, theater, cinema_name, cinema_code):
        if cinema_code:
            if cinema_code not in [c.name for c in theater.cinemas]:
                return False
        else:
            if cinema_name not in [c.name for c in theater.cinemas]:
                return False

        return True


DEFAULT_SCHEDULE_CORRELATION = ScheduleCorrelation()
convert_schedule = DEFAULT_SCHEDULE_CORRELATION.convert_schedule
query_theater_with_code = DEFAULT_SCHEDULE_CORRELATION.query_theater_with_code
query_cinema_in_theater = DEFAULT_SCHEDULE_CORRELATION.query_cinema_in_theater
