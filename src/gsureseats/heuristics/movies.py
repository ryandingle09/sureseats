import logging
import re
import string

from operator import attrgetter
from uuid import uuid4 as uuid_gen

from flask import url_for

from google.appengine.api import taskqueue, memcache
from google.appengine.ext import ndb

from gsureseats import orgs
from gsureseats.admin import movie_heuristics_notification, movie_multiple_correlation_notification
from gsureseats.models import Feed, FeedCorrelation, Movie
from gsureseats.util import roman_to_numeric


log = logging.getLogger(__name__)

CORRELATION_VERSION = 4
BATCH_SIZE = 50

FILTER_WORDS = ['the', 'a', 'an', 'or', 'and', 'is', 'in']
ROMAN_NUMERAL_MATCH = '^M{0,4}(CM|CD|D?C{0,3})(XC|XL|L?X{0,3})(IX|IV|V?I{0,3})$'

variants = ['3D-HFR', '2D', '3D', '4DX', 'ATMOS', '35MM', 'HFR', 'IMAX']
suffix_single_pattern = '(' + '|'.join(variant for variant in variants) + ')'
suffix_multiple_pattern = '(' + '|'.join(variant for variant in variants) + ')\s*/\s*(' + '|'.join(variant for variant in variants) + ')'

SUFFIX_VARIANCE_MATCH = r'\((?i)(' + suffix_multiple_pattern + '|' + suffix_single_pattern + ')\)$'
PREFIX_VARIANCE_MATCH = r'^\((?i)(' + suffix_multiple_pattern + '|' + suffix_single_pattern + ')\)'
IN_SUFFIX_VARIANCE_MATCH = r'\((?i)?in\s+(' + suffix_multiple_pattern + '|' + suffix_single_pattern + ')\)?'
SUFFIX_YEAR_MATCH = r'\((?i)(\d{4})\)$'


def build_whitelist_blacklist_check(blacklist, whitelist):
    log.debug("build_whitelist_blacklist_check, Blacklist: %s, whitelist: %s" % (blacklist, whitelist))

    def check_field(m, field):
        return field in m and (field not in blacklist) and (field in whitelist or not whitelist)

    return check_field

def transform(per_org_payloads):
    key_to_movie = {}
    payload_movie_matches = {}
    feed_ids = per_org_payloads.keys()
    feeds = ndb.get_multi([ndb.Key(Feed, feed_id) for feed_id in feed_ids])
    prioritized_feeds = reversed(sorted(feeds, key=attrgetter('heuristics_priority')))
    correlator = MovieCorrelation()


    for f in prioritized_feeds:
        feed_id = f.key.id()
        field_blacklist = f.movie_fields_blacklist
        field_whitelist = f.movie_fields_whitelist
        check_field = build_whitelist_blacklist_check(field_blacklist, field_whitelist)
        movie_payload = per_org_payloads[feed_id]

        for m in movie_payload:
            movie = correlator.correlate(f.key, m, f)

            if not movie:
                log.warn("SKIP!, heuristics, movies, transform, movie not found...")

                continue

            movie_key = movie.key.id()

            if movie_key not in key_to_movie:
                key_to_movie[movie_key] = movie

            variant = get_variant(m)

            if not check_field(m, 'synopsis'):
                m.pop('synopsis', None)

            if not check_field(m, 'cast'):
                m.pop('cast', None)

            if not check_field(m, 'release_date'):
                m.pop('release_date', None)

            if not check_field(m, 'genre'):
                m.pop('genre', None)

            if not check_field(m, 'runtime_mins'):
                m.pop('runtime_mins', None)

            if not check_field(m, 'advisory_rating'):
                m.pop('advisory_rating', None)

            if not check_field(m, 'rating'):
                m.pop('rating', None)

            if not check_field(m, 'image_url'):
                m.pop('image_url', None)

            movie.bind_org_title(feed_id, variant, m['movie_title'])
            
            if movie_key not in payload_movie_matches:
                payload_movie_matches[movie_key] = m
            else:
                payload_movie_matches[movie_key].update(m)

    try:
        movie_heuristics_notification(new_movies=correlator.NEW_MOVIES_ADDED,
                existing_from_cache=correlator.EXISTING_MOVIES_FROM_CACHE,
                existing_from_datastore=correlator.EXISTING_MOVIES_FROM_DATASTORE,
                correlated_by_title=correlator.DS_CORRELATED_BY_TITLE,
                correlated_by_id=correlator.DS_CORRELATED_BY_ORG_AND_ID,
                cached_by_id=correlator.CACHE_MATCH_BY_ID,
                cached_by_title=correlator.CACHE_MATCH_BY_TITLE,
                movies_not_created=correlator.FEED_NO_CREATION,
                multi_correlations=correlator.MULTIPLE_CORRELATION,
                movies_added='\n'.join(correlator.MOVIES_ADDED).encode('utf-8'),
                movies_not_match_by_correlation_id_or_title='\n'.join(correlator.MOVIES_NOT_MATCH_BY_CORRELATION_ID_OR_TITLE).encode('utf-8'),
                movies_with_multiple_correlation='\n'.join(correlator.MOVIES_WITH_MULTIPLE_CORRELATION).encode('utf-8'))
    except Exception as e:
        log.warn("ERROR!, heuristics, movies, transform, failed to send email notification...")
        log.error(e)
    log.info('payload_movie_matches: {}'.format(payload_movie_matches.items()))
    return transform_merged_payloads(map(lambda (m_key, m): (key_to_movie[m_key], m), payload_movie_matches.items()))


def get_advisory_rating(advisory_rating, rating):
    try:
        if advisory_rating:
            result = advisory_rating
        else:
            result = rating
    except:
        log.warning('Warning in getting Advisory Rating: No rating found.')
        result = ''
    return result


def transform_merged_payloads(raw_movies):
    movie_list = []
    movie_batch = []

    for movie, m in raw_movies:
        try:
            if movie.is_locked:
                log.warn("SKIP!, heuristics, movies, transform_merged_payloads, %s is locked..." % movie.canonical_title)
            else:
                movie.synopsis = m['synopsis'] if 'synopsis' in m else ''
                movie.genre = m['genre'] if 'genre' in m else ''
                movie.cast = m['cast'] if 'cast' in m else []
                movie.advisory_rating = m['advisory_rating'] if 'advisory_rating' in m else m['rating']
                # movie.advisory_rating = get_advisory_rating(m['advisory_rating'], m['rating'])
                movie.runtime_mins = m['runtime_mins'] if 'runtime_mins' in m else 0.0
                movie.poster_portrait = m['image_url'] if 'image_url' in m else ''

                if 'release_date' in m:
                    movie.release_date = m['release_date']
        except AttributeError as e:
            log.warn("ERROR!, heuristics, movies, transform_merged_payloads, AttributeError...")
            log.error(e)
        except Exception as e:
            log.warn("ERROR!, heuristics, movies, transform_merged_payloads...")
            log.error(e)

        movie_batch.append(movie)
        movie_list.append(movie)

        if len(movie_batch) == BATCH_SIZE:
            ndb.put_multi(movie_batch)
            movie_batch = []

    ndb.put_multi(movie_batch)
    log.info('raw_movies: {}'.format(raw_movies))
    log.info('movie_list: {}'.format(movie_list))
    return movie_list

def get_variant(movie_dict):
    if 'variant' in movie_dict:
        return movie_dict['variant']

    return variant_from_title(movie_dict['movie_title'])

# strip variance in canonical_title
def get_canonical_title(title):
    canon = title.strip()
    canon = strip_suffix_variance(canon)
    canon = strip_prefix_variance(canon)
    canon = strip_in_suffix(canon)
    canon = canon.strip()

    return canon

def strip_punct(w):
    return filter(lambda c: c not in string.punctuation, w)

def convert_roman_numeral(w):
    if re.search(ROMAN_NUMERAL_MATCH, w):
        return str(roman_to_numeric(w))

    return w

def strip_part_specifier_before_romans(words):
    new_words = []

    for w in words:
        if re.search(ROMAN_NUMERAL_MATCH, w) and new_words and new_words[-1].lower() == 'part':
            new_words.pop()

        new_words.append(w)

    return new_words

def strip_suffix_variance(title):
    return re.sub(SUFFIX_VARIANCE_MATCH, '', title)

def strip_prefix_variance(title):
    return re.sub(PREFIX_VARIANCE_MATCH, '', title)

def strip_in_suffix(title):
    return re.sub(IN_SUFFIX_VARIANCE_MATCH, '', title)

def strip_suffix_year(title):
    return re.sub(SUFFIX_YEAR_MATCH, '', title)

def variant_from_title(title):
    match = re.search(SUFFIX_VARIANCE_MATCH, title)

    if match:
        return match.group(1).upper()

    match = re.search(PREFIX_VARIANCE_MATCH, title)

    if match:
        return match.group(1).upper()

    match = re.search(IN_SUFFIX_VARIANCE_MATCH, title)

    if match:
        return match.group(1).upper()

    return None

def get_all_correlation_titles(title):
    return [get_correlation_title(v, title) for v in range(1, CORRELATION_VERSION + 1)]

def get_correlation_title(v, title):
    if title.strip() == '':
        return title

    title = title.strip()
    title = strip_prefix_variance(title)
    title = strip_suffix_variance(title)
    title = strip_in_suffix(title)
    title = strip_suffix_year(title)

    title_words = title.split(' ')
    title_words = filter(None, [strip_punct(w) for w in title_words])

    if v == 2 or v == 4:
        title_words = strip_part_specifier_before_romans(title_words)
        title_words = [convert_roman_numeral(w) for w in title_words]

    title_words = [w.lower() for w in title_words]
    title_words = filter(lambda w: w != '', title_words)
    title_words = filter(lambda w: w not in FILTER_WORDS, title_words)

    if v <= 2:
        correlation_title = "%s:%s" % (v, '_'.join(title_words))
    else:
        correlation_title = "%s:%s" % (v, ''.join(title_words))

    return correlation_title


class MovieCorrelation:
    def __init__(self):
        self.NEW_MOVIES_ADDED = 0
        self.EXISTING_MOVIES_FROM_CACHE = 0
        self.EXISTING_MOVIES_FROM_DATASTORE = 0
        self.DS_CORRELATED_BY_TITLE = 0
        self.DS_CORRELATED_BY_ORG_AND_ID = 0
        self.CACHE_MATCH_BY_ID = 0
        self.CACHE_MATCH_BY_TITLE = 0
        self.FEED_NO_CREATION = 0
        self.MULTIPLE_CORRELATION = 0

        self.MOVIES_ADDED = []
        self.MOVIES_NOT_MATCH_BY_CORRELATION_ID_OR_TITLE = []
        self.MOVIES_WITH_MULTIPLE_CORRELATION = []

        self.movie_cache = {}

    def cached(self, key):
        if key not in self.movie_cache:
            self.movie_cache[key] = key.get()

        return self.movie_cache[key]

    def add_movie_variant(self, movie, variant):
        if movie.variants and variant not in movie.variants:
            movie.variants.append(variant)
        elif not movie.variants:
            movie.variants = [variant]

        return movie

    def correlate(self, org_key, movie_dict, feed=None):
        if not feed:
            log.debug("heuristics, MovieCorrelation, correlate, getting feed from org_key %s: %s..." % (org_key, feed))

            feed = org_key.get()

        log.debug("heuristics, MovieCorrelation, correlate, feed name %s!..." % feed.name)

        try:
            correlation_id = str(movie_dict['id']) if 'id' in movie_dict else None
        except UnicodeEncodeError as e:
            log.warn("ERROR!, heuristics, movies, correlate, correlation_id UnicodeEncodeError...")
            log.error(e)

            correlation_id = unicode(movie_dict['id']) if 'id' in movie_dict else None
        except Exception as e:
            log.warn("ERROR!, heuristics, MovieCorrelation, correlate, correlation_id...")
            log.error(e)

            correlation_id = None

        variant = get_variant(movie_dict)
        correlation_titles = get_all_correlation_titles(movie_dict['movie_title'])
        corr = FeedCorrelation(movie_id=correlation_id, org_key=org_key, variant=variant) if correlation_id else None

        log.debug("heuristics, MovieCorrelation, correlate, params: id %s correlation_titles %s..." % (correlation_id, correlation_titles))

        cached_movie_id = memcache.get("movie::movie_correlation::org_key/movie_id[%s,%s]" % (org_key.id(), correlation_id))
        cached_titles = filter(None, [memcache.get("movie::correlation_title::%s" % t) for t in correlation_titles])

        if cached_movie_id or cached_titles:
            self.EXISTING_MOVIES_FROM_CACHE += 1

            if cached_movie_id:
                log.debug("heuristics, MovieCorrelation, correlate, matched against cached feed movie_id %s..." % correlation_id)

                movie = self.cached(ndb.Key(urlsafe=cached_movie_id))
                self.CACHE_MATCH_BY_ID += 1
            else: # cached_title
                log.debug("heuristics, MovieCorrelation, correlate, matched against cached correlation title(s) %s..." % correlation_titles)

                # We don't care *which* title; we assume that the
                # different versions all point to the same entity
                movie = self.cached(ndb.Key(urlsafe=cached_titles[0]))
                self.CACHE_MATCH_BY_TITLE += 1

            log.debug("heuristics, MovieCorrelation, correlate, found correlated item in cache: %s..." % movie.key.id())

            if corr and corr not in movie.movie_correlation:
                # We were correlated by title, cache the correlation title
                movie.movie_correlation.append(corr)

            if variant: # HACK: Ensure variant specified is in our variants list
                movie = self.add_movie_variant(movie, variant)

            return movie

        if correlation_id:
            movie_queryset = Movie.query(ndb.OR(ndb.AND(Movie.movie_correlation.org_key==org_key,
                    Movie.movie_correlation.movie_id==correlation_id), Movie.correlation_title.IN(correlation_titles)),
                    Movie.is_active==True)

            if movie_queryset.count() == 1:
                self.DS_CORRELATED_BY_ORG_AND_ID += 1
        else:
            movie_queryset = Movie.query(Movie.correlation_title.IN(correlation_titles), Movie.is_active==True)

            if movie_queryset.count() == 1:
                self.DS_CORRELATED_BY_TITLE += 1

        movie = movie_queryset.get()

        if movie_queryset.count() == 1:
            self.EXISTING_MOVIES_FROM_DATASTORE += 1

            movie.canonical_title = get_canonical_title(movie_dict['movie_title'])
            movie.correlation_title = correlation_titles

            if corr and corr not in movie.movie_correlation:
                movie.movie_correlation.append(corr)

                for t in movie.correlation_title:
                    memcache.set("movie::correlation_title::%s" % t, movie.key.urlsafe())
            elif corr:
                # We were correlated by ID, cache the correlation ID
                memcache.set("movie::movie_correlation::org_key/movie_id[%s,%s]" % (org_key.id(), correlation_id), movie.key.urlsafe())

                # Cache the correlation_title as well for this entry
                for t in movie.correlation_title:
                    memcache.set("movie::correlation_title::%s" % t, movie.key.urlsafe())

            if variant: # HACK: Ensure variant specified is in our variants list
                movie = self.add_movie_variant(movie, variant)
        elif movie_queryset.count() == 0 and feed.allow_movie_creation:
            self.NEW_MOVIES_ADDED += 1

            movie = Movie(key=ndb.Key(Movie, str(uuid_gen())), per_org_titlemap={})
            movie.canonical_title = get_canonical_title(movie_dict['movie_title'])
            movie.correlation_title = correlation_titles
            movie.movie_correlation = [corr] if corr else []
            movie.variants = [variant] if variant else []

            for t in correlation_titles:
                memcache.set("movie::correlation_title::%s" % t, movie.key.urlsafe())

            self.movie_cache[movie.key] = movie
        elif not feed.allow_movie_creation:
            log.warn("SKIP!, heuristics, MovieCorrelation, correlate, '%s', this feed is not allowed to create new movies..." % correlation_titles)

            self.FEED_NO_CREATION += 1

            return None
        else:
            log.warn("FIXME!, heuristics, MovieCorrelation, correlate, found multiple matching correlation_titles: %s!..." % correlation_titles)

            self.MULTIPLE_CORRELATION += 1
            self.MOVIES_WITH_MULTIPLE_CORRELATION.append('feed: %s, movie_title: %s' % (feed.name, movie_dict['movie_title']))

            movie_multiple_correlation_notification(movies_with_multiple_correlation='\n'.join(self.MOVIES_WITH_MULTIPLE_CORRELATION).encode('utf-8'))

            raise ValueError("Multiple correlated Movies for %s..." % movie_dict)

        return movie


DEFAULT_MOVIE_CORRELATION = MovieCorrelation()
correlate_dict_to_movie_entity = DEFAULT_MOVIE_CORRELATION.correlate
