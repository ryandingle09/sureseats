import logging
import os

from datetime import datetime

from google.appengine.ext import deferred
from google.appengine.ext.ndb import Key, transactional

import state_machine

from .query import query_reservation_reference
from .scheduler import reschedule, cancel_reaping

from gsureseats.models import Listener, DATETIME_FORMAT
from gsureseats.settings import CLIENT_INITIATED_REAP_TIMEOUT


log = logging.getLogger(__name__)

CHANNEL_NAME = 'payment-complete'
LISTENER_ID = '$1$payment_completion_listener'
BUY_TICKET_TIMEOUT = CLIENT_INITIATED_REAP_TIMEOUT
RESERVE_TICKET_TIMEOUT = CLIENT_INITIATED_REAP_TIMEOUT


def listener_bind():
    version = os.environ['CURRENT_VERSION_ID'].split('.')
    major_version = version[0]

    if Listener.query(Listener.event_channel==CHANNEL_NAME, Listener.version==major_version,
            Listener.listener_id==LISTENER_ID).count() == 0:
        l = Listener()
        l.event_channel = CHANNEL_NAME
        l.version = major_version
        l.listener_id = LISTENER_ID

        l.callback = payment_completion_listener
        l.put()

        log.info("client_initiated, listener_bind, bound listener for channel: %s...", CHANNEL_NAME)

def bind_listeners(tx):
    log.debug("client_initiated, bind_listeners, (deferred) Binding listener for TX %s...", tx.key.id())

    deferred.defer(listener_bind)

def payment_completion_listener(outstanding):
    for e in outstanding:
        log.debug("client_initiated, payment_completion_listener, processing: %s", e)

        if e.payload:
            if 'reference' not in e.payload:
                continue

            if 'claim-code' not in e.payload:
                continue

            if 'claim-date' not in e.payload:
                continue

            reference = e.payload['reference']
            claim_code = e.payload['claim-code']
            claim_date = convert_date(e.payload['claim-date'])
            tx = query_reservation_reference(reference)

            if tx:
                log.debug("client_initiated, payment_completion_listener, completing: %s...", tx.key.id())

                trigger_completion(tx, claim_code, claim_date)

                log.debug("client_initiated, payment_completion_listener, triggered completion...")

            e.read = True

@transactional
def trigger_completion(tx, claim_code, claim_date):
    if tx.state != state_machine.state.TX_CLIENT_PAYMENT_HOLD:
        log.error("ERROR!, client_initiated, trigger_completion, attempt to complete TX %s, which is not in TX_CLIENT_PAYMENT_HOLD; callback in error?", tx.key.id())

        return

    tx.workspace['RSVP:claimcode'] = claim_code
    tx.workspace['RSVP:claimdate'] = claim_date

    cancel_reaping(tx)
    state_machine.transition_state(tx, state_machine.state.TX_CLIENT_PAYMENT_HOLD, state_machine.state.TX_GENERATE_TICKET)

    reschedule(tx)

def convert_date(d):
    return datetime.strptime(d, DATETIME_FORMAT)
