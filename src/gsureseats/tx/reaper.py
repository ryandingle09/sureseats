import logging

from google.appengine.ext.ndb import transactional

from .actions import cancel
from .query import query_info

from gsureseats.exceptions.api import AlreadyCancelledException, TransactionConflictException


log = logging.getLogger(__name__)


@transactional
def reap_stale_tx(device_id, tx_id):
    tx = query_info(device_id, tx_id)
    tx.workspace['reap_task'] = None

    try:
        log.debug("reap_stale_tx, cancel TX...")

        cancel(device_id, tx_id)
    except TransactionConflictException, ignored:
        log.warn("reap_stale_tx, reap task fired on a transaction in the wrong state; may have been retried?")
    except AlreadyCancelledException, ignored:
        log.warn("reap_stale_tx, reap task fired on an already cancelled transaction. Will ignore...")

    tx.was_reaped = True
    tx.put()
