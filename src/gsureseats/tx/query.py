import logging
import traceback
from google.appengine.ext.ndb import Key
from google.appengine.ext import db

from gsureseats.models import Device, ReservationTransaction, Theater, TheaterOrganization


log = logging.getLogger(__name__)


def query_info(device_id, tx_id):
    log.debug("query_info, querying for TX entity %s..." % tx_id)

    tx = ReservationTransaction.get_by_id(tx_id, parent=Key(Device, device_id))
    return tx


def query_state(device_id, tx_id):
    log.debug("query_state, querying for TX state %s..." % tx_id)

    tx = ReservationTransaction.get_by_id(tx_id, parent=Key(Device, device_id))

    if not tx:
        return None, None
    msg = tx.workspace['msg'] if 'msg' in tx.workspace else None
    return tx.state, msg


def query_reservation_reference(reference):
    q = ReservationTransaction.query(ReservationTransaction.reservation_reference==reference)

    if q.count() == 1:
        log.debug("query_reservation_reference, found TX, reference: %s..." % reference)
        return q.fetch(1)[0]
    elif q.count() > 1:
        log.warn("SKIP!, query_reservation_reference, duplicate reservation_reference.")
        return None
    else:
        log.warn("SKIP!, query_reservation_reference, something went wrong.")
        return None


def query_by_claimcode(claim_code):
    log.debug("query_by_claimcode, querying for TX entity by claim_code %s..." % claim_code)
    try:
        q = ReservationTransaction.query(ReservationTransaction.ticket.code==claim_code)
        log.debug("query_by_claimcode, found TX, claim_code: %s..." % claim_code)
        tx = q.get()
        return tx
    except Exception as e:
        log.debug("query_by_claimcode, not found TX, claim_code: %s..." % claim_code)
        log.debug("query_by_claimcode, error: %s..." % e)
        log.debug(traceback.format_exc())

