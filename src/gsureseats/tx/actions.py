import logging

from google.appengine.api import memcache
from google.appengine.ext.ndb import Key, transactional
from google.appengine.api import taskqueue

from .payment import prepare_payment, cancel_payment
from .query import query_info
from .reservation import prepare_reservation, cancel_reservation
from .scheduler import reschedule, cancel_reaping
from .state_machine import state, to_state_str, transition_state

from gsureseats.exceptions.api import TransactionConflictException, AlreadyCancelledException, NotFoundException
from gsureseats.models import ReservationTransaction


log = logging.getLogger(__name__)

MAX_QUEUES = 10

state_after_update = {
    state.TX_PAYMENT_ERROR: state.TX_RESERVATION_OK,
    state.TX_RESERVATION_ERROR: state.TX_STARTED,
    state.TX_PREPARE_ERROR: state.TX_START
}


def _incr_queue_id(client, tx_type):
    while True:
        queue_name = 'tx_queue_id_reserve' if tx_type == 'RESERVE' else 'tx_queue_id'
        queue_id = client.gets(queue_name)

        if queue_id == None:
            client.set(queue_name, 1)

            return 1

        new_queue_id = (queue_id % MAX_QUEUES) + 1 # cycle from 1 to 10.

        if client.cas(queue_name, new_queue_id):
            return new_queue_id

@transactional
def start(device_id, tx_info):
    tx = ReservationTransaction.create_transaction(device_id, tx_info)
    tx_id = tx.key.id()
    tx_type = tx.transaction_type
    client = memcache.Client()
    state_key = "%s::s" % tx_id
    client.set(state_key, state.TX_START)
    tx.state = state.TX_START
    queue_id = _incr_queue_id(client, tx_type)
    tx.workspace['queue_id'] = queue_id
    tx.put()

    return tx

@transactional
def start_stopgap(device_id, tx_info):
    tx = ReservationTransaction.create_transaction_raw(device_id, tx_info)
    tx_id = tx.key.id()
    client = memcache.Client()
    state_key = "%s::s" % tx_id
    client.set(state_key, state.TX_START)
    tx.state = state.TX_START
    tx.put()

    return tx

@transactional
def update(device_id, tx_id, tx_info={}):
    tx = query_info(device_id, tx_id)

    if not tx:
        raise NotFoundException(details={'tx_id': tx_id})

    if tx.state == state.TX_CANCELLED:
        raise AlreadyCancelledException()

    if tx.state not in state_after_update:
        raise TransactionConflictException(state_after_update.keys())

    cancel_reaping(tx)

    if tx_info:
        payment_info = dict(tx_info['payment'])
        payment_info.pop('type', None)
        same_payment_info = (payment_info == tx.payment_info)

        if 'reservations' in tx_info and tx.state == state.TX_PAYMENT_ERROR:
            raise TransactionConflictException([ to_state_str(state.TX_RESERVATION_ERROR) ])

        old_reservations = tx.reservations
        tx.copy_api_entity(tx_info)
        same_reservation_info = (old_reservations == tx.reservations)

        log.debug("Same payment info? %s", same_payment_info)
        log.debug("Same reservation info? %s", same_reservation_info)

        if not same_payment_info or not same_reservation_info:
            log.debug("Triggering update callbacks")

            tx.trigger_update(same_reservation=same_reservation_info, same_payment=same_payment_info)

        # HACK: Sneak in prepare step (even though we're technically not in TX_PREPARE)
        #
        # We need to do this because we depend particularly on the
        # state of the workspace being set in the prepare_* methods,
        # which in turn depends on the passed in transaction info. If
        # the transaction info changes, we need to re-run the
        # preparation

        r, msg = 'success', None

        if tx.state in [state.TX_RESERVATION_ERROR] and not same_reservation_info:
            r, msg = prepare_reservation(tx)

        # HACK: Sneak in error message into tx.workspace['msg'] and don't reschedule
        if r != 'success':
            tx.workspace['msg'] = msg
            tx.put()
            return tx

        if tx.state in [state.TX_PAYMENT_ERROR, state.TX_RESERVATION_ERROR] and not same_payment_info:
            r, msg = prepare_payment(tx)

        # HACK: Sneak in error message into tx.workspace['msg'] and don't reschedule
        if r != 'success':
            tx.workspace['msg'] = msg
            tx.put()
            return tx

        # NB: Setting the error message during transaction update with
        # the pre-existing error state (either TX_PAYMENT_ERROR or
        # TX_RESERVATION_ERROR) is semantically wrong because the
        # error message is related to transaction preparation (for
        # instance, invalid MPASS username) and could be unexpected by
        # the client. This should be fixed in a newer public API.

    new_tx_state = state_after_update[tx.state]

    tx.workspace['msg'] = None # clear msg from workspace.

    transition_state(tx, tx.state, new_tx_state)
    reschedule(tx)
    return tx

@transactional
def cancel(device_id, tx_id):
    tx = query_info(device_id, tx_id)

    if not tx:
        raise NotFoundException(details={'tx_id': tx_id })

    if tx.state == state.TX_CANCELLED:
        raise AlreadyCancelledException()

    allowed_cancellation_states = [state.TX_STARTED, state.TX_PREPARE_ERROR,
            state.TX_RESERVATION_ERROR, state.TX_PAYMENT_ERROR, state.TX_CLIENT_PAYMENT_HOLD]

    if tx.state not in allowed_cancellation_states:
        raise TransactionConflictException([to_state_str(s) for s in allowed_cancellation_states])

    tx.trigger_cancellation()
    tx.state = state.TX_CANCELLED
    tx.put()

    return tx
