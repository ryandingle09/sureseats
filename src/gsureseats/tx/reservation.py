from __future__ import absolute_import

import logging
import json
import requests

from datetime import datetime
from lxml import etree

from google.appengine.api import urlfetch
from google.appengine.ext.ndb import get_multi, non_transactional, Key

from .sureseats import create_sureseats_request
from .util import call_once, translate_message_via_error_code, DEFAULT_ERROR_MESSAGE

from gsureseats.orgs import AYALA_MALLS
from gsureseats.models import ReservationInfo, ReservationTransaction, TheaterOrganization
from gsureseats.settings import SURESEATS_BASE_URL

from .payment import GCashPayment

log = logging.getLogger(__name__)

TIMEOUT_DEADLINE = 45

SOURCE_ID = '10'
BUY_TYPE = 'BUY'
RESERVATION_TYPE = 'RESERVE'
RESERVATION_ACTION = 'TRANSACT2'
PAYMENTTYPE_EQUIVALENTTO_PAYMENTOPTIONSIDENTIFIER = {
        ReservationTransaction.allowed_payment_types.CLIENT_INITIATED: 'pesopay',
        ReservationTransaction.allowed_payment_types.MPASS: 'mpass',
        ReservationTransaction.allowed_payment_types.GCASH: 'gcash'
}


@non_transactional
def non_transactional_get_multi(keys):
    return get_multi(keys)


@non_transactional
def _get_ticket_type(org_uuid):
    theaterorg_key = Key(TheaterOrganization, org_uuid)
    theaterorg = theaterorg_key.get()
    ticket_type = theaterorg.ticket_type if theaterorg else 'barcode'
    return ticket_type


@non_transactional
def _get_convenience_fee(org_uuid, payment_identifier):
    convenience_fee = '0.00'
    theaterorg_key = Key(TheaterOrganization, org_uuid)
    theaterorg = theaterorg_key.get()

    if theaterorg.convenience_fees and payment_identifier in theaterorg.convenience_fees:
        convenience_fee = theaterorg.convenience_fees[payment_identifier]
    return convenience_fee


@non_transactional
def _check_enabled_payment_options(theater, payment_type):
    log.debug("_check_enabled_payment_options, start checking payment_type, %s" % payment_type)

    if payment_type not in PAYMENTTYPE_EQUIVALENTTO_PAYMENTOPTIONSIDENTIFIER:
        log.warn("SKIP!, __check_enabled_payment_options, payment_type %s, not found..." % payment_type)
        return False

    payment_identifier = PAYMENTTYPE_EQUIVALENTTO_PAYMENTOPTIONSIDENTIFIER[payment_type]

    if payment_identifier not in theater.payment_options:
        log.warn("SKIP!, _check_enabled_payment_options, payment_identifier %s, not found..." % payment_identifier)
        return False
    return True


def convert_date(d):
    return datetime.strptime(d, '%m/%d/%Y %I:%M:%S %p')


def prepare_reservation(tx):
    log.debug("prepare_reservation, preparing transaction...")
    log.info("reservation: ID: {}; WORKSPACE: {}...".format(tx.key.id(), tx.workspace))

    try:
        if 'is_raw' in tx.workspace and tx.workspace['is_raw']:
            reservations = tx.workspace['reservations_raw'][0]

            # for raw reservations, we need to substitute values for workspace keys we can't fulfill.
            tx.workspace['theaters.key'] = [None]
            tx.workspace['theaters.theater_cinema_name'] = ['XXXXXXXXXXX']
            tx.workspace['theaters.org_theater_code'] = reservations['theater_codes']
            tx.workspace['theaters.org_id'] = [str(AYALA_MALLS)]
            tx.workspace['schedules.movie'] = [None]
            tx.workspace['schedules.schedule_code'] = reservations['schedule_codes']
            tx.workspace['schedules.price'] = ['XX.XX']
            tx.workspace['seats'] = reservations['seats']
            tx.workspace['seats::imploded'] = ','.join(reservations['seats'])

        else:
            theater_keys, cinemas, schedule_keys, times, time_strs, seats = zip(*[(r.theater, r.cinema,
                    r.schedule, r.time, str(r.time), r.seat_id) for r in tx.reservations])


            # So we don't spend two RPC calls to fetch all of the theater
            # and schedule entities, we join the two key lists and then do
            # a single batch fetch, splitting it up as necessary
            seats = list(set(seats))
            entity_keys = theater_keys + schedule_keys
            entities = non_transactional_get_multi(entity_keys)
            theaters = entities[:len(theater_keys)]
            schedules = entities[len(theater_keys):]

            if tx.transaction_type.upper() not in [BUY_TYPE, RESERVATION_TYPE]:
                log.warn("ERROR!, prepare_reservation, transaction_type is not BUY or RESERVE...")

                return 'error', DEFAULT_ERROR_MESSAGE

            if tx.transaction_type.upper() == BUY_TYPE and not theaters[0].allow_buy_ticket:
                log.warn("ERROR!, prepare_reservation, not allow_buy_ticket...")

                return 'error', DEFAULT_ERROR_MESSAGE

            if tx.transaction_type.upper() == BUY_TYPE and not _check_enabled_payment_options(theaters[0], tx.payment_type):
                log.warn("ERROR!, prepare_reservation, disabled payment_type...")

                return 'error', DEFAULT_ERROR_MESSAGE

            if tx.transaction_type.upper() == RESERVATION_TYPE and not theaters[0].allow_reserve_ticket:
                log.warn("ERROR!, prepare_reservation, not allow_reserve_ticket...")

                return 'error', DEFAULT_ERROR_MESSAGE

            convenience_fee = '0.00' # default convenience fee
            slots = [s.get_timeslot(t) for (s, t) in zip(schedules, times)]
            movie_keys = [s.movie for s in slots]
            movies = non_transactional_get_multi(movie_keys)
            theater_cinemas = zip(theaters, cinemas)
            schedule_times = zip(schedules, times)
            theaterorg_id = [t.key.parent().id() for t in theaters][0]

            if tx.transaction_type.upper() == BUY_TYPE and tx.payment_type in [ReservationTransaction.allowed_payment_types.CLIENT_INITIATED]:
                convenience_fee = _get_convenience_fee(theaterorg_id, 'credit-card')
            elif tx.transaction_type.upper() == BUY_TYPE and tx.payment_type in [ReservationTransaction.allowed_payment_types.MPASS]:
                convenience_fee = _get_convenience_fee(theaterorg_id, 'mpass')
            elif tx.transaction_type.upper() == RESERVATION_TYPE and tx.payment_type in [ReservationTransaction.allowed_payment_types.RESERVE]:
                convenience_fee = _get_convenience_fee(theaterorg_id, 'reserve')
            elif tx.transaction_type.upper() == RESERVATION_TYPE and tx.payment_type in [ReservationTransaction.allowed_payment_types.GCASH]:
                convenience_fee = tx.gcash_convenience_fee

            tx.workspace['schedules.slot'] = slots
            tx.workspace['theaters.key'] = theater_keys
            tx.workspace['theaters.org_id'] = [t.key.parent().id() for t in theaters]
            tx.workspace['theaters.org_theater_code'] = [t.org_theater_code for t in theaters]
            tx.workspace['theaters.id'] = [str(t.key.id()) for t in theaters]
            tx.workspace['theaters.theater_name'] = theaters[0].name
            tx.workspace['theaters.cinemas.cinema_name'] = cinemas[0]
            tx.workspace['schedules.movie'] = movies
            tx.workspace['schedules.schedule_code'] = [sl.feed_code for sl in slots]
            tx.workspace['schedules.show_datetime'] = [datetime.combine(s.show_date, time) for s, time in schedule_times]
            tx.workspace['schedules.price'] = [sl.price for sl in slots]
            tx.workspace['schedules.variant'] = [sl.variant for sl in slots]
            tx.workspace['seating_type'] = [sl.seating_type for sl in slots][0]
            tx.workspace['seats'] = seats
            tx.workspace['seats::seat_count'] = str(tx.reservation_count())
            tx.workspace['seats::imploded'] = ','.join(seats)
            tx.workspace['convenience_fee'] = convenience_fee
            tx.workspace['snackbar_fee'] = tx.snackbar_fee
            tx.workspace['ticket_type'] = _get_ticket_type(theaterorg_id)
            tx.workspace['orders'] = tx.orders
            tx.workspace['orders_total'] = tx.orders_total

            if theaterorg_id != str(AYALA_MALLS):
                log.warn("ERROR!, prepare_reservation, reservation is not supported...")

                return 'error', DEFAULT_ERROR_MESSAGE

        tx.put()
        log.info("WORKSPACE: {}...".format(tx.workspace))
        return 'success', None
    except Exception as e:
        log.info("WORKSPACE: {}...".format(tx.workspace))
        log.warn("ERROR!, prepare_reservation...")
        log.error(e)
    return 'error', DEFAULT_ERROR_MESSAGE


def request_to_sureseats(tx, rsvp_payload):
    def __send_reservation_req(s, req):
        r = s.send(req, timeout=TIMEOUT_DEADLINE)
        return r.status_code, r.text

    s = requests.Session()
    req = create_sureseats_request(SURESEATS_BASE_URL, rsvp_payload)
    r_status_code, r_text = call_once(__send_reservation_req, tx, s, req)
    log.debug("requests {}".format(req.url))
    log.debug("do_reservation, got response: %s..." % str(r_text))
    return r_status_code, r_text


def do_reservation(tx):
    '''
    def __send_reservation_req(s, req):
        r = s.send(req, timeout=TIMEOUT_DEADLINE)
        return r.status_code, r.text
    '''

    r_status_code, r_text = '', ''

    if 'is_raw' in tx.workspace and tx.workspace['is_raw']:
        merchant_code = tx.workspace['theaters.org_theater_code'][0]
        schedule_code = tx.workspace['schedules.schedule_code'][0]
        seats = tx.workspace['seats::seat_count'] if tx.workspace['seating_type'] in ['Free Seating'] else tx.workspace['seats::imploded']
        orders = tx.workspace['orders']
        orders_total = tx.workspace['orders_total']
        snackbar_fee = tx.workspace['snackbar_fee']
    else:
        merchant_codes = tx.workspace['theaters.org_theater_code']
        slots = tx.workspace['schedules.slot']
        merchant_code = merchant_codes[0]
        schedule_code = slots[0].feed_code
        seats = tx.workspace['seats::seat_count'] if tx.workspace['seating_type'] in ['Free Seating'] else tx.workspace['seats::imploded']
        orders = tx.workspace['orders']
        orders_total = tx.workspace['orders_total']
        snackbar_fee = tx.workspace['snackbar_fee']

    theaterorg_id = tx.workspace['theaters.org_id'][0]
    theaterorg_key = Key(TheaterOrganization, theaterorg_id)
    rsvp_payload = {'src': SOURCE_ID,
                    'action': RESERVATION_ACTION,
                    'code': merchant_code,
                    'sid': schedule_code,
                    'seats': seats}

    if tx.transaction_type.upper() == BUY_TYPE and tx.payment_type in [ReservationTransaction.allowed_payment_types.MPASS]:
        log.debug("do_reservation, M-Pass, transaction_type, %s..." % tx.transaction_type.upper())

        rsvp_payload['type'] = BUY_TYPE
        rsvp_payload['ptype'] = 'M-Pass'
        rsvp_payload['email'] = tx.payment_info['email']
        rsvp_payload['id'] = tx.payment_info['customer_id']
        rsvp_payload['orders'] = orders
        rsvp_payload['orders_total'] = orders_total
        rsvp_payload['snackbar_fee'] = snackbar_fee
        tx.workspace['RSVP:fee'] = tx.workspace['convenience_fee'] if 'convenience_fee' in tx.workspace else '0.00'
        log.debug("do_reservation, sent request with payload: %s..." % rsvp_payload)
        r_status_code, r_text = request_to_sureseats(tx, rsvp_payload)

    elif tx.transaction_type.upper() == BUY_TYPE and tx.payment_type in [ReservationTransaction.allowed_payment_types.CLIENT_INITIATED]:
        log.debug("do_reservation, Credit-Card, transaction_type, %s..." % tx.transaction_type.upper())

        rsvp_payload['type'] = BUY_TYPE
        rsvp_payload['ptype'] = 'Credit Card'
        rsvp_payload['email'] = tx.payment_info['email']
        rsvp_payload['orders'] = orders
        rsvp_payload['orders_total'] = orders_total
        rsvp_payload['snackbar_fee'] = snackbar_fee

        if 'customer_id' in tx.payment_info and tx.payment_info['customer_id']:
            rsvp_payload['id'] = tx.payment_info['customer_id']

        total_convenience_fee = tx.workspace['convenience_fee'] if 'convenience_fee' in tx.workspace else '25.00'
        tx.workspace['RSVP:fee'] = total_convenience_fee
        log.debug("do_reservation, sent request with payload: %s..." % rsvp_payload)
        r_status_code, r_text = request_to_sureseats(tx, rsvp_payload)

    elif tx.transaction_type.upper() == BUY_TYPE and tx.payment_type in [ReservationTransaction.allowed_payment_types.GCASH]:
        log.debug("do_reservation, GCASH, transaction_type, %s..." % tx.transaction_type.upper())

        tx.workspace['RSVP:totalamount'] = tx.payment_info['total_transaction_amount']
        tx.put()

        engine = GCashPayment(tx.payment_info)
        r, msg = engine.do_payment_tx(tx)

        if r == 'success':

            rsvp_payload['type'] = BUY_TYPE
            rsvp_payload['ptype'] = 'G-Cash'
            rsvp_payload['mobnum'] = tx.payment_info['gcash_account']
            rsvp_payload['orders'] = orders
            rsvp_payload['orders_total'] = orders_total
            rsvp_payload['snackbar_fee'] = snackbar_fee
            if 'customer_id' in tx.payment_info and tx.payment_info['customer_id']:
                rsvp_payload['id'] = tx.payment_info['customer_id']

            total_convenience_fee = tx.workspace['convenience_fee'] if 'convenience_fee' in tx.workspace else '25.00'
            tx.workspace['RSVP:fee'] = total_convenience_fee
            log.debug("do_reservation, sent request with payload: %s..." % rsvp_payload)
            r_status_code, r_text = request_to_sureseats(tx, rsvp_payload)

        else:
            return 'error', msg

    elif tx.transaction_type.upper() == RESERVATION_TYPE and tx.payment_type in [ReservationTransaction.allowed_payment_types.RESERVE]:
        log.debug("do_reservation, Reserve, transaction_type, %s..." % tx.transaction_type.upper())

        rsvp_payload['type'] = RESERVATION_TYPE
        rsvp_payload['email'] = tx.payment_info['email']
        rsvp_payload['id'] = tx.payment_info['customer_id']
        tx.workspace['RSVP:fee'] = tx.workspace['convenience_fee'] if 'convenience_fee' in tx.workspace else '0.00'
        log.debug("do_reservation, sent request with payload: %s..." % rsvp_payload)
        r_status_code, r_text = request_to_sureseats(tx, rsvp_payload)

    else:
        log.warn("ERROR!, do_reservation, transaction_type or payment_type not supported...")
        return 'error', DEFAULT_ERROR_MESSAGE

    if r_status_code != 200:
        log.warn("ERROR!, do_reservation, cannot perform reservation, there was an error calling SureSeats...")
        if tx.transaction_type.upper() == BUY_TYPE and tx.payment_type in [ReservationTransaction.allowed_payment_types.GCASH]:
            engine = GCashPayment(tx.payment_info)
            engine.do_refund_tx(tx)
        return 'error', DEFAULT_ERROR_MESSAGE

    try:
        xml = etree.fromstring(str(r_text))
        status = xml.xpath('//Transaction/Status/text()')[0]

        if status.upper() == 'SUCCESS':
            log.debug("do_reservation, status equal to SUCCESS...")

            tx.reservation_reference = xml.xpath('//Transaction/RefNo/text()')[0]
            tx.workspace['RSVP:claimcode'] = xml.xpath('//Transaction/ClaimCode/text()')[0]
            tx.workspace['RSVP:claimdate'] = convert_date(xml.xpath('//Transaction/ClaimDate/text()')[0])
            tx.workspace['RSVP:totalamount'] = xml.xpath('//Transaction/TotalAmount/text()')[0]
            tx.workspace['RSVP:movie'] = xml.xpath('//Transaction/Movie/text()')[0]
            tx.workspace['RSVP:seats'] = [res.seat_id for res in tx.reservations]
            tx.bind_cancellation_callback(cancel_reservation)
            tx.put()
            return 'success', None

        elif status.upper() == 'PENDING':
            log.debug("do_reservation, status equal to PENDING...")

        # credit card transaction (ugh, ugly hack)
            tx.reservation_reference = xml.xpath('//Transaction/RefNo/text()')[0]
            tx.workspace['RSVP:totalamount'] = xml.xpath('//Transaction/TotalAmount/text()')[0]
            tx.workspace['RSVP:customerid'] = xml.xpath('//Transaction/CustID/text()')[0]
            tx.workspace['RSVP:movie'] = xml.xpath('//Transaction/Movie/text()')[0]
            tx.bind_cancellation_callback(cancel_reservation)
            tx.put()
            return 'success', None

        else:
            log.warn("ERROR!, do_reservation, status not equal to SUCCESS or PENDING...")
            log.warn("status: %s..." % status)

            if tx.transaction_type.upper() == BUY_TYPE and tx.payment_type in [ReservationTransaction.allowed_payment_types.GCASH]:
                engine = GCashPayment(tx.payment_info)
                engine.do_refund_tx(tx)

            namespace = 'ayala-malls'
            response_code = 'FAILED' # default response code to translate the error messages.
            response_message = status

            if status == '1 or more seats have been reserved by another user. Please try again and select other seats.':
                response_code = 'SEATSTAKEN'
            elif status == 'You do not have enough credits in your M-Pass card':
                response_code = 'INSUFFICIENTMPASSCREDITS'
            elif status == 'Sorry, you can only reserve 1 transaction. Kindly complete or cancel one of these before making a new set.':
                response_code = 'RESERVETRANSACTIONLIMIT1'
            elif status == 'Sorry, you can only reserve up to 2 transactions. Kindly complete or cancel one of these before making a new set.':
                response_code = 'RESERVETRANSACTIONLIMIT2'
            elif status == 'Error: Reservation quota for this screening time has been reached. The remaining seats are available for buying customers only.':
                response_code = 'RESERVATIONQUOTARESERVETPYE'
            elif status == 'Your account has been locked! You have made a reservation and did not claim it.':
                response_code = 'RESERVATIONLOCKEDACCOUNT'
            elif 'not available at' in status:
                response_code = 'RESERVATIONPURCHASENOTAVAILABLE'
            return 'error', translate_message_via_error_code(namespace, response_code, response_message)

    except etree.XMLSyntaxError as e:
        log.warn("ERROR!, do_reservation, XMLSyntaxError...")
        log.error(e)
    except Exception as e:
        log.warn("ERROR!, do_reservation...")
        log.error(e)
    return 'error', DEFAULT_ERROR_MESSAGE


def cancel_reservation(tx):
    pass
