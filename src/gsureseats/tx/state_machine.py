import json
import logging
import requests
import traceback

from google.appengine.ext.ndb import transactional
from google.appengine.api import memcache

import client_initiated

from .mutex import mutex_acquire, mutex_release
from .payment import do_payment, prepare_payment, is_client_initiated, is_gcash_initiated
from .query import query_info
from .reservation import prepare_reservation, do_reservation
from .scheduler import reschedule, schedule_for_reaping
from .ticket import do_ticket

from gsureseats.settings import SURESEATS_DIGITALVENTURES_BASE_URL, CMS_SURESEATS_DIGITALVENTURES_BASE_URL
from gsureseats.util import make_enum

import re


log = logging.getLogger(__name__)

TX_TASK_MAX_RETRY = 10
SEND_EMAIL_URL = SURESEATS_DIGITALVENTURES_BASE_URL + '/api/payment/email/'

state = make_enum('state', TX_START=0, TX_PREPARE=1, TX_STARTED=2, TX_RESERVATION_HOLD=3,
        TX_RESERVATION_OK=4, TX_PAYMENT_HOLD=5, TX_GENERATE_TICKET=6, TX_CLIENT_PAYMENT_HOLD=7,
        TX_FINALIZE=8, TX_FINALIZE_HOLD=9, TX_PREPARE_ERROR=13, TX_RESERVATION_ERROR=11,
        TX_PAYMENT_ERROR=12, TX_DONE=100, TX_CANCELLED=101, TX_STATE_ERROR=-1)


def to_state_str(s):
    if s not in state.reverse_mapping:
        return 'TX_UNKNOWN'
    return state.reverse_mapping[s]


def on_start(tx, params):
    log.info("Entering ON_START state...")
    log.info("TX %s\tstate TX_START\t\t Preparing transaction..." % tx.key.id())
    transition_state(tx, state.TX_START, state.TX_PREPARE)
    r, msg = prepare_transaction(tx)
    reschedule(tx, r=r, msg=msg)


def on_started(tx, params):
    log.info("Entering ON_STARTED state...")
    log.info("on_started, transaction workspace: {}...".format(tx.workspace))
    log.info("TX %s\tstate TX_STARTED\t\t Running reservation..." % tx.key.id())

    transition_state(tx, state.TX_STARTED, state.TX_RESERVATION_HOLD)
    r, msg = do_reservation(tx)
    log.info("Leaving ON_STARTED state...")
    log.info("DO RESERVATION STATUS: {0} MSG: {1}...".format(r, msg))
    reschedule(tx, r=r, msg=msg)


def on_reservation_ok(tx, params):
    log.info("Entering ON_RESERVATION_OK state...")
    log.info("TX %s\tstate TX_RESERVATION_OK\t\t Running payment..." % tx.key.id())

    if is_client_initiated(tx):
        log.info("TX %s\t\t\tTransaction flagged for client-initiated payment (Ayala Malls)..." % tx.key.id())
        transition_state(tx, state.TX_RESERVATION_OK, state.TX_CLIENT_PAYMENT_HOLD)
        tx.workspace['msg'] = tx.reservation_reference
        client_initiated.bind_listeners(tx)
        schedule_for_reaping(tx, client_initiated.BUY_TICKET_TIMEOUT)
        log.info("Leaving ON_RESERVATION_OK state...")
        return

    if is_gcash_initiated(tx):
        log.info("TX %s\t\t\tTransaction flagged for gcash-initiated payment (Ayala Malls)..." % tx.key.id())
        do_ticket(tx)
        transition_state(tx, state.TX_RESERVATION_OK, state.TX_DONE)
        tx.workspace['msg'] = tx.reservation_reference
        log.info("Leaving ON_RESERVATION_OK state...")
        log.info("Leaving ON_TICKET_GENERATE state...")
        reschedule(tx)
        return

    transition_state(tx, state.TX_RESERVATION_OK, state.TX_PAYMENT_HOLD)
    r, msg = do_payment(tx)
    log.info("Leaving ON_RESERVATION_OK state...")
    reschedule(tx, r=r, msg=msg)


def on_finalize(tx, params):
    log.info("Entering ON_FINALIZE state...")
    log.info("TX %s\tstate TX_FINALIZE\t\t Finalizing transaction..." % tx.key.id())
    transition_state(tx, state.TX_FINALIZE, state.TX_FINALIZE_HOLD)
    r, msg = do_payment(tx)
    log.info("Leaving ON_FINALIZE state...")
    reschedule(tx, r=r, msg=msg)


def on_finalize_hold(tx, params):
    log.info("Entering ON_FINALIZE_HOLD state...")
    log.info("TX %s\tstate TX_FINALIZE_HOLD\t\t Finalization hold..." % tx.key.id())
    transition_state(tx, state.TX_FINALIZE_HOLD, state.TX_GENERATE_TICKET)
    log.info("Leaving ON_FINALIZE_HOLD state...")
    reschedule(tx)


def on_ticket_generate(tx, params):
    log.info("Entering ON_TICKET_GENERATE state...")
    log.debug("TX %s\tstate TX_GENERATE_TICKET\t\t Generating ticket..." % tx.key.id())
    do_ticket(tx)
    transition_state(tx, state.TX_GENERATE_TICKET, state.TX_DONE)
    log.info("Leaving ON_TICKET_GENERATE state...")
    reschedule(tx)


def on_done(tx, params):
    log.info("Entering ON_DONE state...")
    log.debug("TX %s\tstate TX_DONE\t\t Deleting workspace..." % tx.key.id())
    log.debug("TX %s\tstate TX_DONE\t\t Ensuring payment info is elided..." % tx.key.id())

    tx.workspace = {}
    tx.elide_payment_details()

    # sending snackbar information to cms
    try:
        orders = tx.orders
        if not (orders == ''):
            log.debug('on_done, Start sending Snackbar information to CMS')
            transaction_id = str(tx.key.id())
            orders = str(orders)
            theater_info = str(tx.reservations)
            transaction_date = str(tx.date_created)

            first_name = tx.customer.first_name
            last_name = tx.customer.last_name
            mobile_number = tx.customer.mobile
            email = tx.customer.email
            ticket_code = tx.ticket.code
            ticket_extra = json.dumps(tx.ticket.extra)
            ticket_extra = json.loads(ticket_extra)
            payment_method = str(tx.payment_type)
            screening_time = str(ticket_extra.get('show_datetime'))
            movie_title = str(ticket_extra.get('movie_title'))
            cinema_name = str(ticket_extra.get('cinema_name'))
            variant = str(ticket_extra.get('variant'))
            snackbar_fee = str(tx.snackbar_fee)

            seats = str(ticket_extra.get('seats'))

            match = re.search("('Theater',\s)([0-9]+)", theater_info)
            theater_id = str("Mcs7a6cnEeKbE8i8yI6krA~" + match.group(2))

            snackbar_url = CMS_SURESEATS_DIGITALVENTURES_BASE_URL + '/api/snacks/transactions'
            # snackbar_url = 'http://api.sureseats.qa.digitalventures.ph/api/snacks/transactions'

            data = {"transactions":transaction_id,
                   "transaction_date":transaction_date,
                   "orders":orders,
                   "theater_id":theater_id,
                   "first_name":first_name,
                   "last_name":last_name,
                   "mobile_number":mobile_number,
                   "email":email,
                   "confirmation_code":ticket_code,
                   "screening_time":screening_time,
                   "movie_title":movie_title,
                   "payment_method": payment_method,
                   "cinema_name":cinema_name,
                   "variant":variant,
                   "snackbar_fee":snackbar_fee,
                   "seats":seats}

            log.info('snackbar_parameters: {}'.format(data))
            r = requests.post(snackbar_url, json=data)
            log.info(r.url)
            if r.status_code == 200:
                log.info('snackbar_parameters: Snackbar orders forwarded to CMS handler')
                log.info('snackbar_return_value: {}'.format(r.text))
            else:
                log.debug('snackbar_parameters: Forwarding Snackbar orders to CMS handler failed')
    except Exception as e:
        log.debug(e)
        log.debug(traceback.format_exc())

    # request to send email.
    try:
        log.debug("on_done, requesting to send email via %s..." % SEND_EMAIL_URL)

        TX_SEND_EMAIL_URL = SEND_EMAIL_URL + str(tx.key.id())
        SURESEATS_USER_UUID = str(tx.customer.uuid)

        headers = {'X-SureSeats-User': SURESEATS_USER_UUID, 'Content-type': 'application/json'}
        payload = {}
        payload['id'] = str(tx.key.id())
        payload['transaction'] = tx.to_entity()
        payload_str = json.dumps(payload)

        log.debug('payload_str: {}'.format(payload_str))

        s = requests.Session()
        req = requests.Request('POST', url=TX_SEND_EMAIL_URL, headers=headers, data=payload_str).prepare()
        res = s.send(req, timeout=60)

        log.info("######################### START ############################")
        log.info(res.status_code)
        log.info(res.text)
        log.info("########################## END #############################")

        log.debug("on_done, email sent via %s..." % TX_SEND_EMAIL_URL)

    except Exception as e:
        log.warn("ERROR!, failed on requesting to send email...")
        log.error(e)
        log.debug(traceback.format_exc())
    log.info("Leaving ON_DONE state...")

HOLD_NEXT_STATES = {
    state.TX_PREPARE: (state.TX_STARTED, state.TX_PREPARE_ERROR),
    state.TX_RESERVATION_HOLD: (state.TX_RESERVATION_OK, state.TX_RESERVATION_ERROR),
    state.TX_PAYMENT_HOLD: (state.TX_GENERATE_TICKET, state.TX_PAYMENT_ERROR),
    state.TX_FINALIZE: (state.TX_FINALIZE_HOLD, state.TX_PAYMENT_ERROR),
    state.TX_FINALIZE_HOLD: (state.TX_GENERATE_TICKET, state.TX_PAYMENT_ERROR),
}


def bind_on_hold(which):
    new_success_state, new_error_state = HOLD_NEXT_STATES[which]

    def on_hold(tx, params):
        ret = params['r']

        if ret == 'success':
            log.info("TX %s\tstate %s\t\t Success..." % (tx.key.id(), which))

            transition_state(tx, which, new_success_state)
            reschedule(tx)
        else:
            msg = params['msg']
            log.info("TX %s\tstate %s\t\t Error %s..." % (tx.key.id(), which, msg))
            tx.workspace['msg'] = msg
            transition_state(tx, which, new_error_state)
            schedule_for_reaping(tx)

    return on_hold


state_handlers = {
    state.TX_START: on_start,
    state.TX_PREPARE: bind_on_hold(state.TX_PREPARE),
    state.TX_STARTED: on_started,
    state.TX_PAYMENT_HOLD: bind_on_hold(state.TX_PAYMENT_HOLD),
    state.TX_RESERVATION_OK: on_reservation_ok,
    state.TX_RESERVATION_HOLD: bind_on_hold(state.TX_RESERVATION_HOLD),
    state.TX_FINALIZE: on_finalize,
    state.TX_FINALIZE_HOLD: bind_on_hold(state.TX_FINALIZE_HOLD),
    state.TX_GENERATE_TICKET: on_ticket_generate,
    state.TX_DONE: on_done,
}


def current_state(tx):
    return tx.state


def transition_state(tx, current_state, new_state):
    tx_id = tx.key.id()

    if tx.state == current_state or not current_state:
        log.info("TX_ID: {} -- State movement from {} to {}...".format(tx_id, current_state, new_state))

        tx.state = new_state
        tx.ver += 1
        tx.put()
    else:
        log.warn("Transitioning to an older state; did a transaction fail? (current: %s, expected: %s)..." % (to_state_str(tx.state), to_state_str(current_state)))


def next_state(params):
    device_id = params['device_id']
    tx_id = params['tx_id']

    mutex_acquire(tx_id)

    try:
        state = __in_transaction_next_state(device_id, tx_id, params)
    finally:
        mutex_release(tx_id)

    log.info("next_state on Device %s -> TX %s => %s...", device_id, tx_id, to_state_str(state))

    return state

@transactional(retries=0)
def __in_transaction_next_state(device_id, tx_id, params):
    retry = int(params.get('retry', '0'))
    tx_ver = int(params['tx_ver'])

    if retry >= TX_TASK_MAX_RETRY:
        log.warn("TX %s on device %s reached max retry; bailing...", tx_id, device_id)

        return None

    tx = query_info(device_id, tx_id)
    current_tx_state = current_state(tx)

    if current_tx_state == state.TX_CANCELLED:
        log.info("TX %s\tstate %s\t\t Transaction was cancelled..." % (tx_id, to_state_str(current_tx_state)))

        return current_tx_state

    if tx_ver < tx.ver:
        log.warn("TX %s\tstate %s\t\t Retried for spurious transaction failure; exiting...", tx_id, to_state_str(current_tx_state))

        return current_tx_state

    if current_tx_state not in state_handlers:
        if tx_ver > tx.ver:
            log.warn("TX %s\tstate %s\t Txaction was scheduled! Shouldn't be at this state..." % (tx_id, to_state_str(current_tx_state)))
            log.warn("Might be due to datastore contention; intentionally rescheduling transaction (incrementing retry)...")

            reschedule(tx, retry=str(retry+1))
        else:
            log.fatal("TX %s: Should not have been scheduled in this state: %s...", tx_id, to_state_str(current_tx_state))

            assert current_tx_state in state_handlers, 'Unexpected state'
    else:
        try:
            state_handlers[current_tx_state](tx, params)
        except Exception as e:
            log.warn("Exception occurred in state_handler, rescheduling transaction tasklet...")
            log.exception(e)

            # Reset state to current_tx_state (we need to do this,
            # also because we cache state in memcache, which doesn't
            # participate in the transaction)
            transition_state(tx, None, current_tx_state)

            raise e

    return current_tx_state

def prepare_transaction(tx):
    r, msg = prepare_reservation(tx)

    if r == 'success':
        log.info("Reservation Preparation Success --> Preparing Payment...")

        r, msg = prepare_payment(tx)

    return r, msg
