import logging
import requests
import json
import traceback

from Crypto.Hash import SHA

from google.appengine.ext.ndb import Key, non_transactional
from .util import call_once, translate_message_via_error_code, DEFAULT_ERROR_MESSAGE
from gsureseats.models import ClientInitiatedPaymentSettings, ReservationTransaction, Theater, TheaterOrganization
from gsureseats.orgs import AYALA_MALLS
from gsureseats.settings import CMS_SURESEATS_DIGITALVENTURES_BASE_URL, GCASH_PROXY_BASE_URL

log = logging.getLogger(__name__)

BUY_TYPE = 'BUY'
RESERVATION_TYPE = 'RESERVE'
WS_PAYMENT_ENGINE = 'payment::engine'
WS_PAYMENT_CLIENT_INITIATED = 'payment::is-client-initiated'
WS_PAYMENT_GCASH_INITIATED = 'payment::is-gcash-initiated'

# GCASH ENDPOINTS
GCASH_PAYMENT_ENDPOINT = CMS_SURESEATS_DIGITALVENTURES_BASE_URL + '/api/users/transaction/payGcash'
#GCASH_REFUND_ENDPOINT = 'http://api.sureseats.qa.digitalventures.ph/api/users/transaction/refundGcash'
GCASH_REFUND_ENDPOINT = GCASH_PROXY_BASE_URL + '/api/n/refund'
GMOVIESGCASHTOKEN = ':eT"{>U3qmX83g)n3^MxQ{+c]<7XY]9"TL[N`GC!4&8pB+jW~n#=q=T5\daWqxSa}w^~~5J\cu(~S"2jrSrAWM]/'

# OTP
SEND_OTP = CMS_SURESEATS_DIGITALVENTURES_BASE_URL + '/api/users/transaction/send/otp'
VERIFY_OTP = CMS_SURESEATS_DIGITALVENTURES_BASE_URL + '/api/users/transaction/verify/otp'
RESEND_OTP = CMS_SURESEATS_DIGITALVENTURES_BASE_URL + '/api/users/transaction/resend/otp'

# GMOVIES MPASS ACCOUNT DETAILS
PROMOCODE_USERNAME = 'gmovies_promo'
PROMOCODE_PASSWORD = 'gmovies123'

TIMEOUT_DEADLINE = 45


def prepare_payment(tx):
    try:
        # for raw reservations, use AYALA_MALLS instead.
        if 'is_raw' in tx.workspace and tx.workspace['is_raw']:
            theater_key = Key(TheaterOrganization, str(AYALA_MALLS))
        else:
            theater_key = tx.workspace['theaters.key'][0]

        if tx.transaction_type.upper() == BUY_TYPE and tx.payment_type == ReservationTransaction.allowed_payment_types.CLIENT_INITIATED:
            log.debug("prepare_payment, initializing ClientInitiatedPayment handler, transaction_type: %s..." % tx.transaction_type)

            payment_gateway = tx.payment_info.pop('payment-gateway', 'pesopay')
            cinema_name = tx.workspace['theaters.cinemas.cinema_name']
            settings = _get_client_initiated_payment_settings(theater_key, payment_gateway, cinema_name)
            engine = ClientInitiatedPayment(settings, tx.payment_info)
            tx.workspace[WS_PAYMENT_CLIENT_INITIATED] = True

        elif tx.transaction_type.upper() == BUY_TYPE and tx.payment_type == ReservationTransaction.allowed_payment_types.MPASS:
            log.debug("prepare_payment, initializing MPassPayment handler, transaction_type: %s..." % tx.transaction_type)
            engine = MPassPayment(tx.payment_info)

        elif tx.transaction_type.upper() == BUY_TYPE and tx.payment_type == ReservationTransaction.allowed_payment_types.GCASH:
            log.debug("prepare_payment, initializing GCashPayment handler, transaction_type: %s..." % tx.transaction_type)
            engine = GCashPayment(tx.payment_info)
            tx.workspace[WS_PAYMENT_GCASH_INITIATED] = True

        elif tx.transaction_type.upper() == RESERVATION_TYPE and tx.payment_type == ReservationTransaction.allowed_payment_types.RESERVE:
            log.debug("prepare_payment, initializing ReservePayment handler, transaction_type: %s..." % tx.transaction_type)
            engine = ReservePayment(tx.payment_info)

        else:
            log.warn("ERROR!, prepare_payment, transaction_type or payment_type is not supported...")
            return 'error', DEFAULT_ERROR_MESSAGE
        tx.workspace[WS_PAYMENT_ENGINE] = engine
        status, message = engine.do_prepare(tx)
        tx.put()
        return status, message

    except KeyError as e:
        log.warn("ERROR!, prepare_payment, missing key...")
        log.error(e)
    except Exception as e:
        log.warn("ERROR!, prepare_payment...")
        log.error(e)
    return 'error', DEFAULT_ERROR_MESSAGE


def do_payment(tx):
    engine = tx.workspace[WS_PAYMENT_ENGINE]
    retval = engine.do_payment_tx(tx)
    tx.bind_cancellation_callback(cancel_payment)
    tx.put()
    return retval


def cancel_payment(tx):
    pass


def is_client_initiated(tx):
    return WS_PAYMENT_CLIENT_INITIATED in tx.workspace and tx.workspace[WS_PAYMENT_CLIENT_INITIATED]


def is_gcash_initiated(tx):
    return WS_PAYMENT_GCASH_INITIATED in tx.workspace and tx.workspace[WS_PAYMENT_GCASH_INITIATED]

@non_transactional
def _get_client_initiated_payment_settings(theater_key, payment_gateway, cinema_name):
    return ClientInitiatedPaymentSettings.get_settings(theater_key, payment_gateway, cinema_name)


class MPassPayment():
    def __init__(self, payment_info):
        self.err_message = DEFAULT_ERROR_MESSAGE

    # empty, nothing to prepare using this payment type, because customer_id already got in transaction payload.
    def do_prepare(self, tx):
        return 'success', None

    # empty, because MPass payments are one-step through Sureseats.
    def do_payment_tx(self, tx):
        return 'success', None


class ClientInitiatedPayment():
    def __init__(self, settings, payment_info):
        self.err_message = DEFAULT_ERROR_MESSAGE
        self.settings = settings
        self.reference_parameter_key = None
        self.form_method = 'POST'
        self.form_target = settings.settings.get('default-form-target')
        self.form_parameters = dict(settings.default_params)
        merchant_id = settings.settings.get('pesopay::merchant-id')

        if merchant_id:
            self.form_parameters['merchantId'] = merchant_id

        if 'reference-parameter' in payment_info:
            self.reference_parameter_key = payment_info['reference-parameter']

        if 'form-method' in payment_info:
            self.form_method = payment_info['form-method']

        if 'form-target' in payment_info:
            self.form_target = payment_info['form-target']

        if 'form-parameters' in payment_info:
            self.form_parameters = dict(self.form_parameters.items() + payment_info['form-parameters'].items())

    def do_prepare(self, tx):
        return 'success', None

    # default error, since we should be in a different state in the first place.
    # Internal error: Client-initiated payment, but entered TX_PAYMENT_HOLD.
    def do_payment_tx(self, tx):
        return 'error', self.err_message

    def generate_hash(self, reference):
        merchant_id = self.settings.settings['pesopay::merchant-id']
        hash_secret = self.settings.settings['pesopay::merchant-hash-secret']
        currency_code = self.form_parameters['currCode']
        amount = self.form_parameters['amount']
        payment_type = self.form_parameters['payType']

        seed = [merchant_id, reference, currency_code, amount, payment_type, hash_secret]
        seed_str = '|'.join(seed)
        hash = SHA.new(seed_str).digest()
        return hash.encode('hex')


class ReservePayment():
    def __init__(self, payment_info):
        self.err_message = DEFAULT_ERROR_MESSAGE

    # empty, nothing to prepare using this payment type.
    def do_prepare(self, tx):
        return 'success', None

    # empty, because RESERVE transaction types are one-step through SureSeats and payment is via cash on cinema's ticket booth.
    def do_payment_tx(self, tx):
        return 'success', None


class GCashPayment():
    def __init__(self, payment_info):
        self.err_message = DEFAULT_ERROR_MESSAGE
        self.mobile = payment_info['gcash_account']
        self.customer_id = payment_info['customer_id']
        self.otp = payment_info['otp']
        self.uuid = payment_info['uuid']
        self.token = payment_info['token']

    def _do_gcash_otp_verify_prepare(self):
        log.debug("GCashPayment, _do_gcash_otp_verify_prepare, preparing OTP verify from %s..." % VERIFY_OTP)
        form_data = {}
        form_data['mobile'] = self.mobile
        form_data['pin'] = self.otp

        headers = {
            "X-SureSeats-User": self.uuid,
            "X-SureSeats-Token": self.token,
        }

        req = requests.Request('POST', url=VERIFY_OTP, headers=headers, data=form_data).prepare()
        return req

    def _do_gcash_payment_prepare(self, params):
        log.debug("GCashPayment, _do_gcash_payment_prepare, preparing GCash Sell Command from %s..." % GCASH_PAYMENT_ENDPOINT)
        log.debug("GCashPayment, _do_gcash_payment_prepare, payload: {}...".format(params))

        headers = {
            "X-SureSeats-User": self.uuid,
            "X-SureSeats-Token": self.token
            #"GMOVIESTOKEN": GMOVIESGCASHTOKEN
        }

        form_data = {}
        form_data['transaction_id'] = params['transaction_id']
        form_data['mobile'] = params['mobile']
        form_data['amount'] = params['amount']
        form_data['theater_id'] = params['theater_id']
        req = requests.Request('POST', url=GCASH_PAYMENT_ENDPOINT, headers=headers, data=form_data).prepare()
        return req

    def _do_gcash_refund_prepare(self, params):

        headers = {
            #"X-SureSeats-User": self.uuid,
            #"X-SureSeats-Token": self.token
            "GMOVIESTOKEN": GMOVIESGCASHTOKEN
        }

        #form_data = {}
        #form_data['txn_id'] = params['txn_id']
        #form_data['transaction_id'] = params['transaction_id']

        #txn_id = params['transaction_id']

        url = GCASH_REFUND_ENDPOINT + '/' + params

        log.debug(
            "GCashPayment, _do_gcash_refund_prepare, preparing GCash Transfer Command from %s, with transaction_id: %s..." % (
            GCASH_REFUND_ENDPOINT, params))

        #req = requests.Request('POST', url=GCASH_REFUND_ENDPOINT, headers=headers).prepare()
        req = requests.Request('GET', url=url, headers=headers).prepare()
        return req

    def _do_gcash_otp_verify(self, s, req):
        r = s.send(req, timeout=TIMEOUT_DEADLINE)
        return r.status_code, r.text

    def _do_gcash_payment(self, s, req):
        r = s.send(req, timeout=TIMEOUT_DEADLINE)
        return r.status_code, r.text

    def _do_gcash_refund(self, s, req):
        r = s.send(req, timeout=TIMEOUT_DEADLINE)
        return r.status_code, r.text

    def do_prepare(self, tx):
        self.device_type = tx.platform if tx.platform else ''
        theaterorg_id = tx.workspace['theaters.org_id'][0]

        s = requests.Session()
        req = self._do_gcash_otp_verify_prepare()
        r_status_code, r_json = call_once(self._do_gcash_otp_verify, tx, s, req)

        log.debug("GCashPayment, OTP, verify response: {}...".format(r_json))

        if r_status_code != 200:
            return 'error', self.err_message

        d_json = json.loads(r_json)

        if 'status' not in d_json:
            return 'error', self.err_message

        if d_json['status'] == 0:
            msg = d_json['message']
            return 'error', msg
        else:
            return 'success', None

    def do_payment_tx(self, tx):
        theaterorg_id = 'Mcs7a6cnEeKbE8i8yI6krA'

        try:
            gcash_payment_payload = {}
            gcash_payment_payload['transaction_id'] = tx.key.id()
            gcash_payment_payload['mobile'] = self.mobile
            gcash_payment_payload['amount'] = tx.workspace['RSVP:totalamount']

            theater_id = str(tx.workspace['theaters.id'][0])

            if tx.workspace['theaters.org_theater_code'][0] == 'ATC':
                cinema_name = tx.workspace['theaters.cinemas.cinema_name']
                gcash_payment_payload['theater_id'] = theaterorg_id + '~' + theater_id + '-' + cinema_name
            else:
                gcash_payment_payload['theater_id'] = theaterorg_id + '~' + theater_id

            s = requests.Session()
            req = self._do_gcash_payment_prepare(gcash_payment_payload)
            r_status_code, r_json = call_once(self._do_gcash_payment, tx, s, req)

            if r_status_code == 200:
                d_json = json.loads(r_json)
                if 'status' not in d_json:
                    log.debug('GCashPayment, Invalid Response from GCash. Return error {}'.format(r_status_code))
                    return 'error', self.err_message

                if d_json['status'] == 0:
                    if d_json['message']:
                        error_message = d_json['message']
                        log.debug('GCashPayment, An error occured in GCash {}'.format(error_message))
                        return 'error', error_message,
                    else:
                        log.debug('GCashPayment, Invalid Response from GCash. message parameter not in response.')
                        return 'error', self.err_message

                log.debug('GCashPayment, Success Payment from GCash')
                return 'success', None

            else:
                log.debug('GCashPayment, Invalid Response from GCash. Return error {}'.format(r_status_code))
                return 'error', self.err_message

        except Exception as e:
            log.debug('GCashPayment, {}'.format(e))
            log.debug('GCashPayment, {}'.format(traceback.format_exc()))

    def do_refund_tx(self, tx):
        log.warn("START!, Refund process GCashPayment...")

        try:
            #gcash_refund_payload = {}
            #gcash_refund_payload['transaction_id'] = tx.key.id()
            gcash_refund_payload = tx.key.id()
            s = requests.Session()
            req = self._do_gcash_refund_prepare(gcash_refund_payload)
            r_status_code, r_json = call_once(self._do_gcash_refund, tx, s, req)

            log.debug("GCashPayment, refund status_code: %s" % r_status_code)
            log.debug("GCashPayment, refund results: {}...".format(r_json))

        except Exception as e:
            log.warn("ERROR!, GCashPayment. do_refund_tx...")
            log.error(e)
            log.debug('GCashPayment, {}'.format(traceback.format_exc()))
