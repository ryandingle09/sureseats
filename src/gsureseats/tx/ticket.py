import logging

from google.appengine.ext.ndb import non_transactional

from .barcode import generate_ticket_barcode
from .qrcode import generate_ticket_qrcode
from .util import DEFAULT_ERROR_MESSAGE

from gsureseats.models import TicketData


log = logging.getLogger(__name__)

TICKET_DATETIME_FORMAT = '%m/%d/%Y %I:%M %p'


@non_transactional
def currency_format(amount, amount_format='{:.2f}'):
    return amount_format.format(float(amount))

def do_ticket(tx):
    log.debug("do_ticket, start...")

    if 'RSVP:claimcode' not in tx.workspace:
        log.warn("ERROR!, do_ticket, missing key RSVP:claimcode...")

        return 'error', DEFAULT_ERROR_MESSAGE

    ref = tx.reservation_reference
    code = tx.workspace['RSVP:claimcode']
    date = tx.workspace['RSVP:claimdate']
    amount = tx.workspace['RSVP:totalamount']
    ticket_type = tx.workspace['ticket_type']
    seat_price = tx.workspace['schedules.price'] or ['0.00']
    original_seat_price = str(tx.total_seat_price(tx.workspace['RSVP:fee'], True))
    original_total_amount = str(tx.total_amount(tx.workspace['RSVP:fee'], True))

    ticket = TicketData(ref=ref, code=code, date=date, ticket_type=ticket_type, extra={})
    ticket.extra['movie_id'] = tx.workspace['schedules.movie'][0].key.id()
    ticket.extra['movie_title'] = tx.workspace['schedules.movie'][0].canonical_title
    ticket.extra['advisory_rating'] = tx.workspace['schedules.movie'][0].advisory_rating if tx.workspace['schedules.movie'][0].advisory_rating else ''
    ticket.extra['runtime'] = str(tx.workspace['schedules.movie'][0].runtime_mins) if tx.workspace['schedules.movie'][0].runtime_mins else ''
    ticket.extra['theater_name'] = tx.workspace['theaters.theater_name'] if 'theaters.theater_name' in tx.workspace else ''
    ticket.extra['cinema_name'] = tx.workspace['theaters.cinemas.cinema_name'] if 'theaters.cinemas.cinema_name' in tx.workspace else ''
    ticket.extra['show_datetime'] = tx.workspace['schedules.show_datetime'][0].strftime(TICKET_DATETIME_FORMAT)
    ticket.extra['variant'] = tx.workspace['schedules.variant'][0]
    ticket.extra['seating_type'] = tx.workspace['seating_type']
    ticket.extra['seat_count'] = tx.workspace['seats::seat_count']
    ticket.extra['seats'] = tx.workspace['seats::imploded']
    ticket.extra['seat_price'] = currency_format(str(seat_price[0]))
    ticket.extra['total_seat_price'] = currency_format(tx.workspace['RSVP:totalseatprice']) if 'RSVP:totalseatprice' in tx.workspace else currency_format(original_seat_price)
    ticket.extra['total_amount'] = currency_format(amount)
    ticket.extra['reservation_fee'] = tx.workspace['RSVP:fee']
    ticket.extra['ticket_count'] = tx.ticket_count()

    # additional details for discounts.
    ticket.extra['discount_type'] = tx.workspace['RSVP:discounttype'] if 'RSVP:discounttype' in tx.workspace else ''
    ticket.extra['original_seat_price'] = tx.workspace['RSVP:originalseatprice'] if 'RSVP:originalseatprice' in tx.workspace else original_seat_price
    ticket.extra['original_total_amount'] = tx.workspace['RSVP:originaltotalamount'] if 'RSVP:originaltotalamount' in tx.workspace else original_total_amount
    ticket.extra['total_discount'] = tx.workspace['RSVP:totaldiscount'] if 'RSVP:totaldiscount' in tx.workspace else '0.00'
    ticket.extra['total_seat_discount'] = tx.workspace['RSVP:totalseatdiscount'] if 'RSVP:totalseatdiscount' in tx.workspace else '0.00'
    ticket.extra['total_discounted_seats'] = str(tx.workspace['RSVP:totaldiscountedseats']) if 'RSVP:totaldiscountedseats' in tx.workspace else '0'
    ticket.extra['is_discounted'] = tx.workspace['discount::is_discounted'] if 'discount::is_discounted' in tx.workspace else False

    # append transaction code.
    ticket.transaction_codes.append(code)

    tx.ticket = ticket

    generate_ticket_barcode(tx, font_size=20, text_distance=2, dpi=230)
    generate_ticket_qrcode(tx)

    tx.put()

    log.debug("do_ticket, process done...")
