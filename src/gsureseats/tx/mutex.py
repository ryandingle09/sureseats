# Mutex, so that state machine handlers for same TX don't exec concurrently.

from google.appengine.api import memcache


def mutex_acquire(tx_id):
    while not memcache.add("LOCK::%s" % tx_id, 1):
        pass

    return True

def mutex_release(tx_id):
    memcache.delete("LOCK::%s" % tx_id)
