import logging
import pickle

from google.appengine.api import memcache


log = logging.getLogger(__name__)
DEFAULT_ERROR_MESSAGE = 'An error occurred during your transaction. Kindly contact customerservice@sureseats.com to address your issue.'


def fn_name(fn):
    return fn.__name__

def call_once(fn, tx, *args, **kwargs):
    idemp_key = "tx(%s|%s)::call=%s" % (tx.key.id(), tx.ver, fn_name(fn))
    retval = memcache.get(idemp_key)
    log.info("CALL ONCE: IDEMP-KEY:%s RETVAL:%s ARGS: %s KWARGS: %s CALLING-FUNCTION:%s", idemp_key, retval, args, kwargs, fn)

    if not retval:
        retval = fn(*args, **kwargs)
        memcache.add(idemp_key, retval)
    return retval


def translate_message_via_error_code(namespace, error_code, error_message):
    log.debug("translate_message_via_error_code, start...")
    log.info("namespace: %s..." % namespace)
    log.info("error_code: %s..." % error_code)
    log.info("error_message: %s..." % error_message)

    messages = {}

    # Ayala Malls
    messages['ayala-malls'] = {}
    messages['ayala-malls']['SEATSTAKEN'] = 'Sorry, the seat is not available and is already taken. Please select other seats.'
    messages['ayala-malls']['INSUFFICIENTMPASSCREDITS'] = 'Sorry, you do not have enought credits in your M-Pass card. Please select other payment option.'
    messages['ayala-malls']['RESERVETRANSACTIONLIMIT1'] = error_message
    messages['ayala-malls']['RESERVETRANSACTIONLIMIT2'] = error_message
    messages['ayala-malls']['RESERVATIONQUOTARESERVETPYE'] = error_message
    messages['ayala-malls']['RESERVATIONLOCKEDACCOUNT'] = error_message
    messages['ayala-malls']['RESERVATIONPURCHASENOTAVAILABLE'] = error_message

    # MPass
    messages['mpass'] = {}
    messages['mpass']['INCORRECTUSERNAMEPASSWORD'] = 'Sorry, your username and password is incorrect. Please check it and try again.'

    if namespace in messages:
        if str(error_code) in messages[namespace]:
            return messages[namespace][str(error_code)]

    return DEFAULT_ERROR_MESSAGE
