from __future__ import absolute_import

import ImageFont
import logging

from StringIO import StringIO

from barcode import get_barcode_class
from barcode.writer import ImageWriter, FONT, mm2px


log = logging.getLogger(__name__)

TICKET_BARCODE_TYPE = 'code39'
TICKET_BARCODE_GENERATOR = get_barcode_class(TICKET_BARCODE_TYPE)


class CustomImageWriter(ImageWriter):
    def _paint_text(self, xpos, ypos):
        xpos = ((xpos + self.font_size) / 2) - self.quiet_zone # center the ticket code
        pos = (mm2px(xpos, self.dpi), mm2px(ypos, self.dpi))
        font = ImageFont.truetype(FONT, self.font_size)
        self._draw.text(pos, self.text, font=font, fill=self.foreground)


def generate_ticket_barcode(tx, font_size=10, text_distance=5, dpi=300):
    ticket_barcode = StringIO()
    ticket_code = tx.ticket.code

    barcode_gen = TICKET_BARCODE_GENERATOR(code=ticket_code, writer=CustomImageWriter(), add_checksum=False)
    barcode_gen.write(ticket_barcode, options={'font_size': font_size, 'text_distance': text_distance, 'dpi': dpi})

    tx.ticket.barcode = ticket_barcode.getvalue()
    tx.ticket.barcodes.append(tx.ticket.barcode)
