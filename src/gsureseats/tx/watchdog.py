from datetime import datetime, timedelta

from google.appengine.ext.ndb import transactional

from .actions import cancel
from .scheduler import reschedule
from .state_machine import state

from gsureseats.models import ReservationTransaction


ACTIVE_WINDOW = timedelta(minutes=10)


@transactional
def nudge_stuck_tx(tx):
    reschedule(tx)

def watchdog():
    stuck_tx_timestamp = datetime.now() - ACTIVE_WINDOW
    stuck_tx_q = ReservationTransaction.query(ReservationTransaction.is_active==True, ReservationTransaction.date_created<=stuck_tx_timestamp)

    for tx in stuck_tx_q:
        if tx.state not in TX_WAIT_STATES:
            nudge_stuck_tx(tx)
        else:
            cancel(tx.key.parent().id(), tx.key.id()) # cancel this transaction.
