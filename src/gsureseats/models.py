import base64
import json
import logging

from datetime import date, datetime, time, timedelta
from decimal import Decimal
from itertools import groupby
from operator import attrgetter
from uuid import uuid4 as uuid_gen, UUID
from warnings import warn

from google.appengine.api import memcache
from google.appengine.ext import ndb

from gsureseats.exceptions.api import BadValueException
from gsureseats.indexes import geolocation
from gsureseats.util import make_enum
from gsureseats.util.id_encoder import decoded_theater_id, encoded_theater_id, encode_uuid
from gsureseats.util.model_utils import camelcase


log = logging.getLogger(__name__)

DATE_FORMAT = '%d %b %Y'
TIME_FORMAT = '%H:%M'
DATETIME_FORMAT = DATE_FORMAT + ' ' + TIME_FORMAT
BIRTH_DATE_FORMAT = '%Y-%m-%d'
SLOT_KEY_FORMAT = '%Y%m%d'


def _handle_callbacks(cb_key):
    def _bind_handler(tx, cb):
        if cb_key not in tx.workspace:
            tx.workspace[cb_key] = []

        tx.workspace[cb_key].append(cb)

    def _trigger_cb_handler(tx, *args, **kwargs):
        if cb_key in tx.workspace:
            for cb in tx.workspace[cb_key]:
                cb(tx, *args, **kwargs)

    return _bind_handler, _trigger_cb_handler


# Custom Decimal Property
# Decimal property ideal to store currency values, such as $20.34
# See https://developers.google.com/appengine/docs/python/ndb/subclassprop
class DecimalProperty(ndb.IntegerProperty):
    def _validate(self, value):
        if not isinstance(value, (Decimal, str, unicode, int, long)):
            raise TypeError('Expected a Decimal, str, unicode, int or long an got instead %s' % repr(value))

    def _to_base_type(self, value):
        return int(Decimal(value) * 100)

    def _from_base_type(self, value):
        return Decimal(value)/Decimal(100)


###############
#
# Movies related Models
#
###############

class Feed(ndb.Model):
    name = ndb.StringProperty(required=True)
    image_density = ndb.StringProperty(required=True)
    movie_fields_blacklist = ndb.StringProperty(repeated=True)
    movie_fields_whitelist = ndb.StringProperty(repeated=True)
    allow_movie_creation = ndb.BooleanProperty(default=True)
    heuristics_priority = ndb.IntegerProperty(default=99)


class FeedCorrelation(ndb.Model):
    org_key = ndb.KeyProperty(kind=Feed, required=True)
    movie_id = ndb.StringProperty()
    variant = ndb.StringProperty()

    def to_entity(self):
        raise ValueError("Cannot convert FeedCorrelation directly to API entity")


class Movie(ndb.Model):
    canonical_title = ndb.StringProperty(required=True)
    correlation_title = ndb.StringProperty(repeated=True)
    per_org_titlemap = ndb.PickleProperty()
    movie_correlation = ndb.StructuredProperty(FeedCorrelation, repeated=True)
    ratings = ndb.JsonProperty(indexed=True)
    advisory_rating = ndb.StringProperty()
    genre = ndb.StringProperty()
    cast = ndb.StringProperty(repeated=True)
    synopsis = ndb.TextProperty()
    runtime_mins = ndb.FloatProperty()
    slottime_mins = ndb.FloatProperty() # check if used, if not remove it.
    release_date = ndb.DateProperty()
    variants = ndb.StringProperty(repeated=True)
    poster_portrait = ndb.StringProperty(indexed=False)
    poster_landscape = ndb.StringProperty(indexed=False)
    is_featured = ndb.BooleanProperty(default=False)
    is_special = ndb.BooleanProperty(default=False)
    is_expired = ndb.BooleanProperty(default=False)
    is_locked = ndb.BooleanProperty(default=False)
    is_published = ndb.BooleanProperty(default=True)
    is_active = ndb.BooleanProperty(default=True)

    def is_not_future_release_date(self):
        # assume entries without a release date are now showing, for backwards compatibility purposes (data).
        if not self.release_date:
            return True
        return date.today() >= self.release_date

    is_showing = ndb.ComputedProperty(is_not_future_release_date)

    def has_schedules(self):
        schedules_queryset = Schedule.query(Schedule.slots.movie==self.key, Schedule.show_date>=date.today()).order(-Schedule.show_date)
        if schedules_queryset.count() > 0:
            return True
        return False

    def bind_org_title(self, org_id, variant, title):
        if org_id not in self.per_org_titlemap:
            self.per_org_titlemap[org_id] = {}
        self.per_org_titlemap[org_id][variant] = title

    def get_link_name(self):
        link_name = ''
        try:
            title_words = self.correlation_title[0].split(':')[-1].split('_')
            link_name = '-'.join(title_words)
        except Exception as e:
            log.warn("ERROR!, get_link_name...")
            log.error(e)
        return link_name

    def to_entity(self):
        e = self.to_dict()
        e.pop('correlation_title', None)
        e.pop('is_locked', None)
        e.pop('is_published', None)
        e.pop('is_active', None)
        e.pop('movie_correlation', None)
        e.pop('per_org_titlemap', None)
        e.pop('poster_landscape', None)
        e.pop('ratings', None)
        e.pop('slottime_mins', None)

        e['has_schedules'] = self.has_schedules()
        e['advisory_rating'] = e['advisory_rating'] if e['advisory_rating'] else 'MTRCB rating not yet available'
        e['genre'] = e['genre'] if e['genre'] else ''
        e['synopsis'] = e['synopsis'] if e['synopsis'] else ''
        e['poster_portrait'] = e['poster_portrait'] if e['poster_portrait'] else ''
        e['release_date'] = e['release_date'].strftime(DATE_FORMAT) if e['release_date'] else ''
        e['runtime_mins'] = e['runtime_mins'] if e['runtime_mins'] else 0.0
        e['variants'] = e['variants'] if e['variants'] else []
        e['link_name'] = self.get_link_name()

        return e

###############
#
# Theaters related Models
#
###############


class TheaterOrganization(ndb.Model):
    name = ndb.StringProperty(required=True)
    pubkey = ndb.TextProperty()
    default_client_control = ndb.JsonProperty()
    client_control = ndb.JsonProperty()
    convenience_fees = ndb.JsonProperty()
    template = ndb.StringProperty(indexed=False)
    template_image = ndb.BlobProperty()
    ticket_remarks = ndb.JsonProperty(indexed=False)
    ticket_type = ndb.StringProperty(required=True, default='barcode')
    sort_priority = ndb.IntegerProperty(default=99)
    is_published = ndb.BooleanProperty(default=False)
    is_active = ndb.BooleanProperty(default=False)

    def to_entity(self):
        return {'name': self.name,
                'default_client_control': self.default_client_control,
                'client_control': self.client_control}


class Cinema(ndb.Model):
    name = ndb.StringProperty(required=True)
    seat_count = ndb.StringProperty()
    seat_map = ndb.PickleProperty()
    location = ndb.GeoPtProperty()

    def to_entity(self):
        e = self.to_dict()
        e['seat_count'] = e['seat_count'] if e['seat_count'] else '0'
        e['location'] = {'lon': self.location.lon, 'lat': self.location.lat} if e['location'] else {}

        return e

    def to_minimum_entity(self):
        e = self.to_dict()
        e['seat_count'] = e['seat_count'] if e['seat_count'] else '0'
        e['location'] = {'lon': self.location.lon, 'lat': self.location.lat} if e['location'] else {}

        e.pop('seat_map', None)

        return e


class Theater(ndb.Model):
    name = ndb.StringProperty(required=True)
    location = ndb.GeoPtProperty()
    cinemas = ndb.StructuredProperty(Cinema, repeated=True)
    org_theater_code = ndb.StringProperty()
    address = ndb.StringProperty(indexed=False)
    address_category = ndb.StringProperty()
    logo = ndb.StringProperty(indexed=False)
    payment_options = ndb.StringProperty(repeated=True)
    allow_buy_ticket = ndb.BooleanProperty(default=False)
    allow_reserve_ticket = ndb.BooleanProperty(default=False)
    is_published = ndb.BooleanProperty(default=False)
    is_active = ndb.BooleanProperty(default=False)

    blacklist = ['cinemas.name']

    def get_cinema_entity(self, cinema_name):
        cinemas = self.cinemas

        for c in cinemas:
            if cinema_name == c.name:
                return c

    def get_link_name(self):
        link_name = '-'.join(self.name.lower().split(' '))

        return link_name.strip()

    def to_entity(self):
        e = self.to_dict()
        e.pop('address_category', None)
        e.pop('allow_buy_ticket', None)
        e.pop('allow_reserve_ticket', None)
        e.pop('is_published', None)
        e.pop('is_active', None)
        e.pop('logo', None)
        e.pop('payment_options', None)
        e.pop('org_theater_code', None)

        e['address'] = e['address'] if e['address'] else ''
        e['location'] = {'lon': self.location.lon, 'lat': self.location.lat} if e['location'] else {}
        e['link_name'] = self.get_link_name()
        e['cinemas'] = [c.to_entity() for c in self.cinemas]
        e['is_metro_manila'] = True if self.address_category == 'Metro Manila' else False

        return e

    def to_entity_seatmap(self, cinema_name):
        cinema = self.get_cinema_entity(cinema_name)
        e = self.to_dict()
        e.pop('allow_buy_ticket', None)
        e.pop('allow_reserve_ticket', None)
        e.pop('is_published', None)
        e.pop('is_active', None)
        e.pop('payment_options', None)
        e.pop('org_theater_code', None)

        e['address'] = e['address'] if e['address'] else ''
        e['location'] = {'lon': self.location.lon, 'lat': self.location.lat} if e['location'] else {}
        e['cinema'] = cinema.to_entity()

        return e


###############
#
# Schedules related Models
#
###############

class ScheduleTimeSlot(ndb.Model):
    start_time = ndb.TimeProperty(required=True)
    feed_code = ndb.StringProperty()
    movie = ndb.KeyProperty(kind=Movie, required=True)
    variant = ndb.StringProperty()
    seating_type = ndb.StringProperty()
    price = DecimalProperty()


class Schedule(ndb.Expando):
    cinema = ndb.StringProperty()
    show_date = ndb.DateProperty()
    slots = ndb.StructuredProperty(ScheduleTimeSlot, repeated=True)
    reverse_lookup = ndb.PickleProperty()

    whitelist = ['theater', 'location.lat', 'location.lon', 'movie']
    filter_mappings = {'movie' : 'slots.movie', 'seating_type': 'slots.seating_type'}

    @classmethod
    def create_schedule(cls, **kwargs):
        if 'id' not in kwargs:
            kwargs['id'] = cls.make_slot_id(kwargs['cinema'], kwargs['show_date'])

        sched = cls(**kwargs)

        return sched

    @classmethod
    def make_slot_id(cls, cinema_id, show_date):
        return "%s::%s" % (cinema_id, show_date.strftime(SLOT_KEY_FORMAT))

    @classmethod
    def convert_schedule(cls, sched):
        def get_time(t):
            if isinstance(t, time):
                return t
            elif isinstance(t, datetime):
                return t.time()
            else:
                raise ValueError(t)

        # defaults for really old schema schedules.
        price = None
        seating_type = None
        variant = None

        slot_id = cls.make_slot_id(sched.cinema, sched.show_date)
        slot_key = ndb.Key(cls, slot_id, parent=sched.key.parent())
        slot = memcache.get("schedule:new_schema::%s" % slot_key.urlsafe())
        
        if not slot:
            slot = cls(id=slot_id, parent=sched.key.parent())
            slot.cinema = sched.cinema
            slot.show_date = sched.show_date

        map_time_to_code = json.loads(sched.map_time_to_code)

        if 'price' in sched._properties:
            price = Decimal(sched.price) / Decimal(100)

        if 'seating_type' in sched._properties:
            seating_type = sched.seating_type

        if 'variant' in  sched._properties:
            variant = sched.variant

        [slot.add_timeslot(start_time=get_time(t), feed_code=map_time_to_code.get(str(get_time(t)), None), movie=sched.movie,
                variant=variant, seating_type=seating_type, price=price) for t in sched.start_times if get_time(t) not in slot._start_times()]

        memcache.set("schedule:new_schema::%s" % slot_key.urlsafe(), slot)

        return slot

    @classmethod
    def find_schedule(cls, theater_key, cinema_name, show_datetime):
        if isinstance(show_datetime, str) or isinstance(show_datetime, unicode):
            show_datetime = datetime.strptime(show_datetime, DATETIME_FORMAT)

        if isinstance(show_datetime, datetime):
            show_date = show_datetime.date()
            show_time = show_datetime.time()
        else:
            show_date = show_datetime
            show_time = None

        slot_id = cls.make_slot_id(cinema_name, show_date)
        slot_key = ndb.Key(cls, slot_id, parent=theater_key)
        slot = slot_key.get()

        if show_time and slot and show_time not in slot._start_times():
            # .warn("show_time not in start_times for schedule slot: %s" % show_time)
            return None

        return slot

    def sorted_slots(self):
        return sorted(self.slots, key=attrgetter('start_time'))

    def add_timeslot(self, **kwargs):
        if not self.slots:
            self.slots = []

        timeslot = ScheduleTimeSlot(**kwargs)
        self.slots.append(timeslot)

        return timeslot

    def get_timeslot(self, time):
        for s in self.slots:
            if time == s.start_time:
                return s

        return None

    def get_feed_code(self, feed_code):
        for s in self.slots:
            if feed_code == s.feed_code:
                return s
        return None

    def _start_times(self):
        warn("_start_times is deprecated; caller must use slots property directly", DeprecationWarning, stacklevel=2)

        return [s.start_time for s in self.slots]

    def _feed_codes(self):
        warn("_feed_codes is deprecated; caller must use slots property directly", DeprecationWarning, stacklevel=2)

        return [s.feed_code for s in self.slots]

    def _to_entity_parent(self):
        return encoded_theater_id(self.key.parent())

    def __to_entity_id(self, movie_tup):
        encoded_movie_id = encode_uuid(UUID(movie_tup[0].id()))

        return "%s,%s,%s,%s,%s" % (self.key.id(), encoded_movie_id, movie_tup[1], movie_tup[2], movie_tup[3])

    def to_entity_with_slot_group(self, (movie, variant, price, seating_type), slot_group):
        e = self.to_dict()
        e.pop('reverse_lookup', None)
        e.pop('slots', None)

        slots = sorted([s for s in slot_group], key=attrgetter('start_time'))

        e['start_times'] = [s.start_time.strftime(TIME_FORMAT) for s in slots]
        e['movie'] = movie.id() if movie else ''
        e['price'] = str(price)
        e['seating_type'] = seating_type
        e['show_date'] = e['show_date'].strftime(DATE_FORMAT) if e['show_date'] else ''
        e['theater'] = encoded_theater_id(self.key.parent())
        e['variant'] = variant

        return e

    def to_entity(self):
        raise Exception('to_entity() currently not supported for ScheduleSlots: caller must use to_entities()')

    # create the necessary API entities represented by this single ScheduleSlots instance.
    def to_entities(self):
        ordered_slots = sorted(self.slots, key=attrgetter('movie', 'variant', 'start_time'))
        grouped_slots = groupby(ordered_slots, attrgetter('movie', 'variant', 'price', 'seating_type'))
        ret = [(self.__to_entity_id(movie_tup), self.to_entity_with_slot_group(movie_tup, slot_group)) for movie_tup, slot_group in grouped_slots]

        return ret


###############
#
# Transactions related Models
#
###############

class Device(ndb.Model):
    device_class = ndb.StringProperty()


class ReservationInfo(ndb.Model):
    schedule = ndb.KeyProperty(Schedule)
    theater = ndb.KeyProperty(Theater)
    movie = ndb.KeyProperty(Movie)
    time = ndb.TimeProperty()
    cinema = ndb.StringProperty()
    seat_id = ndb.StringProperty()


    def to_entity(self):
        e = self.to_dict()
        e.pop('seat_id', None)

        e['schedule'] = e['schedule'].id()
        e['time'] = e['time'].strftime(TIME_FORMAT)
        e['theater'] = encoded_theater_id(e['theater'])
        e['movie'] = e['movie'].id()
        e['seat'] = self.seat_id


        return e


class CustomerData(ndb.Model):
    lbid = ndb.StringProperty()
    uuid = ndb.StringProperty()
    customer_id = ndb.StringProperty()
    first_name = ndb.StringProperty()
    last_name = ndb.StringProperty()
    email = ndb.StringProperty()
    mobile = ndb.StringProperty()
    customer_type = ndb.StringProperty()

    def to_entity(self):
        e = self.to_dict()
        return e


class DiscountData(ndb.Model):
    user_id = ndb.StringProperty()
    mobile = ndb.StringProperty()
    claim_code = ndb.StringProperty()
    claim_code_type = ndb.StringProperty()
    payment_gateway = ndb.StringProperty()
    original_total_amount = ndb.StringProperty()
    total_discount = ndb.StringProperty()
    total_amount = ndb.StringProperty()
    discount_value = ndb.StringProperty()
    discount_type = ndb.StringProperty()

    def to_entity(self):
        e = self.to_dict()

        e['original_total_amount'] = e['original_total_amount'] if e['original_total_amount'] else '0.00'
        e['total_discount'] = e['total_discount'] if e['total_discount'] else '0.00'
        e['total_amount'] = e['total_amount'] if e['total_amount'] else '0.00'
        e['discount_value'] = e['discount_value'] if e['discount_value'] else '0'
        e['discount_type'] = e['discount_type'] if e['discount_type'] else ''

        return e


class TicketData(ndb.Model):
    ref = ndb.StringProperty()
    code = ndb.StringProperty()
    date = ndb.DateProperty()
    extra = ndb.JsonProperty()
    ticket_type = ndb.StringProperty()
    barcode = ndb.BlobProperty()
    qrcode = ndb.BlobProperty()
    barcodes = ndb.BlobProperty(repeated=True)
    qrcodes = ndb.BlobProperty(repeated=True)
    transaction_codes = ndb.StringProperty(repeated=True)
    image = ndb.BlobProperty()

    def to_entity(self):
        e = self.to_dict()
        e.pop('image', None)
        e.pop('barcode', None)
        e.pop('qrcode', None)
        e.pop('barcodes', None)
        e.pop('qrcodes', None)

        if e['ticket_type'] == 'qrcode':
            image_codes = [qr.encode('base64') for qr in self.qrcodes if self.qrcodes]
        else:
            image_codes = [bar.encode('base64') for bar in self.barcodes if self.barcodes]

        e['date'] = e['date'].strftime(DATE_FORMAT) if e['date'] else ''
        e['qrcodes'] = image_codes

        return e


class ReservationTransaction(ndb.Model):
    allowed_payment_types = make_enum('payment_types',
                                      RESERVE='reserve',
                                      MPASS='mpass',
                                      CLIENT_INITIATED='client-initiated',
                                      GCASH='gcash'
                                      )

    payment_reference = ndb.StringProperty()
    payment_type = ndb.StringProperty(required=True)
    payment_info = ndb.JsonProperty(required=True)
    reservation_reference = ndb.StringProperty()
    reservations = ndb.StructuredProperty(ReservationInfo, repeated=True)
    customer = ndb.StructuredProperty(CustomerData)
    discount = ndb.StructuredProperty(DiscountData)
    ticket = ndb.StructuredProperty(TicketData)
    transaction_type = ndb.StringProperty(required=True)
    platform = ndb.StringProperty()
    state = ndb.IntegerProperty(required=True)
    ver = ndb.IntegerProperty(default=1)
    workspace = ndb.PickleProperty()
    date_created = ndb.DateTimeProperty(auto_now_add=True)
    date_cancelled = ndb.DateTimeProperty()
    was_reaped = ndb.BooleanProperty()
    is_discounted = ndb.BooleanProperty(default=False)
    is_cancelled = ndb.BooleanProperty(default=False)
    orders = ndb.StringProperty(required=False)
    orders_total = ndb.StringProperty(required=False)
    snackbar_fee = ndb.StringProperty(required=False)

    @classmethod
    def generate_tx_id(cls):
        return str(uuid_gen())

    @classmethod
    def create_transaction(cls, device_id, tx_info):
        tx_id = cls.generate_tx_id()
        tx = cls(key=ndb.Key(Device, device_id, cls, tx_id))
        tx.copy_api_entity(tx_info)
        tx.workspace = {}

        return tx

    def ticket_count(self):
        return len(self.reservations)

    def reservation_count(self):
        return len(self.reservations)

    def total_amount(self, reservation_fee, use_workspace=False):
        if not use_workspace:
            schedules = ndb.get_multi([r.schedule for r in self.reservations])
            schedule_times = zip(schedules, [r.time for r in self.reservations])

            return sum([(s.get_timeslot(tt).price + Decimal(reservation_fee)) for s, tt in schedule_times])
        else:
            return sum([p+Decimal(reservation_fee) for p in self.workspace['schedules.price']])

    def total_seat_price(self, reservation_fee, use_workspace=False):
        if not use_workspace:
            schedule = self.reservations[0].schedule.get()
            schedule_time = self.reservations[0].time

            return schedule.get_timeslot(schedule_time).price + Decimal(reservation_fee)
        else:
            return self.workspace['schedules.price'][0] + Decimal(reservation_fee)

    def get_queue(self):
        queue_id = self.workspace.get('queue_id', None)

        if self.transaction_type == 'RESERVE':
            return "txqueuereserve%d" % queue_id if queue_id else 'transactions'

        return "txqueue%d" % queue_id if queue_id else 'transactions'

    def check_transaction_information(self, tx_info):
        try:
            transaction_type = tx_info['transaction_type']
            payment = tx_info['payment']
            payment_type = payment['type']

            if payment_type == ReservationTransaction.allowed_payment_types.MPASS:
                payment['customer_id']
                payment['email']
            elif payment_type == ReservationTransaction.allowed_payment_types.CLIENT_INITIATED:
                payment['customer_id']
                payment['email']
                payment['reference-parameter']
                payment['form-method']
                payment['form-target']
                payment['form-parameters']
            elif payment_type == ReservationTransaction.allowed_payment_types.RESERVE:
                payment['customer_id']
                payment['email']
            elif payment_type == ReservationTransaction.allowed_payment_types.GCASH:
                payment['customer_id']
                payment['mobile']
                payment['otp']
                payment['gcash_account']
                payment['uuid']
                payment['token']
                payment['total_transaction_amount']

            else:
                raise BadValueException('type', "Unknown payment type: %s" % payment_type)
        except KeyError as e:
            raise BadValueException(e[0], "Missing %s params for payment" % e[0])

        try:
            reservations = tx_info['reservations']

            for reservation in reservations:
                reservation['theater']
                reservation['cinema']
                reservation['movie']
                reservation['show_datetime']
                reservation['seat']

        except KeyError as e:
            raise BadValueException(e, "Missing %s params for reservation" % e)

    def copy_api_entity(self, tx_info):
        self.check_transaction_information(tx_info)

        if 'reservations' in tx_info:
            self.reservations = [self.__build_reservation(r) for r in tx_info['reservations']]

        if 'user' in tx_info:
            self.customer = self.__build_customer_data(tx_info['user'])

        if 'discount' in tx_info:
            self.discount = self.__build_discount_data(tx_info['discount'])

        payment_info = dict(tx_info['payment'])

        if 'type' not in payment_info:
            raise BadValueException('type', 'No payment type specified')

        self.payment_type = payment_info.pop('type', None)
        self.payment_info = payment_info
        self.platform = tx_info.pop('platform', None)
        self.transaction_type = tx_info.pop('transaction_type', None)
        self.transaction_type = self.transaction_type.upper() if self.transaction_type else None

        self.orders = tx_info.pop('orders', None)
        self.orders_total = tx_info.pop('orders_total', None)
        self.snackbar_fee = tx_info.pop('snackbar_fee', None)

    # ensure always executed outside of a transaction.
    @ndb.non_transactional
    def __build_reservation(self, r):
        org_uuid, t_id = decoded_theater_id(r['theater'])

        if org_uuid is None or t_id is None:
            raise BadValueException('reservations', "Theater not found: %s" % r)

        t = ndb.Key(TheaterOrganization, str(org_uuid), Theater, t_id)
        m = ndb.Key(Movie, r['movie'])
        c = r['cinema']
        s = Schedule.find_schedule(t, r['cinema'], r['show_datetime'])
        show_datetime = datetime.strptime(r['show_datetime'], DATETIME_FORMAT)
        time = show_datetime.time()

        if not s:
            raise BadValueException('reservations', "Schedule not found: %s" % r)

        if show_datetime < datetime.now():
            raise BadValueException('reservations', "Attempting to reserve for an expired schedule: %s" % r)

        reserv = ReservationInfo()
        reserv.schedule = s.key
        reserv.time = time
        reserv.theater = t
        reserv.cinema = c
        reserv.movie = m
        reserv.seat_id = r['seat']
        return reserv

    @ndb.non_transactional
    def __build_customer_data(self, customer_details):
        customer_data = CustomerData()
        customer_data.lbid = customer_details['lbid']
        customer_data.uuid = customer_details['uuid']
        customer_data.customer_id = customer_details['customer_id']
        customer_data.first_name = customer_details['first_name']
        customer_data.last_name = customer_details['last_name']
        customer_data.email = customer_details['email']
        customer_data.mobile = customer_details['mobile']
        customer_data.customer_type = customer_details['customer_type']

        return customer_data

    @ndb.non_transactional
    def __build_discount_data(self, discount_details):
        discount_data = DiscountData()
        discount_data.user_id = discount_details['user_id']
        discount_data.mobile = discount_details['mobile']
        discount_data.claim_code = discount_details['claim_code']
        discount_data.claim_code_type = discount_details['claim_code_type']
        discount_data.payment_gateway = discount_details['payment_gateway']

        # default value, because discounts not yet computed.
        discount_data.original_total_amount = '0.00'
        discount_data.total_discount = '0.00'
        discount_data.total_amount = '0.00'
        discount_data.discount_value = '0'
        discount_data.discount_type = ''

        return discount_data

    def to_entity(self):
        date_created = self.date_created.strftime(DATETIME_FORMAT) if self.date_created else ''
        reservation_entities = [r.to_entity() for r in self.reservations]
        customer_entity = self.customer.to_entity() if self.customer else {}
        discount_entity = self.discount.to_entity() if self.discount else {}
        ticket_entity = self.ticket.to_entity() if self.ticket else {}
        payment_type = 'pesopay' if self.payment_type == 'client-initiated' else self.payment_type
        transaction_type = self.transaction_type.upper() if self.transaction_type else ''

        orders = self.orders if self.orders else ''
        orders_total = self.orders_total if self.orders_total else ''
        snackbar_fee = self.snackbar_fee if self.snackbar_fee else ''

        return {'date_created': date_created, 'reservations': reservation_entities, 'payment_info': self.payment_info,
                'user_info': customer_entity, 'discount_info': discount_entity, 'ticket': ticket_entity,
                'payment_type': payment_type, 'transaction_type': transaction_type,
                'orders': orders, 'orders_total': orders_total, 'snackbar_fee': snackbar_fee
                }

    def elide_payment_details(self):
        pass

    bind_cancellation_callback, trigger_cancellation = _handle_callbacks('__cancellation_callbacks')
    bind_update_callback, trigger_update = _handle_callbacks('__update_callbacks')


###############
#
# Callback, Client API Authentication, and Payment Settings related Models
#
###############

class Event(ndb.Model):
    event_channel = ndb.StringProperty(required=True)
    org_id = ndb.StringProperty(required=True)
    timestamp = ndb.IntegerProperty()
    payload = ndb.JsonProperty(indexed=False)
    read = ndb.BooleanProperty(default=False)

    @classmethod
    def get_events(cls, channel_name):
        return cls.query(cls.event_channel==channel_name, cls.read==False)


class Listener(ndb.Model):
    event_channel = ndb.StringProperty(required=True)
    listener_id = ndb.StringProperty(required=True)
    callback = ndb.PickleProperty()
    version = ndb.StringProperty()

    @classmethod
    def get_listeners(cls, channel_name):
        return cls.query(cls.event_channel==channel_name)

    @classmethod
    def get_listeners_by_version(cls, channel_name, version):
        return cls.query(cls.event_channel==channel_name, cls.version==version)


class Client(ndb.Model):
    name = ndb.StringProperty(required=True)
    pubkey = ndb.TextProperty()


class ClientInitiatedPaymentSettingsCinema(ndb.Model):
    payment_identifier = ndb.StringProperty()
    default_params = ndb.PickleProperty() # default parameters for client-initiated payment form.
    settings = ndb.PickleProperty()
    cinema = ndb.StringProperty()


class ClientInitiatedPaymentSettings(ndb.Model):
    payment_identifier = ndb.StringProperty()
    default_params = ndb.PickleProperty() # default parameters for client-initiated payment form.
    settings = ndb.PickleProperty()
    cinema_settings = ndb.StructuredProperty(ClientInitiatedPaymentSettingsCinema, repeated=True)

    @classmethod
    def get_settings(cls, key, gateway, cinema=None):
        settings = ndb.Key(ClientInitiatedPaymentSettings, gateway, parent=key).get()

        if settings:
            cinema_names = []

            if settings.cinema_settings:
                cinema_names = [s.cinema for s in settings.cinema_settings]

            if cinema and cinema in cinema_names:
                cinema_settings = settings.get_cinema_settings(cinema)

                if cinema_settings:
                    return cinema_settings

            return settings

        parent_key = key.parent()

        if parent_key:
            return cls.get_settings(parent_key, gateway)
        else:
            settings = cls()
            settings.default_params = {}
            settings.settings = {}

        return settings

    def get_cinema_settings(self, cinema):
        for settings in self.cinema_settings:
            if cinema == settings.cinema:
                return settings

        return None

    def check_cinema_settings(self, cinema_new, cinema_old=None):
        is_not_existing = False
        cinema_names = [settings.cinema for settings in self.cinema_settings]

        if cinema_old and cinema_old in cinema_names:
            cinema_names.remove(cinema_old)

        if cinema_new not in cinema_names:
            is_not_existing = True

        return is_not_existing


###############
#
# SureSeats's Customer's Accounts and Actions History.
#
###############

class SureSeatsCustomerAccount(ndb.Model):
    username = ndb.StringProperty(required=True)
    password = ndb.StringProperty(required=True)
    first_name = ndb.StringProperty(required=True)
    middle_name = ndb.StringProperty()
    last_name = ndb.StringProperty(required=True)
    email = ndb.StringProperty(required=True)
    mobile = ndb.StringProperty(required=True)
    birth_date = ndb.DateProperty()
    gender = ndb.StringProperty(required=True)
    province = ndb.StringProperty(required=True)
    city = ndb.StringProperty(required=True)
    street = ndb.StringProperty()
    default_theater1 = ndb.KeyProperty(kind=Theater)
    default_theater2 = ndb.KeyProperty(kind=Theater)
    default_theater3 = ndb.KeyProperty(kind=Theater)
    synced_at = ndb.DateTimeProperty(auto_now_add=True)

    def to_entity(self):
        e = self.to_dict()
        e.pop('synced_at', None)

        e['customer_id'] = self.key.id()
        e['birth_date'] = e['birth_date'].strftime(BIRTH_DATE_FORMAT) if e['birth_date'] else ''
        e['default_theater1'] = encoded_theater_id(e['default_theater1']) if e['default_theater1'] else ''
        e['default_theater2'] = encoded_theater_id(e['default_theater2']) if e['default_theater2'] else ''
        e['default_theater3'] = encoded_theater_id(e['default_theater3']) if e['default_theater3'] else ''

        return e


class SureSeatsActionHistory(ndb.Model):
    username = ndb.StringProperty(required=True)
    action = ndb.StringProperty(required=True)
    details = ndb.StringProperty()
    description = ndb.StringProperty()
    status = ndb.StringProperty(required=True)
    message = ndb.StringProperty()
    platform = ndb.StringProperty(required=True)
    created_at = ndb.DateTimeProperty(auto_now_add=True)
