from google.appengine.api import memcache
from google.appengine.ext import ndb
from google.appengine.api.app_identity import get_default_version_hostname


class SystemSettings(ndb.Model):
    enforce_auth = ndb.BooleanProperty(default=False)
    enforce_auth_website = ndb.BooleanProperty(default=False)
    notification_addresses = ndb.StringProperty(repeated=True)
    notification_retry_threshold = ndb.IntegerProperty(default=3)
    reap_timeout = ndb.IntegerProperty(default=60)
    client_initiated_reap_timeout = ndb.IntegerProperty(default=600) # 10 minutes
    rt_api_key = ndb.StringProperty(default='xcyuvytdbtzn6hqcg5dyksqd')

    @classmethod
    def get_settings(cls):
        settings = cls.get_by_id('settings')

        if not settings:
            settings = cls(id='settings')
            settings.notification_addresses = ['nnarzeus@yondu.com']
            settings.put()

        return settings

SETTINGS = SystemSettings.get_settings()
ENFORCE_AUTH = SETTINGS.enforce_auth
ENFORCE_AUTH_WEBSITE = SETTINGS.enforce_auth_website
NOTIFICATION_ADDRESSES = SETTINGS.notification_addresses
NOTIFICATION_RETRY_THRESHOLD = SETTINGS.notification_retry_threshold
REAP_TIMEOUT = SETTINGS.reap_timeout
CLIENT_INITIATED_REAP_TIMEOUT = SETTINGS.client_initiated_reap_timeout
ROTTEN_TOMATOES_API_KEY = SETTINGS.rt_api_key

GCASH_PROXY_BASE_URL = 'http://52.77.204.204' # will change this once the staging for gcash is up.

CLAIMCODE_GLOBAL_SERVER_STAGING = 'http://core.qa.digitalventures.ph/api'
CLAIMCODE_GLOBAL_SERVER_PRODUCTION = 'http://52.77.80.147/api'


#PRODUCTION
SURESEATS_BASE_URL = 'http://api.sureseats.com/globe.asp'
SURESEATS_DIGITALVENTURES_BASE_URL = 'http://sureseats-api-elb-99128436.ap-southeast-1.elb.amazonaws.com'
CMS_SURESEATS_DIGITALVENTURES_BASE_URL = 'http://api2.sureseats.com'
STORAGE_SCHEDULE = '/ayala-schedules-prod/'
STORAGE_NOWSHOWING = '/ayala-now-showing-prod/'
STORAGE_COMINGSOON = '/ayala-coming-soon-prod/'

#STAGING
#SURESEATS_BASE_URL = 'http://api.sureseats.com/globe_test.asp'
#SURESEATS_DIGITALVENTURES_BASE_URL = 'http://api.sureseats.qa.digitalventures.ph'
#CMS_SURESEATS_DIGITALVENTURES_BASE_URL = 'http://api.sureseats.qa.digitalventures.ph'
#STORAGE_SCHEDULE = '/ayala-schedules/'
#STORAGE_NOWSHOWING = '/ayala-now-showing/'
#STORAGE_COMINGSOON = '/ayala-coming-soon/'
