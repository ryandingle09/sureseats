import logging

from google.appengine.api import search

from gsureseats.util.id_encoder import encoded_theater_id, decoded_theater_id


log = logging.getLogger(__name__)

THEATER_GEO_INDEX_NAME = 'theater_geo'


def index_theater_locations(enc_theater_ids, geopts):
    docs = []
    theater_tup = zip(enc_theater_ids, geopts)

    for tup in theater_tup:
        enc_theater_id, geopt = tup
        geopt = search.GeoPoint(*geopt)
        doc = search.Document(doc_id=enc_theater_id, fields=[search.GeoField(name='loc', value=geopt)])
        docs.append(doc)

    try:
        idx = search.Index(name=THEATER_GEO_INDEX_NAME)
        log.debug('Indexing %s' % docs)
        idx.put(docs)
    except search.Error:
        log.exception('Put failed')


def index_theater(theater):
    if theater.location:
        index_theater_locations([encoded_theater_id(theater.key)], [(theater.location.lat, theater.location.lon)])
