from flask import json


class APIException(Exception):
    def __init__(self, code, message, error_code, details=None):
        self.code = code
        self.message = message
        self.error_code = error_code
        self.details = details

    def __str__(self):
        return "error_code: %s, message: %s, details: %s" % (self.error_code, self.message, self.details)

    def to_dict(self):
        return dict(message=self.message, error_code=self.error_code, details=self.details)


class InvalidFilterPropertyException(APIException):
    def __init__(self, p):
        APIException.__init__(self, 400, 'Filtering against non-existent property', 'COLLECTIONS_FILTER_UNKNOWN_PROPERTY', p)


class InvalidOrderPropertyException(APIException):
    def __init__(self, p):
        APIException.__init__(self, 400, 'Sorting against non-existent property', 'COLLECTIONS_ORDER_UNKNOWN_PROPERTY', p)


class BadValueException(APIException):
    def __init__(self, p, m):
        APIException.__init__(self, 400, "Bad value for %s: %s" % (p, m), 'BAD_VALUE', p)


class DeviceIdRequiredException(APIException):
    def __init__(self):
        APIException.__init__(self, 400, 'Device must be uniquely identified', 'BAD_VALUE', 'X-Globe-SureSeats-DeviceId')


class TransactionConflictException(APIException):
    def __init__(self, state_strs):
        APIException.__init__(self, 409, 'Action not allowed in this transaction state', 'TX_INVALID_STATE', state_strs)


class AlreadyCancelledException(APIException):
    def __init__(self):
        # FIXME: Hard-coded list of allowed methods
        APIException.__init__(self, 405, 'Transaction already cancelled', 'TX_INVALID_STATE', ['GET'])


class NotFoundException(APIException):
    def __init__(self, details):
        APIException.__init__(self, 404, 'Not found', 'NOT_FOUND', details)


class ClientAuthException(APIException):
    def __init__(self, message):
        APIException.__init__(self, 401, message, 'AUTH_FAILED')


class APISystemError(APIException):
    def __init__(self, message, details=None):
        APIException.__init__(self, 500, message, 'SYS_ERROR', details)


class InvalidActionException(APIException):
    def __init__(self, message=None):
        APIException.__init__(self, 400, message, 'INVALID_ACTION')


class ThirdPartyServiceError(APIException):
    def __init__(self, message, details=None):
        APIException.__init__(self, 400, message, 'CONN_ERROR', details)


class SureSeatsRegisterFailedException(APIException):
    def __init__(self, message, details=None):
        APIException.__init__(self, 400, message, 'REGISTER_FAILED', details)


class SureSeatsUpdateAccountFailedException(APIException):
    def __init__(self, message, details=None):
        APIException.__init__(self, 400, message, 'UPDATEACCOUNT_FAILED', details)


class SureSeatsLoginFailedException(APIException):
    def __init__(self, message):
        APIException.__init__(self, 401, message, 'LOGIN_FAILED')


class SureSeatsForgotPasswordFailedException(APIException):
    def __init__(self, message):
        APIException.__init__(self, 400, message, 'FORGOTPASSWORD_FAILED')


class SureSeatsCancelTransactionFailedException(APIException):
    def __init__(self, message):
        APIException.__init__(self, 400, message, 'CANCEL_FAILED')


class SureSeatsMPassLoadingFailedException(APIException):
    def __init__(self, message):
        APIException.__init__(self, 400, message, 'RELOAD_FAILED')


class SureSeatsMPassBalanceFailedException(APIException):
    def __init__(self, message):
        APIException.__init__(self, 400, message, 'BALANCE_FAILED')
