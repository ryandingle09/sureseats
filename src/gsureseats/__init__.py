import copy_reg
import logging.config
import os
import sys
import types
import yaml

sys.path.insert(0, 'libs')
sys.path.insert(0, 'libs.zip')

from flask import Flask, request, abort, jsonify
from flask.ext.babel import Babel
from flask.ext.bootstrap import Bootstrap

from google.appengine.api import modules
from google.appengine.api.app_identity import get_application_id

from gsureseats.routes import api, callback, task, testcase, web
from gsureseats.tx.state_machine import to_state_str
from gsureseats.util.id_encoder import encoded_theater_id, encode_id_paths
from gsureseats.util.logging import bind_context_logger_to_root, unbind_context_logger_from_root
from gsureseats.util.admin import mask_cc_numbers
from gsureseats.util import pickle_reduce_method

#import requests_toolbelt.adapters.appengine
#requests_toolbelt.adapters.appengine.monkeypatch()

copy_reg.pickle(types.MethodType, pickle_reduce_method)

app = Flask(__name__)
Bootstrap(app)
babel = Babel(app)

app.config['BOOTSTRAP_USE_MINIFIED'] = True
app.config['BOOTSTRAP_USE_CDN'] = True
app.config['BOOTSTRAP_FONTAWESOME'] = True
app.config['SECRET_KEY'] = 'devkey'
app.config['CSRF_ENABLED'] = True
app.config['BABEL_DEFAULT_TIMEZONE'] = 'Asia/Manila'

app.register_blueprint(api, url_prefix='/api/0')
app.register_blueprint(web, url_prefix='/admin')
app.register_blueprint(task, url_prefix='/tasks')
app.register_blueprint(testcase, url_prefix='/test-case')
app.register_blueprint(callback, url_prefix='/callback/0')

app.jinja_env.globals.update(encoded_theater_id=encoded_theater_id)
app.jinja_env.globals.update(encode_id_paths=encode_id_paths)
app.jinja_env.globals.update(to_state_str=to_state_str)
app.jinja_env.globals.update(enumerate=enumerate)
app.jinja_env.globals.update(mask_cc_numbers=mask_cc_numbers)


@app.before_request
def bind_context_logger():
    bind_context_logger_to_root()

@app.teardown_request
def unbind_context_logger(exception=None):
    unbind_context_logger_from_root()

# Configure logging
with open('res/log-gcloud_bucket_download_config.yaml', 'r') as f:
    log_config = yaml.load(f)


log_config.setdefault('version', 1)
logging.config.dictConfig(log_config)


# Ensure subdomain and either CURRENT_VERSION_ID or module name match (or we have no subdomain)
@app.before_request
def check_subdomain_against_version():
    cur_ver = os.environ['CURRENT_VERSION_ID']

    if cur_ver == 'testbed-version':
        # in tests? yield
        return

    major_ver, minor_ver = cur_ver.split('.')

    if len(request.host) >= 9 and request.host[:9] == 'localhost' or request.host[:13] == '192.168.33.10':
        # dev server? yield
        return

    host_parts = request.host.split('.')
    domain_terminal = host_parts[0].split('-dot-')[0]
    app_id = get_application_id()
    module_name = modules.get_current_module_name()

    # Default case: running on default version, no app_version_domain
    if domain_terminal == app_id:
        return

    if domain_terminal == major_ver:
        return

    if domain_terminal == module_name:
        return

    base_path = "%s%s" % (request.script_root, request.path)

    # Special case: Requeue task in the proper version
    if base_path == '/tasks/callback/trigger_listeners':
        import google.appengine.api.taskqueue

        google.appengine.api.taskqueue.taskqueue.add(url=base_path, queue_name='transactions',
                target=domain_terminal, params={'channel_name': request.values['channel_name']})

        return jsonify(status='redirected')

    # FAILURE: Non-existent version
    abort(404)
