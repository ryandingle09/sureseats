from __future__ import absolute_import

import logging
import re

from google.appengine.api import memcache


log = logging.getLogger(__name__)


def make_enum(enum_type, *seq_enums, **named_enums):
    en = dict(zip(seq_enums, range(len(seq_enums))), **named_enums)
    reverse = dict((value, key) for key, value in en.iteritems())
    en['reverse_mapping'] = reverse

    return type(enum_type, (), en)

def roman_to_numeric(w):
    romans = [('M', 1000), ('CM', 900), ('D', 500), ('CD', 400),  ('C', 100), ('XC', 90),
            ('L', 50), ('XL', 40), ('X', 10), ('IX', 9), ('V', 5), ('IV', 4), ('I', 1)]
    mapper = dict(romans)
    syms = [t[0] for t in reversed(romans)]
    stack = list(w)
    stack.reverse()
    val = 0

    while stack:
        cur_val = stack[-1]

        if len(stack) > 1:
            peek = stack[-1] + stack[-2]
        else:
            peek = cur_val

        while syms and syms[-1] != cur_val and syms[-1] != peek:
            syms.pop()

        val += mapper[syms[-1]]

        if syms[-1] == cur_val:
            stack.pop()
        elif syms[-1] == peek:
            stack.pop()
            stack.pop()

    return val

def parse_cast(cast):
    """
    >>> parse_cast('foo')
    ['foo']
    >>> parse_cast('foo, bar')
    ['foo', 'bar']
    >>> parse_cast('foo, bar &amp; baz')
    ['foo', 'bar', 'baz']
    """

    if not cast:
        return []

    log.debug("parse_cast('%s')" % cast)
    cast_list = cast.split(',')

    addtl_cast_list = cast_list[-1].split('&amp;')
    cast_list.pop(-1)
    cast_list.extend(addtl_cast_list)

    addtl_cast_list = cast_list[-1].split('&')
    cast_list.pop(-1)
    cast_list.extend(addtl_cast_list)

    return [c.strip() for c in cast_list]

def list_to_string(cast):
    cs = ''

    for i,c in enumerate(cast):
        if (i+1) == len(cast):
            cs = cs + c
        else:
            cs = cs + c + ','

    return cs

def pickle_reduce_method(m):
    return (getattr, (m.__self__, m.__func__.__name__))
