from __future__ import absolute_import

import flask
import logging

from StringIO import StringIO
import StringIO as si

import unicodedata

class NewStringIO(StringIO):

    def getvalue(self):
        """
        Retrieve the entire contents of the "file" at any time before
        the StringIO object's close() method is called.

        The StringIO object can accept either Unicode or 8-bit strings,
        but mixing the two may take some care. If both are used, 8-bit
        strings that cannot be interpreted as 7-bit ASCII (that use the
        8th bit) will cause a UnicodeError to be raised when getvalue()
        is called.
        """
        si._complain_ifclosed(self.closed)
        if self.buflist:
            msg = []
            for x in self.buflist:
                try:
                    x = unicodedata.normalize("NFKD", unicode(x))
                    msg.append(x)
                except UnicodeError:
                    x = repr(x)
                    msg.append(x)
            self.buf += ''.join(msg)
            self.buflist = []
        return self.buf


class InRequestContextLoggingHandler(logging.Handler):
    def __init__(self, level=logging.NOTSET):
        logging.Handler.__init__(self, level)

        self.__default_formatter = logging.Formatter("%(asctime)s|%(levelname)s\t  %(message)s", datefmt="%I:%M:%S %p") # always define a format.

    def emit(self, rec):
        if flask.has_request_context():
            self.__bind_stream_if_necessary()

            return flask.g.gsureseats_logger.emit(rec)
        else:
            return None

    def __bind_stream_if_necessary(self):
        if not getattr(flask.g, 'gsureseats_logger', None):
            log_target = StringIO()
            #log_target = NewStringIO()
            flask.g.gsureseats_logger_target = log_target
            flask.g.gsureseats_logger = logging.StreamHandler(stream=log_target)
            flask.g.gsureseats_logger.setFormatter(self.__default_formatter)

def get_current_context_log():
    return flask.g.gsureseats_logger_target.getvalue()


def bind_context_logger_to_root():
    root_logger = logging.getLogger()
    root_logger.addHandler(LOG_HANDLER)

    global __bind_count
    __bind_count += 1

def unbind_context_logger_from_root():
    global __bind_count
    __bind_count -= 1

    if __bind_count <= 0:
        root_logger = logging.getLogger()
        root_logger.removeHandler(LOG_HANDLER)
        __bind_count = 0


LOG_HANDLER = InRequestContextLoggingHandler()
__bind_count = 0
