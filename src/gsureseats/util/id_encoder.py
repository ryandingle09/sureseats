# ID path encoding
from __future__ import absolute_import

import logging

from base64 import urlsafe_b64encode, urlsafe_b64decode
from uuid import UUID


log = logging.getLogger(__name__)


def encode_id_paths(*ids):
    return '~'.join(map(str, ids))

def decode_id_paths(id_path):
    return id_path.split('~')

def encode_uuid(uuid):
    encoded = urlsafe_b64encode(uuid.bytes)

    return encoded[:-2] # trim trailing padding (==), we don't need it.

def decode_uuid(enc_uuid):
    enc_uuid_padded = str(enc_uuid + '==')
    uuid_bytes = urlsafe_b64decode(enc_uuid_padded)

    if len(uuid_bytes) != 16:
        log.debug("Did not get a correct UUID, bailing")

        return None

    return UUID(bytes=uuid_bytes)

def encoded_theater_id(theater_key):
    theater_id = theater_key.id()
    parent_org = UUID(theater_key.parent().id())

    return encode_id_paths(encode_uuid(parent_org), theater_id)

def decoded_theater_id(enc_theater_id):
    ids = decode_id_paths(enc_theater_id)

    if len(ids) < 2:
        return (None, None)

    return (decode_uuid(ids[0]), int(ids[1]))
