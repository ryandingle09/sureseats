from __future__ import absolute_import

import datetime
import logging
import re
import StringIO
import time

from decimal import Decimal
from itertools import groupby
from operator import attrgetter
from uuid import uuid4 as uuid_gen

from google.appengine.ext import ndb
from google.appengine.api import memcache
from google.appengine.api import taskqueue

from gsureseats.heuristics import movies, schedules
from gsureseats.models import (Cinema, Device, Feed, Movie, ReservationTransaction, Schedule, Theater, TheaterOrganization,
        SureSeatsActionHistory, SureSeatsCustomerAccount, DATE_FORMAT, TIME_FORMAT, DATETIME_FORMAT)
from gsureseats.orgs import AYALA_MALLS, GLOBE, ROTTEN_TOMATOES
from gsureseats.util import id_encoder, parse_cast
from gsureseats.util.password_encoder import encrypt_password, decrypt_password

from gsureseats.settings import CMS_SURESEATS_DIGITALVENTURES_BASE_URL

import requests

log = logging.getLogger(__name__)

ALLOWED_EXTENSIONS = set(['csv', 'txt'])
ALLOWED_IMAGE_CONTENT_TYPE = ['image/png', 'image/jpg', 'image/jpeg', 'image/gif']


def create_theaterorg_params(form):
    theaterorg = TheaterOrganization(key=ndb.Key(TheaterOrganization, str(uuid_gen())))
    form.populate_obj(theaterorg)
    theaterorg.put()

    return theaterorg

def create_theater_params(form, theaterorg_id):
    theaterorg_key = ndb.Key(TheaterOrganization, str(theaterorg_id))
    theater = Theater(parent=theaterorg_key)
    form.populate_obj(theater)

    if form.latitude.data and form.longitude.data:
        theater.location = ndb.GeoPt(form.latitude.data, form.longitude.data)

    theater.put()

    return theater

def create_cinema_params(form, encoded_theater_id):
    geo_pt = None
    theater = get_theater(encoded_theater_id)

    if form.latitude.data and form.longitude.data:
        geo_pt = ndb.GeoPt(form.latitude.data, form.longitude.data)

    cinema = Cinema()
    form.populate_obj(cinema)
    cinema.seat_map = []
    cinema.location = geo_pt
    theater.cinemas.append(cinema)
    theater.put()

def get_theaterorg(theaterorg_id):
    theaterorg_key = ndb.Key(TheaterOrganization, str(theaterorg_id))

    return theaterorg_key.get()

def get_theater(encoded_theater_id):
    (parent_org, theater_id) = id_encoder.decoded_theater_id(encoded_theater_id)
    theater_key = ndb.Key(Theater, theater_id, parent=ndb.Key(TheaterOrganization, str(parent_org)))

    return theater_key.get()

def get_theater_by_name(theater_name):
    theater = Theater.query(Theater.name==theater_name)

    return theater.get()

def get_theater_by_theatercode(theater_code):
    theater = Theater.query(Theater.org_theater_code==theater_code)

    return theater.get()

def get_cinema_entity(theater, cinema_name):
    cinemas = theater.cinemas

    for c in cinemas:
        if cinema_name == c.name:
            return c

def check_theaterorg(name):
    theaterorgs = TheaterOrganization.query(TheaterOrganization.name==name).fetch()

    if len(theaterorgs) > 0:
        return False

    return True

def check_theater(name, parent_key):
    theaterorg_key = ndb.Key(TheaterOrganization, str(parent_key))
    theaters = Theater.query(Theater.name==name, ancestor=theaterorg_key).fetch()

    if len(theaters) > 0:
        return False

    return True

def check_cinema(name, encoded_theater_id):
    is_not_existing = False
    theater = get_theater(encoded_theater_id)
    cinema_names = [c.name for c in theater.cinemas]

    if name not in cinema_names:
        is_not_existing = True

    return is_not_existing

def generate_seat_map(row_list, number_list):
    seat_map = []

    for r in row_list:
        for n in number_list:
            seat_map.append(str(r)+str(n))

    return seat_map

def add_seats(seats, extant_rows):
    seat_map = [r.split(',') for r in extant_rows]

    if seats.strip() != '':
        add_seats = seats.split(',')
        seat_map.append(filter(lambda s: s != '', add_seats))

    return seat_map

def create_movie(form):
    movie_dict = {}
    movie_dict['movie_title'] = form.canonical_title.data
    movie_dict['cast'] = parse_cast(form.cast_list.data)
    movie_dict['genre'] = form.genre.data
    movie_dict['synopsis'] = form.synopsis.data
    movie_dict['runtime_mins'] = float(form.runtime_mins.data) if form.runtime_mins.data else None
    movie_dict['advisory_rating'] = form.advisory_rating.data
    movie_dict['release_date'] = form.release_date.data
    movie_list = movies.transform({str(GLOBE): [movie_dict]})

    for m in movie_list:
        memcache.set('movie::new::key', m)

def check_movie(old_correlation_titles, new_correlation_titles, key):
    is_not_existing = True
    queryset = Movie.query(Movie.correlation_title.IN(new_correlation_titles), Movie.is_active==True)
    movie = queryset.get()

    if movie and movie.key != key:
       is_not_existing = False

    if old_correlation_titles != new_correlation_titles:
        for t in old_correlation_titles:
            cached_title = memcache.get("movie::correlation_title::%s" % t)

            if cached_title is not None:
                memcache.delete("movie::correlation_title::%s" % t)

    return is_not_existing

def create_schedule(form, theater):
    schedule_dict = {}
    schedule_dict['id'] = ''
    schedule_dict['movie_title'] = form.movie_title.data
    schedule_dict['screening'] = str(form.screening.data)
    schedule_dict['theater_code'] = theater.org_theater_code
    schedule_dict['cinema'] = form.cinema_code.data
    schedule_dict['variant'] = form.variant.data
    schedule_dict['uuid'] = str(orgs.GLOBE)
    schedule = schedules.convert_schedule(schedule_dict)

    return schedule

def create_sureseats_action_history(status, username, action, customer_id=None, details=None, description=None, message=None, platform='website'):
    log.debug("create_sureseats_action_history, creating action history...")

    customer_account_key = ndb.Key(SureSeatsCustomerAccount, str(customer_id)) if customer_id else None
    action_history = SureSeatsActionHistory(parent=customer_account_key) if customer_account_key else SureSeatsActionHistory()
    action_history.action = action
    action_history.username = username
    action_history.status = status
    action_history.details = details if details else ''
    action_history.description = description if description else ''
    action_history.message = message if message else ''
    action_history.platform = platform
    action_history.put()

    log.debug("create_sureseats_action_history, created action history...")

    return action_history

def create_sureseats_customer_account(username, password, **customer_details):
    log.debug("create_sureseats_customer_account, start creating customer_account...")

    customer_account = SureSeatsCustomerAccount(key=ndb.Key(SureSeatsCustomerAccount, str(customer_details['customer_id'])))
    customer_account.username = username
    customer_account.password = encrypt_password(password)
    customer_account.first_name = customer_details['first_name']
    customer_account.middle_name = customer_details['middle_name']
    customer_account.last_name = customer_details['last_name']
    customer_account.email = customer_details['email']
    customer_account.mobile = customer_details['mobile']
    customer_account.birth_date = parse_string_to_date(customer_details['birth_date'], fdate='%Y-%m-%d')
    customer_account.gender = customer_details['gender']
    customer_account.province = customer_details['province']
    customer_account.city = customer_details['city']
    customer_account.street = customer_details['street']
    customer_account.default_theater1 = customer_details['default_theater1']
    customer_account.default_theater2 = customer_details['default_theater2']
    customer_account.default_theater3 = customer_details['default_theater3']
    customer_account.put()

    log.debug("create_sureseats_customer_account, created customer_account...")

    return customer_account

def update_sureseats_customer_account(username, password, customer_id, **customer_details):
    log.debug("update_sureseats_customer_account, start updating customer_account...")

    customer_account_key = ndb.Key(SureSeatsCustomerAccount, str(customer_id))
    customer_account = customer_account_key.get()

    if not customer_account:
        log.debug("update_sureseats_customer_account, not found, start creating customer_account...")

        customer_account = SureSeatsCustomerAccount(key=ndb.Key(SureSeatsCustomerAccount, str(customer_id)))
        customer_account.username = username

    if password:
        log.debug("update_sureseats_customer_account, change password...")

        customer_account.password = encrypt_password(password)

    customer_account.first_name = customer_details['first_name']
    customer_account.middle_name = customer_details['middle_name']
    customer_account.last_name = customer_details['last_name']
    customer_account.email = customer_details['email']
    customer_account.mobile = customer_details['mobile']
    customer_account.birth_date = parse_string_to_date(customer_details['birth_date'], fdate='%Y-%m-%d')
    customer_account.gender = customer_details['gender']
    customer_account.province = customer_details['province']
    customer_account.city = customer_details['city']
    customer_account.street = customer_details['street']
    customer_account.default_theater1 = customer_details['default_theater1']
    customer_account.default_theater2 = customer_details['default_theater2']
    customer_account.default_theater3 = customer_details['default_theater3']
    customer_account.put()

    log.debug("update_sureseats_customer_account, updated customer_account...")

    return customer_account

def parse_sureseats_error_message(message):
    message_dict = {}
    message_list = message.replace('\n\n', '\n').split('\n')

    for message in message_list:
        if 'username' in message.lower():
            message_dict['username'] = message
        elif 'password' in message.lower():
            message_dict['password'] = message
        elif 'email' in message.lower():
            message_dict['email'] = message
        elif 'mobile' in message.lower():
            message_dict['mobile'] = message
        elif 'first name' in message.lower():
            message_dict['first_name'] = message
        elif 'middle initial' in message.lower():
            message_dict['middle_name'] = message
        elif 'last name' in message.lower():
            message_dict['last_name'] = message
        elif 'birthdate' in message.lower():
            message_dict['birth_date'] = message
        elif 'street' in message.lower():
            message_dict['street'] = message
        elif 'city' in message.lower():
            message_dict['city'] = message
        elif 'province' in message.lower():
            message_dict['province'] = message
        elif re.search(r'[0-9]{9}', message):
            message_dict['mobile'] = message
        elif re.search(r'.+\@.+\..+', message):
            message_dict['email'] = message

    return message_dict

def cancel_transaction_by_ticketcode(ticket_code):

    log.debug("cancel_transaction_by_referencenumber, querying transaction using ticket_code, %s..." % ticket_code)

    transaction = ReservationTransaction.query(ReservationTransaction.ticket.code==ticket_code).get()

    if transaction:
        log.debug("cancel_transaction_by_referencenumber, trasaction found, start cancellation...")

        transaction.is_cancelled = True
        transaction.date_cancelled = datetime.datetime.now()
        transaction.put()

        log.debug("cancel_transaction_by_referencenumber, transaction cancelled...")

        snackbar_cancel_url = CMS_SURESEATS_DIGITALVENTURES_BASE_URL + '/api/snacks/transactions_cancelled'

        data = {'confirmation_code': ticket_code}
        r = requests.post(snackbar_cancel_url, data=data)
        if r.status_code == 200:
            log.info('Snackbar Cancel Parameters: {}'.format(data))

def get_transaction(transaction_id):
    device_id, txn_id = id_encoder.decode_id_paths(transaction_id)
    device_key = ndb.Key(Device, device_id)
    transaction_key = ndb.Key(ReservationTransaction, txn_id, parent=device_key)

    return transaction_key.get()

# generates the characters from `c1` to `c2`, inclusive.
def char_range(c1, c2):
    for c in xrange(ord(c1), ord(c2)+1):
        yield c

def camelcase(s):
    s = s.replace('_',' ')

    return ' '.join(''.join([w[0].upper(), w[1:].lower()]) for w in s.split())

def parse_string_to_date(sdate, fdate='%Y-%m-%d'):
    ddate = None

    try:
        ddate = datetime.datetime.strptime(sdate, fdate)
    except ValueError as e:
        log.warn("ERROR!, parse_string_to_date, ValueError, failed to parse sdate(%s) to date..." % sdate)
        log.error(e)
    except Exception as e:
        log.warn("ERROR!, parse_string_to_date, failed to parse sdate(%s) to date..." % sdate)
        log.warn(e)

    return ddate

def parse_string_to_datetime(sdatetime, fdatetime='%Y-%m-%d %H:%M:%S'):
    ddatetime = None

    try:
        ddatetime = datetime.datetime.strptime(sdatetime, fdatetime)
    except ValueError as e:
        log.warn("ERROR!, parse_string_to_datetime, ValueError, failed to parse sdatetime(%s) to datetime..." % sdatetime)
        log.error(e)
    except Exception as e:
        log.warn("ERROR!, parse_string_to_datetime, failed to parse sdatetime(%s) to datetime..." % sdatetime)
        log.error(e)

    return ddatetime

def parse_date_to_string(ddate, fdate='%B %d, %Y'):
    sdate = None

    try:
        sdate = datetime.date.strftime(ddate, fdate)
    except ValueError as e:
        log.warn("ERROR!, parse_date_to_string, ValueError, failed to parse ddate(%s) to string..." % ddate)
        log.error(e)
    except Exception as e:
        log.warn("ERROR!, parse_date_to_string, failed to parse ddate(%s) to string..." % ddate)
        log.error(e)

    return sdate

def parse_time_to_string(ttime, ftime='%I:%M:%S %p'):
    stime = None

    try:
        stime = datetime.time.strftime(ttime, ftime)
    except ValueError as e:
        log.warn("ERROR!, parse_time_to_string, ValueError, failed to parse ttime(%s) to string..." % ttime)
        log.error(e)
    except Exception as e:
        log.warn("ERROR!, parse_time_to_string, failed to parse ttime(%s) to string..." % ttime)
        log.error(e)

    return stime

def parse_datetime_to_string(ddatetime, fdatetime='%m/%d/%Y %I:%M:%S %p'):
    sdatetime = None

    try:
        sdatetime = datetime.datetime.strftime(ddatetime, fdatetime)
    except ValueError as e:
        log.warn("ERROR!, parse_datetime_to_string, ValueError, failed to parse ddatetime(%s) to string..." % ddatetime)
        log.error(e)
    except Exception as e:
        log.warn("ERROR!, parse_datetime_to_string, failed to parse ddatetime(%s) to string..." % ddatetime)
        log.error(e)

    return sdatetime

def convert_string_to_time(time_parameter):
    if not isinstance(time_parameter, datetime.time) and time_parameter != None:
        times = re.split('\:', time_parameter)

        return datetime.time(int(times[0]), int(times[1]), int(times[2]))

    return time_parameter

def convert_timezone(date_parameter, hours, operation):
    if operation == '+':
        result = date_parameter + datetime.timedelta(hours=hours)
    else:
        result = date_parameter - datetime.timedelta(hours=hours)

    return result

def mask_cc_numbers(cc_number):
    cc_number = str(cc_number)

    return "*" * (len(cc_number) - 4) + cc_number[-4:]

def update_org_client_control(theater_id, org_uuid, allow_buy_ticket=False, allow_reserve_ticket=False):
    theater_ids = []
    theaterorg_key = ndb.Key(TheaterOrganization, org_uuid)
    theaterorg = theaterorg_key.get()

    log.info("update_org_client_control, current theaterorg.default_client_control: {}...".format(theaterorg.default_client_control))

    if theaterorg.default_client_control:
        theater_ids = theaterorg.default_client_control['theater_whitelist']

        if allow_buy_ticket or allow_reserve_ticket:
            if theater_id not in theater_ids:
                theater_ids.append(theater_id)
        else:
            if theater_id in theater_ids:
                theater_ids.remove(theater_id)

        theaterorg.default_client_control['theater_whitelist'] =  theater_ids
    else:
        if allow_buy_ticket or allow_reserve_ticket:
            theater_ids.append(theater_id)

            theaterorg.default_client_control = {'theater_whitelist': theater_ids}

    log.info("update_org_client_control, saving theaterorg with updated whitelisted theater_ids, {}...".format(theater_ids))

    theaterorg.put()

def dummy_tx_info(payload):
    reservations = []
    show_datetime = None
    slot = None
    payment_info = payload['payment'] if 'payment' in payload else {}
    user_info = payload['user'] if 'user' in payload else {}
    discount_info = payload['discount'] if 'discount' in payload else {}
    payment_type = payment_info.pop('type') if 'type' in payment_info else ''
    transaction_type = payload['transaction_type'] if 'transaction_type' in payload else ''

    if 'reservations' in payload:
        for reservation in payload['reservations']:
            if 'cinema' not in reservation and 'show_datetime' in reservation:
                log.warn("SKIP!, dummy_tx_info, cinema and show_datetime not in payload reservations...")

                continue

            org_uuid, theater_id = id_encoder.decoded_theater_id(reservation['theater'])
            theaterorg = ndb.Key(TheaterOrganization, str(org_uuid), Theater, theater_id)
            schedule = Schedule.find_schedule(theaterorg, reservation['cinema'], reservation['show_datetime'])
            show_datetime = datetime.datetime.strptime(reservation['show_datetime'], DATETIME_FORMAT)
            time = show_datetime.time()
            slot = [slot for slot in schedule.slots if slot.start_time == time]
            slot = slot[0] if slot else None

            reservation_dict = {}
            reservation_dict['cinema'] = reservation['cinema']
            reservation_dict['movie'] = reservation['movie']
            reservation_dict['schedule'] = str(schedule.key.id())
            reservation_dict['seat'] = reservation['seat']
            reservation_dict['theater'] = reservation['theater']
            reservation_dict['time'] = str(time)[:5]

            reservations.append(reservation_dict)

    date_created = datetime.datetime.now()
    theater = get_theater(reservations[0]['theater']) if reservations and 'theater' in reservations[0] else None
    movie = ndb.Key(Movie, reservations[0]['movie']).get() if reservations and 'movie' in reservations[0] else None
    reservation_fee = '25.00' if payment_type in ['client-initiated', 'reserve'] else '0.00'
    total_amount = (slot.price * len(reservations)) + Decimal(reservation_fee) if slot else '0.00'
    total_seat_price = slot.price + Decimal(reservation_fee) if slot else '0.00'

    if discount_info:
        discount_info['discount_type'] = ''
        discount_info['discount_value'] = '0'
        discount_info['original_total_amount'] = '0.00'
        discount_info['total_amount'] = '0.00'
        discount_info['total_discount'] = '0.00'

    ticket = {}
    ticket['code'] = 'SAMPLECODE0001'
    ticket['date'] = date_created.strftime(DATETIME_FORMAT)
    ticket['ref'] = 'SAMPLECODE0001'
    ticket['ticket_type'] = 'barcode'
    ticket['transaction_codes'] = ['SAMPLECODE0001']
    ticket['qrcodes'] = []
    ticket['extra'] = {}
    ticket['extra']['advisory_rating'] = movie.advisory_rating if movie else ''
    ticket['extra']['cinema_name'] = reservations['cinema'] if reservations and 'cinema' in reservations else ''
    ticket['extra']['discount_type'] = ''
    ticket['extra']['is_discounted'] = False
    ticket['extra']['movie_id'] = movie.key.id() if movie else ''
    ticket['extra']['movie_title'] = movie.canonical_title if movie else ''
    ticket['extra']['original_seat_price'] = str(total_seat_price)
    ticket['extra']['original_total_amount'] = str(total_amount)
    ticket['extra']['reservation_fee'] = reservation_fee
    ticket['extra']['runtime'] = movie.runtime_mins if movie else ''
    ticket['extra']['seat_count'] = str(len(reservations))
    ticket['extra']['seat_price'] = str(slot.price) if slot else '0.00'
    ticket['extra']['seating_type'] = slot.seating_type if slot else ''
    ticket['extra']['seats'] = ','.join([r['seat'] for r in reservations if 'seat' in r])
    ticket['extra']['show_datetime'] = show_datetime.strftime('%m/%d/%Y %I:%M %p') if show_datetime else ''
    ticket['extra']['theater_name'] = theater.name if theater else ''
    ticket['extra']['ticket_count'] = len(reservations)
    ticket['extra']['total_amount'] = str(total_amount)
    ticket['extra']['total_discount'] = '0.00'
    ticket['extra']['total_discounted_seats'] = '0'
    ticket['extra']['total_seat_discount'] = '0.00'
    ticket['extra']['total_seat_price'] = str(total_seat_price)
    ticket['extra']['variant'] = ''

    transaction = {}
    transaction['date_created'] = date_created
    transaction['reservations'] = reservations
    transaction['payment_info'] = payment_info
    transaction['user_info'] = user_info
    transaction['discount_info'] = discount_info
    transaction['payment_type'] = payment_type
    transaction['transaction_type'] = transaction_type
    transaction['ticket'] = ticket

    return transaction
