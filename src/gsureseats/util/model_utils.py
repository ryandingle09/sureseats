from __future__ import absolute_import


def camelcase(s):
    s = s.replace('_',' ')

    return ' '.join(''.join([w[0].upper(), w[1:].lower()]) for w in s.split())
