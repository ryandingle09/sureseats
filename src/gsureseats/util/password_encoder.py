import base64


def encrypt_password(password):
    enc = base64.b64encode(password)
    enc = enc.encode('hex')
    enc = enc[::-1]

    return enc


def decrypt_password(password):
    dec = password[::-1]
    dec = dec.decode('hex')
    dec = base64.b64decode(dec)
    return dec

