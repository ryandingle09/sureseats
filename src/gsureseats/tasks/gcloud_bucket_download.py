import os
import sys
import yaml
import time
import errno
import logging
from google.cloud import storage
from google.cloud.storage import Bucket
from pathlib import Path

log = logging.getLogger().addHandler(__name__)


def get_script_path():
    return os.path.dirname(os.path.realpath(sys.argv[0]))


class GCloudBucketDownloader(object):

    def __init__(self, config_file='config/gcloud_bucket_download_config.yaml'):
        """
        Looks for the config_file

        :param config_file:
            type: string
            description: config_file location
        """
        file_path = "{0}{1}{2}".format(get_script_path(), '/', config_file)
        self.config = yaml.load(open(file_path))

    def get_buckets(self):
        """
        Retrieves Buckets

        :return:
            type: list
            description: Returns list of buckets defined in the config file.
        """
        try:
            config_credentials = self.config['credentials']
        except KeyError as e:
            raise e

        storage_client = storage.Client.from_service_account_json(config_credentials)

        try:
            bucket_config_list = self.config['bucket_list']
        except KeyError as e:
            raise e

        if not isinstance(bucket_config_list, list):
            raise ValueError("bucket_list should be list")

        bucket_list = []
        if 'all' in bucket_config_list:
            bucket_list = list(storage_client.list_buckets())
        else:
            for bucket in bucket_config_list:

                bucket_chk = storage_client.lookup_bucket(bucket)

                if bucket_chk is None or not isinstance(bucket_chk, Bucket):
                    raise "Invalid bucket {0}".format(bucket)

            for bucket in bucket_config_list:
                bucket_list.append(storage_client.get_bucket(bucket))

        return bucket_list

    def get_bucket_content(self, bucket):
        """
        Retrieves bucket contents as blob object

        :param bucket:
            type: Bucket object
            description: Contains bucket information/properties
        :return:
            type: Blob object
            description: Contains blob information/properties
        """
        prefix = None
        if 'file_prefix' in self.config:
            if self.config['file_prefix'] is not None:
                prefix = self.config['file_prefix']

        max_results = None
        if 'max_results' in self.config:
            if self.config['max_results'] is not None and isinstance(self.config['max_results'], int):
                max_results = self.config['max_results']

        blobs = bucket.list_blobs(prefix=prefix, max_results=max_results)
        bucket_content = []
        for blob in blobs:
            bucket_content.append(blob)

        return bucket_content

    def download_content(self, blob):
        """
        Checks if file is existing
        if yes does nothing
        if no downloads the file

        :param blob:
            type: Blob object
            description: Contains blob information/properties
        :return:
            type: string
            description: Absolute path where the file is placed.
        """
        filename = blob.name.replace(' ', '_')
        download_dir = self.config['download_dir']

        file_path = "{0}/{1}/{2}/{3}".format(get_script_path(), download_dir, bucket.name, filename)

        dl_file = Path(file_path)
        if dl_file.is_file():
            # log.info("File {0} exists".format(filename))
            return None

        with self._safe_open_w(file_path) as f:
            temp_file_create = open(file_path, 'wr')
            blob.download_to_file(temp_file_create)
            temp_file_create.close()

        return file_path

    def _mkdir_p(self, path):
        """
        Creates a directory if unable to create directory raise

        :param path:
            type: string
            description: File path
        :return:
            type: None
        """
        try:
            os.makedirs(path)
        except OSError as exc:  # Python >2.5
            if exc.errno == errno.EEXIST and os.path.isdir(path):
                pass
            else:
                raise

    def _safe_open_w(self, path):
        """
        Open "path" for writing, creating any parent directories as needed.

        :param path:
            type: string
            description: File path
        :return:
            type: File
            description: opened file
        """
        self._mkdir_p(os.path.dirname(path))
        return open(path, 'w')


if __name__ == '__main__':

    gcb = GCloudBucketDownloader()

    bucket_list = gcb.get_buckets()

    for bucket in bucket_list:
        bucket_content_list = gcb.get_bucket_content(bucket)

        for content in bucket_content_list:
            content_path = gcb.download_content(content)

    end_time = time.time()
