import logging

from datetime import date

from google.appengine.ext.ndb import get_multi, put_multi

from gsureseats.models import Movie, Schedule

from gsureseats.feeds import sureseats

log = logging.getLogger(__name__)


def expire_movies():
    now = date.today()
    current_set = set(Movie.query(Movie.is_showing==True, Movie.is_expired==False).fetch(keys_only=True))
    movies_with_scheds = set([sl.movie for s in Schedule.query(Schedule.show_date>=now).fetch(projection=['slots.movie']) for sl in s.slots])
    expired_movie_keys = current_set - movies_with_scheds
    movies = get_multi(expired_movie_keys)

    log.debug("Expiring keys: %s", expired_movie_keys)

    for m in movies:
        m.is_expired = True

    put_multi(movies)

def unexpire_movies():
    now = date.today()
    expired_set = set(Movie.query(Movie.is_showing==True, Movie.is_expired==True).fetch(keys_only=True))
    movies_with_scheds = set([sl.movie for s in Schedule.query(Schedule.show_date>=now).fetch(projection=['slots.movie']) for sl in s.slots])
    unexpired_movie_keys = expired_set.intersection(movies_with_scheds)
    movies = get_multi(unexpired_movie_keys)

    log.debug("Resurrecting keys: %s", unexpired_movie_keys)

    for m in movies:
        m.is_expired = False

    put_multi(movies)

def unexpire_movies_comingsoon():
    now = date.today()
    unexpired_movie_keys = set(Movie.query(Movie.is_showing==False, Movie.is_expired==True).fetch(keys_only=True))
    movies = get_multi(unexpired_movie_keys)

    log.debug("Resurrecting keys: %s", unexpired_movie_keys)

    for m in movies:
        m.is_expired = False

    put_multi(movies)

def activate_movies_nowshowing():
    now = date.today()
    activated_movies = set(Movie.query(Movie.is_showing==False, Movie.release_date<=now).fetch(keys_only=True))
    movies = get_multi(activated_movies)
    is_showing = [m.is_showing for m in movies]
    put_multi(movies)

def deactivate_movies_comingsoon():
    now = date.today()
    deactivated_movies = set(Movie.query(Movie.is_showing==True, Movie.release_date>now).fetch(keys_only=True))
    movies = get_multi(deactivated_movies)
    is_showing = [m.is_showing for m in movies]
    put_multi(movies)

