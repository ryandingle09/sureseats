import logging
import re

from lxml import etree
from requests import api

from google.appengine.api import urlfetch
from google.appengine.ext import ndb

from gsureseats.models import Cinema, Theater, TheaterOrganization
from gsureseats.orgs import AYALA_MALLS
from gsureseats.settings import SURESEATS_BASE_URL
from gsureseats.util import admin


log = logging.getLogger(__name__)

TIMEOUT_DEADLINE = 45
SCHEDULE_URL = SURESEATS_BASE_URL + '?action=SCHEDULE'
THEATER_URL = SURESEATS_BASE_URL + '?action=THEATER'
SEATMAP_URL = SURESEATS_BASE_URL + '?action=SEATMAP&code=%s&id=%s'


urlfetch.set_default_fetch_deadline(TIMEOUT_DEADLINE)


def read_theaters():
    theater_list = []

    try:
        result = urlfetch.fetch(THEATER_URL, deadline=TIMEOUT_DEADLINE)

        if result.status_code == 200:
            xml_data = etree.fromstring(result.content)

            for t in xml_data.iter('Theater'):
                theater_code = t.find('id').text
                theater_name = t.find('label').text
                theater_logo = t.find('picture').text
                theater = get_existing_theater(theater_list, theater_name, theater_code)

                if theater:
                    log.debug("read_theaters, found, theater_name: %s, theater_code: %s..." % (theater_name, theater_code))

                    theater.logo = theater_logo

                    if theater not in theater_list:
                        theater_list.append(theater)
                else:
                    log.debug("read_theaters, not found, theater_name: %s, theater_code: %s..." % (theater_name, theater_code))

                    theater = create_new_theater(theater_name, theater_code)
                    theater.logo = theater_logo
                    theater_list.append(theater)
    except (etree.XMLSyntaxError) as e:
        log.warn("ERROR!, read_theaters, etree.XMLSyntaxError...")
        log.error(e)
    except Exception as e:
        log.warn("ERROR!, read_theaters...")
        log.error(e)

    if len(theater_list) != 0:
        ndb.put_multi(theater_list)

    return theater_list

def read_theaters2():
    theater_list = []

    try:
        result = urlfetch.fetch(SCHEDULE_URL, deadline=TIMEOUT_DEADLINE)

        if result.status_code == 200:
            xml_data = etree.fromstring(result.content)

            for sched in xml_data.iter('Schedule'):
                theater_name = sched.find('theater').text
                theater_code = sched.find('theater_code').text
                cinema_name = sched.find('cinema').text
                theater = get_existing_theater(theater_list, theater_name, theater_code)

                if theater:
                    log.debug("read_theaters2, found, theater_name: %s, theater_code: %s..." % (theater_name, theater_code))

                    if theater not in theater_list:
                        theater_list.append(theater)

                    create_cinema(theater, theater_code, cinema_name)
                else:
                    log.debug("read_theaters2, not found, theater_name: %s, theater_code: %s..." % (theater_name, theater_code))

                    theater = create_new_theater(theater_name, theater_code)
                    cinema = create_cinema(theater, theater_code, cinema_name)
                    theater_list.append(theater)
    except (etree.XMLSyntaxError) as e:
        log.warn("ERROR!, read_theaters2, etree.XMLSyntaxError...")
        log.error(e)
    except Exception as e:
        log.warn("ERROR!, read_theaters2...")
        log.error(e)

    if len(theater_list) != 0:
        ndb.put_multi(theater_list)

    return theater_list

def get_existing_theater(theater_list, theater_name, theater_code):
    theaters_pending = dict([(t.name, t) for t in theater_list])

    if theater_name in theaters_pending:
        return theaters_pending[theater_name]

    parent_key = ndb.Key(TheaterOrganization, str(AYALA_MALLS))
    queryset = Theater.query(ndb.OR(Theater.name==theater_name, Theater.org_theater_code==theater_code), ancestor=parent_key)

    if queryset.count() > 0:
        return queryset.fetch(1)[0]

    return None

def create_new_theater(theater_name, theater_code):
    theater = Theater(name=theater_name, org_theater_code=theater_code, parent=ndb.Key(TheaterOrganization, str(AYALA_MALLS)))

    return theater

def create_cinema(theater, theater_code, name):
    seat_count, seat_map = get_seat_count(theater_code, name)

    if name in [c.name for c in theater.cinemas]:
        cinema = admin.get_cinema_entity(theater, name)
        cinema.seat_count = str(seat_count)
        cinema.seat_map = seat_map

        log.debug("UPDATE!, via cinema_name with name: %s..." % name)
    else:
        cinema = Cinema()
        cinema.name = name
        cinema.seat_count = str(seat_count)
        cinema.seat_map = seat_map

        theater.cinemas.append(cinema)

        log.debug("CREATE!, cinema with name: %s..." % name)

def get_seat_count(code, cinema):
    seat_count = 0
    seat_map = []

    try:
        if code == 'ATC' and cinema in ['1']:
            log.warn("ATC!, get_seat_count, add suffix 1, update code, %s, cinema, %s..." % (code, cinema))

            code = '%s1' % code
        elif code == 'ATC' and cinema in ['2', '3', '4', '5']:
            log.warn("ATC!, get_seat_count, add suffix 2, update code, %s, cinema, %s..." % (code, cinema))

            code = '%s2' % code

        SEATS_URL = SEATMAP_URL % (code, cinema)
        result = urlfetch.fetch(SEATS_URL, deadline=TIMEOUT_DEADLINE)

        if result.status_code == 200:
            xml_data = etree.fromstring(result.content)

            for seatmap in xml_data.iter('Seatmap'):
                column_count, rows = parse_rowmap(seatmap.find('rowmap').text, seatmap.find('rows').text)
                seat_count += column_count
                seat_map.append(rows)
    except etree.XMLSyntaxError as e:
        log.warn("ERROR!, get_seat_count, etree.XMLSyntaxError...")
        log.error(e)

        # error, reset the values for seat_count and seat_map.
        seat_count = 0
        seat_map = []
    except Exception as e:
        log.warn("ERROR!, get_seat_count...")
        log.error(e)

        # error, reset the values for seat_count and seat_map.
        seat_count = 0
        seat_map = []

    return seat_count, seat_map

def parse_rowmap(rowmap, row):
    results = map(lambda s: s.strip(), rowmap.split(';'))
    rowmap = [(row + i if i.isdigit() else i) for i in results if i != '']

    return len(rowmap), rowmap
