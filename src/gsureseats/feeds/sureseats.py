import logging
import re
import requests

from datetime import datetime
from lxml import etree
from uuid import uuid4 as uuid_gen

from google.appengine.api import urlfetch
from google.appengine.ext import ndb

from gsureseats.exceptions.api import (SureSeatsCancelTransactionFailedException, SureSeatsForgotPasswordFailedException,
        SureSeatsLoginFailedException, SureSeatsMPassBalanceFailedException, SureSeatsMPassLoadingFailedException,
        SureSeatsRegisterFailedException, SureSeatsUpdateAccountFailedException)
from gsureseats.heuristics.movies import variant_from_title # XXX Bad form here; feeds shouldn't pull in from heuristics
from gsureseats.models import SureSeatsCustomerAccount
from gsureseats.orgs import AYALA_MALLS
from gsureseats.settings import SURESEATS_BASE_URL, CMS_SURESEATS_DIGITALVENTURES_BASE_URL, STORAGE_SCHEDULE, STORAGE_NOWSHOWING, STORAGE_COMINGSOON
from gsureseats.tx.sureseats import create_sureseats_request
from gsureseats.util import parse_cast
from gsureseats.util.admin import (cancel_transaction_by_ticketcode, create_sureseats_action_history,
        create_sureseats_customer_account, update_sureseats_customer_account, parse_sureseats_error_message,
        parse_date_to_string, parse_string_to_date, get_theater, get_theater_by_name, get_theater_by_theatercode)
from gsureseats.util.id_encoder import decoded_theater_id, encoded_theater_id
from gsureseats.util.password_encoder import encrypt_password, decrypt_password

import cloudstorage as gcs
import traceback

log = logging.getLogger(__name__)

SCHEDULE_URL = SURESEATS_BASE_URL + '?action=SCHEDULE'
SCHEDULE2_URL = SURESEATS_BASE_URL + '?action=SCHEDULE2'
NOW_SHOWING_URL = SURESEATS_BASE_URL + '?action=NOWSHOWING'
COMING_SOON_URL = SURESEATS_BASE_URL + '?action=COMINGSOON'

BATCH_SIZE = 100
TIMEOUT_DEADLINE = 45
SEATING_TYPES = ['Free Seating', 'Guaranteed Seats', 'Reserved Seating', 'Guaranteed Seating']
SUFFIX_SEATINGTYPE_PATTERN = '(' + '|'.join(SEATING_TYPE for SEATING_TYPE in SEATING_TYPES) + ')'
SUFFIX_SEATINGTYPE_MATCH = r'\((?i)(' + SUFFIX_SEATINGTYPE_PATTERN + ')\)$'

urlfetch.set_default_fetch_deadline(TIMEOUT_DEADLINE)


def save_to_gae(bucket_name,data_list):
    bucket = str(bucket_name)
    filename = bucket + str(datetime.now())
    write_retry_params = gcs.RetryParams(backoff_factor=1.1)
    gcs_file = gcs.open(filename,
                        'w',
                        content_type='text/plain',
                        retry_params=write_retry_params)
    for a in data_list:
        gcs_file.write(str(a) + '\n')
    gcs_file.close()


def read_schedules():
    all_schedules = []

    try:
        result = urlfetch.fetch(SCHEDULE_URL, deadline=TIMEOUT_DEADLINE)

        if result.status_code == 200:
            xml_data = etree.fromstring(result.content)

            for ia in xml_data.iter('Schedule'):
                schedule_dict = {}

                for elem in ia:
                    schedule_dict[elem.tag] = elem.text

                schedule_dict['cinema_code'] = schedule_dict['cinema']
                schedule_dict['cinema_name'] = schedule_dict['cinema']
                schedule_dict['price'] = schedule_dict['price'][4:]
                schedule_dict['variant'] = variant_from_title(schedule_dict['movie_title'])
                schedule_dict['uuid'] = AYALA_MALLS
                all_schedules.append(schedule_dict)
    except (etree.XMLSyntaxError, urlfetch.ResponseTooLargeError) as e:
        log.warn("ERROR!, read_schedules, etree.XMLSyntaxError or urlfetch.ResponseTooLargeError...")
        log.error(e)
    except Exception as e:
        log.warn("ERROR!, read_schedules...")
        log.error(e)

    return all_schedules


def read_schedules2():
    all_schedules = []

    try:
        result = urlfetch.fetch(SCHEDULE2_URL, deadline=TIMEOUT_DEADLINE)

        if result.status_code == 200:
            movie_xpath_prefix_init = "//movie[@code='%s']"
            location_xpath_prefix_init = "//movie[@code='%s']/location[@code='%s']"
            cinema_xpath_prefix_init = "//movie[@code='%s']/location[@code='%s']/cinema[@code='%s']"
            screening_xpath_prefix_init = "//movie[@code='%s']/location[@code='%s']/cinema[@code='%s']/screening[@id='%s']"

            xml_data = etree.fromstring(result.content)
            movie_ids = [ia.attrib['code'].strip() for ia in xml_data.iter('movie') if 'code' in ia.attrib]

            for movie_id in movie_ids:
                movie_xpath_prefix = movie_xpath_prefix_init % movie_id
                movie_title_xpath = movie_xpath_prefix + "/movie_title/text()"
                movie_title = xml_data.xpath(movie_title_xpath)[0].strip() if xml_data.xpath(movie_title_xpath) else ''
                advisory_rating_xpath = movie_xpath_prefix + "/rating/text()"
                advisory_rating = xml_data.xpath(advisory_rating_xpath)[0].strip() if xml_data.xpath(advisory_rating_xpath) else ''
                location_xpath = movie_xpath_prefix + "/location"
                locations = xml_data.xpath(location_xpath) if xml_data.xpath(location_xpath) else []
                theater_codes = [ia.attrib['code'].strip() for ia in locations if 'code' in ia.attrib]

                if not movie_title:
                    log.warn("SKIP!, missing movie_title...")

                    continue

                log.debug("read_schedules2, movie_id, %s, movie_title, %s..." % (movie_id, movie_title))

                for theater_code in theater_codes:
                    location_xpath_prefix = location_xpath_prefix_init % (movie_id, theater_code)
                    theater_name_xpath = location_xpath_prefix + "/theater/text()"
                    theater_name = xml_data.xpath(theater_name_xpath)[0].strip() if xml_data.xpath(theater_name_xpath) else ''
                    cinema_xpath = location_xpath_prefix + "/cinema"
                    cinemas = xml_data.xpath(cinema_xpath) if xml_data.xpath(cinema_xpath) else []
                    cinema_codes = [ia.attrib['code'].strip() for ia in cinemas if 'code' in ia.attrib]

                    log.debug("read_schedules2, theater_code, %s, theater_name, %s..." % (theater_code, theater_name))

                    for cinema_code in cinema_codes:
                        cinema_xpath_prefix = cinema_xpath_prefix_init % (movie_id, theater_code, cinema_code)
                        cinema_name_xpath = cinema_xpath_prefix + "/cinema_label/text()"
                        cinema_name = xml_data.xpath(cinema_name_xpath)[0].strip() if xml_data.xpath(cinema_name_xpath) else ''
                        price_xpath = cinema_xpath_prefix + "/price/text()"
                        price = xml_data.xpath(price_xpath)[0].strip() if xml_data.xpath(price_xpath) else ''
                        screening_xpath = cinema_xpath_prefix + "/screening"
                        screenings = xml_data.xpath(screening_xpath) if xml_data.xpath(screening_xpath) else []
                        schedule_ids = [ia.attrib['id'].strip() for ia in screenings if 'id' in ia.attrib]

                        if not cinema_name:
                            log.warn("SKIP!, missing cinema_name...")

                            continue

                        if not price:
                            log.warn("SKIP!, missing price...")

                            continue

                        log.debug("read_schedules2, cinema_code, %s, cinema_name, %s..." % (cinema_code, cinema_name))

                        for schedule_id in schedule_ids:
                            screening_xpath_prefix = screening_xpath_prefix_init % (movie_id, theater_code, cinema_code, schedule_id)
                            screening_details_xpath = screening_xpath_prefix + "/text()"
                            screening_details = xml_data.xpath(screening_details_xpath)[0].strip() if xml_data.xpath(screening_details_xpath) else ''
                            screening = re.sub(SUFFIX_SEATINGTYPE_MATCH, '', screening_details).strip()
                            seat_type = re.search(SUFFIX_SEATINGTYPE_MATCH, screening_details).group(1).strip()

                            if not screening:
                                log.warn("SKIP!, missing screening...")

                                continue

                            if not seat_type:
                                log.warn("SKIP!, missing seat_type...")

                                continue

                            log.debug("read_schedules2, schedule_id, %s, screening, %s, seat_type, %s..." % (schedule_id, screening, seat_type))

                            schedule_dict = {}
                            schedule_dict['id'] = str(schedule_id)
                            schedule_dict['screening'] = screening
                            schedule_dict['movie_id'] = str(movie_id)
                            schedule_dict['movie_title'] = movie_title
                            schedule_dict['theater_code'] = theater_code
                            schedule_dict['cinema_code'] = str(cinema_code)
                            schedule_dict['cinema_name'] = cinema_name
                            schedule_dict['price'] = price.upper().replace('PHP', '').strip()
                            schedule_dict['seat_type'] = seat_type
                            schedule_dict['variant'] = variant_from_title(movie_title)
                            schedule_dict['uuid'] = AYALA_MALLS
                            all_schedules.append(schedule_dict)

    except (etree.XMLSyntaxError, urlfetch.ResponseTooLargeError) as e:
        log.warn("ERROR!, read_schedules2, etree.XMLSyntaxError or urlfetch.ResponseTooLargeError...")
        log.error(e)
    except Exception as e:
        log.warn("ERROR!, read_schedules2...")
        log.error(e)

    datalist = []
    for each in all_schedules:
        datalist.append(each['id'])
    save_to_gae(STORAGE_SCHEDULE, all_schedules)
    return all_schedules


def read_movie_feed(feed_url, root):
    movies = []

    try:
        result = urlfetch.fetch(feed_url, deadline=TIMEOUT_DEADLINE)

        if result.status_code == 200:
            xml_data = etree.fromstring(result.content)

            for ia in xml_data.iter(root):
                movie_dict = {}

                for elem in ia:
                    if elem.tag == 'picture':
                        movie_dict['image_url'] = elem.text
                    elif elem.tag == 'cast':
                        movie_dict['cast'] = parse_cast(elem.text)
                    elif elem.tag == 'rating':
                        movie_dict['rating'] = elem.text
                    elif elem.tag == 'tentative':
                        movie_dict['release_date'] = datetime.strptime(elem.text, '%m/%d/%Y').date()
                    else:
                        movie_dict[elem.tag] = elem.text

                movies.append(movie_dict)
    except (etree.XMLSyntaxError, urlfetch.ResponseTooLargeError) as e:
        log.warn("ERROR!, read_movie_feed, etree.XMLSyntaxError or urlfetch.ResponseTooLargeError...")
        log.error(e)
        log.error(traceback.format_exc())
    except Exception as e:
        log.warn("ERROR!, read_movie_feed...")
        log.error(e)

    if root == 'Now_Showing':
        save_to_gae(STORAGE_NOWSHOWING, movies)
    else:
        save_to_gae(STORAGE_COMINGSOON, movies)
    return movies


def read_movies_now_showing():
    return read_movie_feed(NOW_SHOWING_URL, 'Now_Showing')


def read_movies_coming_soon():
    return read_movie_feed(COMING_SOON_URL, 'Coming_Soon')


def read_advisory_ratings_for_movies():
    scheds = read_schedules()
    movies_and_ratings = set([(s['movie_title'], s['rating']) for s in scheds])

    return [dict(movie_title=s[0], advisory_rating=s[1]) for s in movies_and_ratings]


def read_transaction_history(customer_id):
    transaction_history = []

    def __send_sureseats_req(s, req):
        response = s.send(req, timeout=TIMEOUT_DEADLINE)

        return response

    try:
        payload = {'action': 'TRANS_HISTORY', 'cust_id': customer_id}

        s = requests.Session()
        req = create_sureseats_request(SURESEATS_BASE_URL, payload)
        result = __send_sureseats_req(s, req)

        if result.status_code == 200:
            xml_data = etree.fromstring(str(result.text))
            claim_code = ''
            pay_type = ''
            for ia in xml_data.iter('History'):
                txhistory_dict = {}
                for elem in ia:
                    txhistory_dict[elem.tag] = elem.text
                    if elem.tag == 'claim_code':
                        claim_code = elem.text
                    if elem.tag == 'status':
                        status = elem.text
                        if status == 'Claimed' or status == 'Purchased':
                            transaction_history_url = CMS_SURESEATS_DIGITALVENTURES_BASE_URL + '/api/snacks/claim_snack'
                            data = {'confirmation_code': claim_code}
                            r = requests.post(transaction_history_url, data=data)
                            if r.status_code == 200:
                                result = r.json()
                                txhistory_dict['snackbar_order_status'] = result['result']['allow_snackbar']

                transaction_history.append(txhistory_dict)
    except (etree.XMLSyntaxError, urlfetch.ResponseTooLargeError) as e:
        log.warn("ERROR!, read_transaction_history, etree.XMLSyntaxError or urlfetch.ResponseTooLargeError...")
        log.error(e)
    except Exception as e:
        log.warn("ERROR!, read_transaction_history...")
        log.error(e)
    return transaction_history


def read_mpass_transaction_history(customer_id):
    mpass_transaction_history = []

    def __send_sureseats_req(s, req):
        response = s.send(req, timeout=TIMEOUT_DEADLINE)

        return response

    try:
        payload = {'action': 'MPASS_TRANS_HISTORY', 'cust_id': customer_id}

        s = requests.Session()
        req = create_sureseats_request(SURESEATS_BASE_URL, payload)
        result = __send_sureseats_req(s, req)

        if result.status_code == 200:
            xml_data = etree.fromstring(str(result.text))

            for ia in xml_data.iter('History'):
                mpasstxhistory_dict = {}

                for elem in ia:
                    mpasstxhistory_dict[elem.tag] = elem.text
                mpass_transaction_history.append(mpasstxhistory_dict)
    except (etree.XMLSyntaxError, urlfetch.ResponseTooLargeError) as e:
        log.warn("ERROR!, read_mpass_transaction_history, etree.XMLSyntaxError or urlfetch.ResponseTooLargeError...")
        log.error(e)
    except Exception as e:
        log.warn("ERROR!, read_mpass_transaction_history...")
        log.error(e)
    return mpass_transaction_history


def register(username, password, **customer_details):
    customer_account = None
    default_theater1 = None
    default_theater2 = None
    default_theater3 = None

    def __send_sureseats_req(s, req):
        log.info("######################### START ############################")
        log.info(req.method)
        log.info(req.url)
        log.info("########################## END #############################")
        response = s.send(req, timeout=TIMEOUT_DEADLINE)
        return response

    try:
        password = decrypt_password(str(password).strip())

        if 'default_theater1' in customer_details and customer_details['default_theater1']:
            default_theater1 = get_theater(customer_details['default_theater1'])

        if 'default_theater2' in customer_details and customer_details['default_theater2']:
            default_theater2 = get_theater(customer_details['default_theater2'])

        if 'default_theater3' in customer_details and customer_details['default_theater3']:
            default_theater3 = get_theater(customer_details['default_theater3'])

        payload = {}
        payload['action'] = 'REGISTER'
        payload['un'] = username
        payload['pw'] = password
        payload['fname'] = customer_details['first_name']
        payload['mname'] = customer_details['middle_name']
        payload['lname'] = customer_details['last_name']
        payload['email'] = customer_details['email']
        payload['mobno'] = customer_details['mobile']
        payload['bday'] = customer_details['birth_date']
        payload['gender'] = customer_details['gender']
        payload['province'] = customer_details['province']
        payload['city'] = customer_details['city']
        payload['street'] = customer_details['street']
        payload['def_theater1'] = default_theater1.org_theater_code if default_theater1 and default_theater1.org_theater_code else ''
        payload['def_theater2'] = default_theater2.org_theater_code if default_theater2 and default_theater2.org_theater_code else ''
        payload['def_theater3'] = default_theater3.org_theater_code if default_theater3 and default_theater3.org_theater_code else ''

        s = requests.Session()
        req = create_sureseats_request(SURESEATS_BASE_URL, payload)
        result = __send_sureseats_req(s, req)

        if result.status_code != 200:
            log.warn("ERROR!, register, SureSeats register failed, status_code: %s..." % result.status_code)
            raise SureSeatsRegisterFailedException('Sorry, failed to register. Please try again.')

        log.info("######################### START ############################")
        log.info(result.text)
        log.info("########################## END #############################")

        xml = etree.fromstring(str(result.text))
        status = xml.xpath('/Account/Status/text()')[0].strip()

        if status != 'You have successfully registered to SureSeats.':
            log.warn("ERROR!, register, SureSeats register failed, status: %s..." % status)
            error_details = parse_sureseats_error_message(status)
            raise SureSeatsRegisterFailedException(status, details=error_details)

        customer_id = xml.xpath('/Account/Customer/text()')[0].strip()

        # add customer_id, username, and password. override the default theaters value in customer_details dict.
        customer_details['customer_id'] = customer_id
        customer_details['username'] = username
        customer_details['password'] = encrypt_password(password)
        customer_details['default_theater1'] = encoded_theater_id(default_theater1.key) if default_theater1 else ''
        customer_details['default_theater2'] = encoded_theater_id(default_theater2.key) if default_theater2 else ''
        customer_details['default_theater3'] = encoded_theater_id(default_theater3.key) if default_theater3 else ''
    except (etree.XMLSyntaxError, urlfetch.ResponseTooLargeError) as e:
        log.warn("ERROR!, register, etree.XMLSyntaxError or urlfetch.ResponseTooLargeError...")
        log.error(e)
        raise SureSeatsRegisterFailedException('Sorry, failed to register. Please try again.')
    return customer_details


def update_account(username, password, customer_id, **customer_details):
    customer_account = None
    default_theater1 = None
    default_theater2 = None
    default_theater3 = None

    def __send_sureseats_req(s, req):
        log.info("######################### START ############################")
        log.info(req.method)
        log.info(req.url)
        log.info("########################## END #############################")
        response = s.send(req, timeout=TIMEOUT_DEADLINE)
        return response

    try:
        password = decrypt_password(password) if password else ''

        if 'default_theater1' in customer_details and customer_details['default_theater1']:
            default_theater1 = get_theater(customer_details['default_theater1'])

        if 'default_theater2' in customer_details and customer_details['default_theater2']:
            default_theater2 = get_theater(customer_details['default_theater2'])

        if 'default_theater3' in customer_details and customer_details['default_theater3']:
            default_theater3 = get_theater(customer_details['default_theater3'])

        payload = {}
        payload['action'] = 'ACCOUNT'
        payload['cust_id'] = customer_id
        payload['pw'] = password
        payload['fname'] = customer_details['first_name']
        payload['mname'] = customer_details['middle_name']
        payload['lname'] = customer_details['last_name']
        payload['email'] = customer_details['email']
        payload['mobno'] = customer_details['mobile']
        payload['bday'] = customer_details['birth_date']
        payload['gender'] = customer_details['gender']
        payload['province'] = customer_details['province']
        payload['city'] = customer_details['city']
        payload['street'] = customer_details['street']
        payload['def_theater1'] = default_theater1.org_theater_code if default_theater1 and default_theater1.org_theater_code else ''
        payload['def_theater2'] = default_theater2.org_theater_code if default_theater2 and default_theater2.org_theater_code else ''
        payload['def_theater3'] = default_theater3.org_theater_code if default_theater3 and default_theater3.org_theater_code else ''

        s = requests.Session()
        req = create_sureseats_request(SURESEATS_BASE_URL, payload)
        result = __send_sureseats_req(s, req)

        if result.status_code != 200:
            log.warn("ERROR!, update_account, SureSeats update account failed, status_code: %s..." % result.status_code)
            raise SureSeatsUpdateAccountFailedException('Sorry, failed to update account. Please try again.')

        log.info("######################### START ############################")
        log.info(result.text)
        log.info("########################## END #############################")

        xml = etree.fromstring(str(result.text))
        status = xml.xpath('/Account/Status/text()')[0].strip()

        if status != 'You have successfully updated your SureSeats account.':
            log.warn("ERROR!, update_account, SureSeats update account failed, status: %s..." % status)

            error_details = parse_sureseats_error_message(status)
            raise SureSeatsUpdateAccountFailedException(status, details=error_details)

        # add customer_id, username, and password. override the default theaters value in customer_details dict.
        customer_details['customer_id'] = customer_id
        customer_details['username'] = username
        customer_details['password'] = encrypt_password(password) if password else ''
        customer_details['default_theater1'] = encoded_theater_id(default_theater1.key) if default_theater1 else ''
        customer_details['default_theater2'] = encoded_theater_id(default_theater2.key) if default_theater2 else ''
        customer_details['default_theater3'] = encoded_theater_id(default_theater3.key) if default_theater3 else ''
    except (etree.XMLSyntaxError, urlfetch.ResponseTooLargeError) as e:
        log.warn("ERROR!, update_account, etree.XMLSyntaxError or urlfetch.ResponseTooLargeError...")
        log.error(e)
        raise SureSeatsUpdateAccountFailedException('Sorry, failed to update account. Please try again.')

    return customer_details


def login(username, password):
    customer_account = None
    mpass_card = ''
    mpass_balance = ''

    def __send_sureseats_req(s, req):
        log.info("######################### START ############################")
        log.info(req.method)
        log.info(req.url)
        log.info("########################## END #############################")

        response = s.send(req, timeout=TIMEOUT_DEADLINE)

        return response

    try:
        password = decrypt_password(password)
        payload = {'action': 'LOGIN', 'un': username, 'pw': password}

        s = requests.Session()
        req = create_sureseats_request(SURESEATS_BASE_URL, payload)
        result = __send_sureseats_req(s, req)

        if result.status_code != 200:
            log.warn("ERROR!, login, SureSeats log-in failed, status_code: %s..." % result.status_code)

            raise SureSeatsLoginFailedException('Sorry, failed to log-in. Please try again.')

        log.info("######################### START ############################")
        log.info(result.text)
        log.info("########################## END #############################")

        xml = etree.fromstring(result.text.encode('utf-8').replace('&','&amp;'))
        status = xml.xpath('/CheckLogin/Status/text()')[0] if xml.xpath('/CheckLogin/Status/text()') else ''

        if status != 'SUCCESS':
            log.warn("ERROR!, login, SureSeats log-in failed, status: %s..." % status)
            raise SureSeatsLoginFailedException(status)

        customer_id = xml.xpath('/CheckLogin/CustID/text()')[0] if xml.xpath('/CheckLogin/CustID/text()') else ''
        birth_date = xml.xpath('/CheckLogin/BirthDay/text()')[0] if xml.xpath('/CheckLogin/BirthDay/text()') else ''
        default_theater1 = get_theater_by_name(xml.xpath('/CheckLogin/DefTheater1/text()')[0]) if xml.xpath('/CheckLogin/DefTheater1/text()') else None
        default_theater2 = get_theater_by_name(xml.xpath('/CheckLogin/DefTheater2/text()')[0]) if xml.xpath('/CheckLogin/DefTheater2/text()') else None
        default_theater3 = get_theater_by_name(xml.xpath('/CheckLogin/DefTheater3/text()')[0]) if xml.xpath('/CheckLogin/DefTheater3/text()') else None

        customer_details = {}
        customer_details['customer_id'] = customer_id
        customer_details['username'] = username
        customer_details['password'] = encrypt_password(password)
        customer_details['first_name'] = xml.xpath('/CheckLogin/FirstName/text()')[0] if xml.xpath('/CheckLogin/FirstName/text()') else ''
        customer_details['middle_name'] = xml.xpath('/CheckLogin/MiddleName/text()')[0] if xml.xpath('/CheckLogin/MiddleName/text()') else ''
        customer_details['last_name'] = xml.xpath('/CheckLogin/LastName/text()')[0] if xml.xpath('/CheckLogin/LastName/text()') else ''
        customer_details['email'] = xml.xpath('/CheckLogin/Email/text()')[0] if xml.xpath('/CheckLogin/Email/text()') else ''
        customer_details['mobile'] = xml.xpath('/CheckLogin/MobileNo/text()')[0] if xml.xpath('/CheckLogin/MobileNo/text()') else ''
        customer_details['birth_date'] = parse_date_to_string(parse_string_to_date(birth_date, fdate='%m/%d/%Y'), fdate='%Y-%m-%d')
        customer_details['gender'] = xml.xpath('/CheckLogin/Gender/text()')[0] if xml.xpath('/CheckLogin/Gender/text()') else ''
        customer_details['province'] = xml.xpath('/CheckLogin/Province/text()')[0] if xml.xpath('/CheckLogin/Province/text()') else ''
        customer_details['city'] = xml.xpath('/CheckLogin/City/text()')[0] if xml.xpath('/CheckLogin/City/text()') else ''
        customer_details['street'] = xml.xpath('/CheckLogin/Street/text()')[0] if xml.xpath('/CheckLogin/Street/text()') else ''
        customer_details['status'] = xml.xpath('/CheckLogin/CustStatus/text()')[0] if xml.xpath('/CheckLogin/CustStatus/text()') else ''
        customer_details['default_theater1'] = encoded_theater_id(default_theater1.key) if default_theater1 else ''
        customer_details['default_theater2'] = encoded_theater_id(default_theater2.key) if default_theater2 else ''
        customer_details['default_theater3'] = encoded_theater_id(default_theater3.key) if default_theater3 else ''
        customer_details['mpass_card'] = xml.xpath('/CheckLogin/MPassCard/text()')[0] if xml.xpath('/CheckLogin/MPassCard/text()') else ''
        customer_details['mpass_balance'] = xml.xpath('/CheckLogin/MPassBal/text()')[0] if xml.xpath('/CheckLogin/MPassBal/text()') else ''
    except (etree.XMLSyntaxError, urlfetch.ResponseTooLargeError) as e:
        log.warn("ERROR!, login, etree.XMLSyntaxError or urlfetch.ResponseTooLargeError...")
        log.error(e)
        raise SureSeatsLoginFailedException('Sorry, failed to log-in. Please try again.')
    return customer_details

def forgot_password(email):
    def __send_sureseats_req(s, req):
        log.info("######################### START ############################")
        log.info(req.method)
        log.info(req.url)
        log.info("########################## END #############################")
        response = s.send(req, timeout=TIMEOUT_DEADLINE)
        return response

    try:
        payload = {'action': 'FORGOT', 'email': email}

        s = requests.Session()
        req = create_sureseats_request(SURESEATS_BASE_URL, payload)
        result = __send_sureseats_req(s, req)

        if result.status_code != 200:
            log.warn("ERROR!, forgot_password, SureSeats forgot password failed, status_code: %s..." % result.status_code)

            raise SureSeatsForgotPasswordFailedException('Sorry, failed to forgot password. Please try again.')

        log.info("######################### START ############################")
        log.info(result.text)
        log.info("########################## END #############################")

        xml = etree.fromstring(str(result.text))
        status = xml.xpath('/ForgotPassword/Status/text()')[0] if xml.xpath('/ForgotPassword/Status/text()') else ''

        if status != 'SUCCESS':
            log.warn("ERROR!, login, SureSeats forgot password failed, status: %s..." % status)

            raise SureSeatsForgotPasswordFailedException(status)

        customer_id = xml.xpath('/ForgotPassword/CustID/text()')[0] if xml.xpath('/ForgotPassword/CustID/text()') else ''
        username = xml.xpath('/ForgotPassword/Username/text()')[0] if xml.xpath('/ForgotPassword/Username/text()') else ''
        password = xml.xpath('/ForgotPassword/Password/text()')[0] if xml.xpath('/ForgotPassword/Password/text()') else ''
        birth_date = xml.xpath('/ForgotPassword/BirthDay/text()')[0] if xml.xpath('/ForgotPassword/BirthDay/text()') else ''
        default_theater1 = get_theater_by_name(xml.xpath('/ForgotPassword/DefTheater1/text()')[0]) if xml.xpath('/ForgotPassword/DefTheater1/text()') else None
        default_theater2 = get_theater_by_name(xml.xpath('/ForgotPassword/DefTheater2/text()')[0]) if xml.xpath('/ForgotPassword/DefTheater2/text()') else None
        default_theater3 = get_theater_by_name(xml.xpath('/ForgotPassword/DefTheater3/text()')[0]) if xml.xpath('/ForgotPassword/DefTheater3/text()') else None

        customer_details = {}
        customer_details['customer_id'] = customer_id
        customer_details['username'] = username
        customer_details['password'] = encrypt_password(password)
        customer_details['first_name'] = xml.xpath('/ForgotPassword/FirstName/text()')[0] if xml.xpath('/ForgotPassword/FirstName/text()') else ''
        customer_details['middle_name'] = xml.xpath('/ForgotPassword/MiddleName/text()')[0] if xml.xpath('/ForgotPassword/MiddleName/text()') else ''
        customer_details['last_name'] = xml.xpath('/ForgotPassword/LastName/text()')[0] if xml.xpath('/ForgotPassword/LastName/text()') else ''
        customer_details['email'] = xml.xpath('/ForgotPassword/Email/text()')[0] if xml.xpath('/ForgotPassword/Email/text()') else ''
        customer_details['mobile'] = xml.xpath('/ForgotPassword/MobileNo/text()')[0] if xml.xpath('/ForgotPassword/MobileNo/text()') else ''
        customer_details['birth_date'] = parse_date_to_string(parse_string_to_date(birth_date, fdate='%m/%d/%Y'), fdate='%Y-%m-%d')
        customer_details['gender'] = xml.xpath('/ForgotPassword/Gender/text()')[0] if xml.xpath('/ForgotPassword/Gender/text()') else ''
        customer_details['province'] = xml.xpath('/ForgotPassword/Province/text()')[0] if xml.xpath('/ForgotPassword/Province/text()') else ''
        customer_details['city'] = xml.xpath('/ForgotPassword/City/text()')[0] if xml.xpath('/ForgotPassword/City/text()') else ''
        customer_details['street'] = xml.xpath('/ForgotPassword/Street/text()')[0] if xml.xpath('/ForgotPassword/Street/text()') else ''
        customer_details['status'] = xml.xpath('/ForgotPassword/CustStatus/text()')[0] if xml.xpath('/ForgotPassword/CustStatus/text()') else ''
        customer_details['default_theater1'] = encoded_theater_id(default_theater1.key) if default_theater1 else ''
        customer_details['default_theater2'] = encoded_theater_id(default_theater2.key) if default_theater2 else ''
        customer_details['default_theater3'] = encoded_theater_id(default_theater3.key) if default_theater3 else ''
        customer_details['mpass_card'] = xml.xpath('/ForgotPassword/MPassCard/text()')[0] if xml.xpath('/ForgotPassword/MPassCard/text()') else ''
        customer_details['mpass_balance'] = xml.xpath('/ForgotPassword/MPassBal/text()')[0] if xml.xpath('/ForgotPassword/MPassBal/text()') else ''
    except (etree.XMLSyntaxError, urlfetch.ResponseTooLargeError) as e:
        log.warn("ERROR!, forgot_password, etree.XMLSyntaxError or urlfetch.ResponseTooLargeError...")
        log.error(e)

        raise SureSeatsForgotPasswordFailedException('Sorry, failed to forgot password. Please try again.')

    return customer_details


def cancel_transaction(customer_id, customer_type, claim_code):
    cancel_details = {}

    def __send_sureseats_req(s, req):
        log.info("######################### START ############################")
        log.info(req.method)
        log.info(req.url)
        log.info("########################## END #############################")
        response = s.send(req, timeout=TIMEOUT_DEADLINE)
        return response

    try:
        payload = {'action': 'CANCEL', 'cust_id': customer_id, 'cust_type': customer_type, 'claim_code': claim_code}

        s = requests.Session()
        req = create_sureseats_request(SURESEATS_BASE_URL, payload)
        result = __send_sureseats_req(s, req)

        if result.status_code != 200:
            log.warn("ERROR!, cancel_transaction, SureSeats cancel transaction failed, status_code: %s..." % result.status_code)
            raise SureSeatsCancelTransactionFailedException('Sorry, failed to cancel transaction. Please try again.')

        log.info("######################### START ############################")
        log.info(result.text)
        log.info("########################## END #############################")

        xml = etree.fromstring(str(result.text))
        status = xml.xpath('/Cancel/Transaction/Status/text()')[0]

        if 'has been cancelled' not in status:
            log.warn("ERROR!, cancel_transaction, SureSeats cancel transaction failed, status: %s..." % status)

            if status == 'Error processing transaction. Try again later.404':
                status = 'Error processing transaction. Please try again.'

            raise SureSeatsCancelTransactionFailedException(status)

        cancel_details['claim_code'] = claim_code
        cancel_details['message'] = status
        cancel_transaction_by_ticketcode(claim_code)
    except (etree.XMLSyntaxError, urlfetch.ResponseTooLargeError) as e:
        log.warn("ERROR!, cancel_transaction, etree.XMLSyntaxError or urlfetch.ResponseTooLargeError...")
        log.error(e)
        raise SureSeatsCancelTransactionFailedException('Sorry, failed to cancel transaction. Please try again.')
    log.info('cancel_details: {}'.format(cancel_details))
    return cancel_details


def mpass_loading(customer_id, load_code):
    reload_details = {}

    def __send_sureseats_req(s, req):
        log.info("######################### START ############################")
        log.info(req.method)
        log.info(req.url)
        log.info("########################## END #############################")

        response = s.send(req, timeout=TIMEOUT_DEADLINE)

        return response

    try:
        payload = {'action': 'RELOAD', 'cust_id': customer_id, 'load_code': load_code}

        s = requests.Session()
        req = create_sureseats_request(SURESEATS_BASE_URL, payload)
        result = __send_sureseats_req(s, req)

        if result.status_code != 200:
            log.warn("ERROR!, mpass_loading, SureSeats MPass loading failed, status_code: %s..." % result.status_code)

            raise SureSeatsMPassLoadingFailedException('Sorry, failed to reload your M-Pass. Please try again.')

        log.info("######################### START ############################")
        log.info(result.text)
        log.info("########################## END #############################")

        xml = etree.fromstring(str(result.text))
        status = xml.xpath('/Reload/Status/text()')[0]

        if 'Loading Successful' not in status:
            log.warn("ERROR!, mpass_loading, SureSeats MPass loading failed, status: %s..." % status)

            raise SureSeatsMPassLoadingFailedException(status)

        reload_details['load_code'] = load_code
        reload_details['mpass_card'] = ''
        reload_details['mpass_balance'] = ''
        reload_details['message'] = status
    except (etree.XMLSyntaxError, urlfetch.ResponseTooLargeError) as e:
        log.warn("ERROR!, mpass_loading, etree.XMLSyntaxError or urlfetch.ResponseTooLargeError...")
        log.error(e)

        raise SureSeatsMPassLoadingFailedException('Sorry, failed to reload your MPass. Please try again.')

    return reload_details

def mpass_balance(customer_id, mpass_card):
    balance_details = {}

    def __send_sureseats_req(s, req):
        log.info("######################### START ############################")
        log.info(req.method)
        log.info(req.url)
        log.info("########################## END #############################")

        response = s.send(req, timeout=TIMEOUT_DEADLINE)

        return response

    try:
        payload = {'action': 'MPASS_BALANCE', 'cust_id': customer_id, 'mpass_card': mpass_card}

        s = requests.Session()
        req = create_sureseats_request(SURESEATS_BASE_URL, payload)
        result = __send_sureseats_req(s, req)

        if result.status_code != 200:
            log.warn("ERROR!, mpass_balance, SureSeats MPass balance failed, status_code: %s..." % result.status_code)

            raise SureSeatsMPassBalanceFailedException('Sorry, failed to reload your M-Pass. Please try again.')

        log.info("######################### START ############################")
        log.info(result.text)
        log.info("########################## END #############################")

        xml = etree.fromstring(str(result.text))
        status = xml.xpath('/MPassBalance/Status/text()')[0]

        if status != 'SUCCESS':
            log.warn("ERROR!, mpass_balance, SureSeats MPass balance failed, status: %s..." % status)

            raise SureSeatsMPassBalanceFailedException(status)

        balance_details['mpass_card'] = mpass_card
        balance_details['mpass_balance'] = xml.xpath('/MPassBalance/MPassBal/text()')[0] if xml.xpath('/MPassBalance/MPassBal/text()') else ''
        balance_details['message'] = status
    except (etree.XMLSyntaxError, urlfetch.ResponseTooLargeError) as e:
        log.warn("ERROR!, mpass_balance, etree.XMLSyntaxError or urlfetch.ResponseTooLargeError...")
        log.error(e)

        raise SureSeatsMPassBalanceFailedException('Sorry, failed to check your MPass balance. Please try again.')

    return balance_details
