import logging
import requests

from lxml import etree

from google.appengine.api import urlfetch

from gsureseats import orgs
from gsureseats.exceptions import BadValueException
from gsureseats.settings import SURESEATS_BASE_URL


log = logging.getLogger(__name__)

TIMEOUT_DEADLINE = 45

urlfetch.set_default_fetch_deadline(TIMEOUT_DEADLINE)


def sureseats_get_available_seats(theater_code, schedule_id):
    log.debug("sureseats_get_available_seats, fetching available seats.")

    remaining_seats = 0

    try:
        AVAILABLE_SEATS_PARAMETERS = '?action=AVAIL_SEAT&code=%s&id=%s' % (theater_code, schedule_id)
        AVAILABLE_SEATS_URL = SURESEATS_BASE_URL + AVAILABLE_SEATS_PARAMETERS
        result = urlfetch.fetch(AVAILABLE_SEATS_URL, deadline=TIMEOUT_DEADLINE)

        if result.status_code != 200:
            return 'error', [], remaining_seats

        data = str(result.content)

        if not data.strip():
            return 'error', [], remaining_seats

        xml = etree.fromstring(data)
        avail_raw = xml.xpath('/Cinema/Seatmap/available/text()')

        if avail_raw:
            avail_raw = avail_raw[0].split(';')
            remaining_seats = len(avail_raw[:-1])

            return 'success', avail_raw[:-1], remaining_seats
        else:
            return 'success', [], remaining_seats
    except etree.XMLSyntaxError as e:
        log.warn("ERROR!, sureseats_get_available_seats, XMLSyntaxError...")
        log.error(e)
    except Exception as e:
        log.warn("ERROR!, sureseats_get_available_seats, unexpected error...")
        log.error(e)

    return 'error', [], remaining_seats

AVAILABLE_SEATS_FEEDMAP = {str(orgs.AYALA_MALLS): sureseats_get_available_seats}

def get_available_seats(org_id, arg1, arg2):
    if org_id not in AVAILABLE_SEATS_FEEDMAP:
        raise BadValueException('theater', 'Theater Organization does not support querying available seats')

    return AVAILABLE_SEATS_FEEDMAP[org_id](arg1, arg2)
