import pprint
import logging


from flask import request

from google.appengine.api import mail
from google.appengine.api.app_identity import get_application_id
from google.appengine.ext import deferred

from gsureseats.util.logging import get_current_context_log
from gsureseats.settings import NOTIFICATION_ADDRESSES


log = logging.getLogger(__name__)
MAIL_FROM_TEMPLATE = "%%s@%s.appspotmail.com" % get_application_id()


def format_value_list(val_list):
    if len(val_list) > 1:
        val_list_quot = ["'%s'" % v for v in val_list]

        return "[ %s ]" % ','.join(val_list_quot)
    else:
        return "'%s'" % val_list[0]

def format_request_dict(values):
     kv_format = "           \t '%s' : %s"
     kv_pairs_str = [kv_format % (k, format_value_list(values.getlist(k))) for k in values.keys()]

     return '\n'.join(kv_pairs_str)

def _get_tb_frames(tb):
    frames = []

    while tb is not None:
        filename = tb.tb_frame.f_code.co_filename
        function = tb.tb_frame.f_code.co_name
        lineno = tb.tb_lineno - 1
        loader = tb.tb_frame.f_globals.get('__loader__')
        module_name = tb.tb_frame.f_globals.get('__name__') or ''
        frames.append({
            'tb': tb,
            'type': 'gsureseats' if module_name.startswith('gsureseats.') else 'sys',
            'filename': filename,
            'function': function,
            'lineno': lineno + 1,
            'vars': tb.tb_frame.f_locals.items(),
            'id': id(tb),
        })
        tb = tb.tb_next

    return frames

def _format_tb_frame(frame):
    FRAME_FORMAT = "{id}\t{type}\t{filename}:{lineno} {function}\n{vars_str}\n\n"
    VAR_FORMAT = " {name: >15} : {value}"
    vars_str_list = [VAR_FORMAT.format(name=name, value=pprint.saferepr(value)) for name,value in frame['vars']]
    vars_str = '\n'.join(vars_str_list)

    return FRAME_FORMAT.format(vars_str=vars_str, **frame)


EXCEPTION_REPORT_TEXT = """
Exception report:

Request   :
  Method  :\t {req_method}
  Path    :\t {req_path}
  Headers :
{req_headers}
  Query   :
{req_query}

User-Agent:\t {req_ua}

Client    :\t {client_id}
Device    :\t {device_id}



Exception occured during request: {e_val}

Log
==================================================
{log_messages}
==================================================

See attached dump.
"""

SCHEDULE_HEURISTICS_REPORT_TEXT = """
Schedules updated.

New schedules added:                    {new_schedules}
Existing schedules
    From cache:                         {existing_from_cache}
    From datastore:                     {existing_from_datastore}
    From datastore with variants:       {existing_from_datastore_variants}
Cache matched
    by code:                            {cached_by_code}
Datastore matched
    by code:                            {fetch_by_code}
    with variants:                      {fetch_variants}
    without variants:                   {fetch_no_variants}
Too many matches:                       {multi_errors}
"""

MOVIE_HEURISTICS_REPORT_TEXT = """
Movies updated.

New movies added:                       {new_movies}
Existing movies
    From cache:                         {existing_from_cache}
    From datastore:                     {existing_from_datastore}
Datastore entries
    correlated by title:                {correlated_by_title}
    correlated by TheaterOrg/movie ID:  {correlated_by_id}
Cache matched
    by movie ID:                        {cached_by_id}
    by title:                           {cached_by_title}
Feed does not allow creation:           {movies_not_created}
Multiple correlations error:            {multi_correlations}

Movies Added:
{movies_added}

No Correlation. Failed to Add Movies:
{movies_not_match_by_correlation_id_or_title}

FIXME: Multiple Correlation. Failed to Add Movies:
{movies_with_multiple_correlation}
"""

MOVIES_MULTIPLE_CORRELATION_REPORT_TEXT = """
FIXME: Multiple Correlation. Failed to Add Movies
{movies_with_multiple_correlation}
"""


def notify_admin(route_class, e, e_bundle):
    e_typ, e_val, e_trace = e_bundle

    frames = _get_tb_frames(e_trace)
    format_trace = ''.join([ _format_tb_frame(fr) for fr in frames ])
    log_dump = get_current_context_log()

    log.info('test_logger: {}'.format(log_dump))

    req_method = request.method
    req_path = request.script_root + request.path
    req_headers = format_request_dict(request.headers)
    req_query = format_request_dict(request.values)

    client_id = '(none)'
    device_id = '(none)'

    if 'X-Globe-SureSeats-Auth-ClientId' in request.headers:
        client_id = request.headers['X-Globe-SureSeats-Auth-ClientId']

    if 'X-Globe-SureSeats-DeviceId' in request.headers:
        device_id = request.headers['X-Globe-SureSeats-DeviceId']

    ua = request.headers['User-Agent']

    email = mail.EmailMessage()
    email.sender = MAIL_FROM_TEMPLATE % 'errors-noreply'
    email.to = NOTIFICATION_ADDRESSES

    email.body = EXCEPTION_REPORT_TEXT.format(req_method=req_method, req_path=req_path,
            req_headers=req_headers, req_query=req_query, client_id=client_id,
            device_id=device_id, req_ua=ua, log_messages=log_dump, e_val=e_val)

    '''
    email.body = EXCEPTION_REPORT_TEXT.format(req_method=req_method, req_path=req_path,
                                              req_headers=req_headers, req_query=req_query, client_id=client_id,
                                              device_id=device_id, log_messages=log_dump, e_val=e_val)
    '''
    email.subject = "Exception in %s: %s" % (route_class, e_typ.__name__)
    email.attachments = [("dump.txt", format_trace)]

    try:
        email.send()
    except:
        pass

def schedule_heuristics_notification(**kwargs):
    email = mail.EmailMessage()
    email.sender = MAIL_FROM_TEMPLATE % 'cron-noreply'
    email.to = NOTIFICATION_ADDRESSES
    email.body = SCHEDULE_HEURISTICS_REPORT_TEXT.format(**kwargs)
    email.subject = "Globe-SureSeats API: Updated Schedules"

    try:
        email.send()
    except:
        pass

def movie_heuristics_notification(**kwargs):
    email = mail.EmailMessage()
    email.sender = MAIL_FROM_TEMPLATE % 'cron-noreply'
    email.to = NOTIFICATION_ADDRESSES
    email.body = MOVIE_HEURISTICS_REPORT_TEXT.format(**kwargs)
    email.subject = "Globe-SureSeats API: Updated Movies"

    try:
        email.send()
    except:
        pass

def movie_multiple_correlation_notification(**kwargs):
    email = mail.EmailMessage()
    email.sender = MAIL_FROM_TEMPLATE % 'cron-noreply'
    email.to = NOTIFICATION_ADDRESSES
    email.body = MOVIES_MULTIPLE_CORRELATION_REPORT_TEXT.format(**kwargs)
    email.subject = "Globe-SureSeats API: Multiple Correlation Movies"

    try:
        email.send()
    except:
        pass
