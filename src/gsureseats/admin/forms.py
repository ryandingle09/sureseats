import datetime

from flask.ext.wtf import Form

from wtforms import (widgets, BooleanField, TextField, DecimalField, HiddenField, IntegerField, SelectMultipleField,
        DateField, TextAreaField, DateTimeField, ValidationError, SelectField, FieldList, FileField, FloatField, Field)
from wtforms.ext.appengine.ndb import model_form
from wtforms.validators import Required, Optional

from gsureseats import models
from gsureseats.settings import SystemSettings
from gsureseats.util.admin import convert_string_to_time


MTRCB_ADVISORY_RATINGS_CHOICES = ['MTRCB rating not yet available', 'G', 'PG', 'R-13', 'R-16', 'R-18']
ADDRESS_CATEGORY_CHOICES = ['Metro Manila', 'Metro Cebu', 'Metro Davao', 'Provincial']
PAYMENT_OPTIONS_CHOICES = [('pesopay','PesoPay'), ('mpass','MPass'), ('gcash', 'GCash')]
TICKET_TYPE_CHOICES = [('barcode', 'Barcode'), ('qrcode', 'QRCode')]
#SNACKBAR_TYPE_CHOICES = [('fixed', 'Fixed'), ('percentage', 'Percentage')]
MOVIE_FIELDS = [('movie_title', 'Movie Title'), ('cast', 'Cast'), ('genre', 'Genre'), ('variant', 'Variant'),
        ('runtime_mins', 'Runtime'), ('advisory_rating', 'Advisory Rating/Classification'), ('ratings', 'Ratings'),
        ('release_date', 'Release Date'), ('synopsis', 'Synopsis'), ('image_url', 'Image URL')]


def whitelist_blacklist_widget(field, **kwargs):
    html = ['<label class="checkbox">%s%s</label>' % (subfield(**kwargs), subfield.label.text) for subfield in field]

    return widgets.HTMLString(''.join(html))

def payment_options_widget(field, **kwargs):
    html = ['<label class="checkbox">%s%s</label>' % (subfield(**kwargs), subfield.label.text) for subfield in field]

    return widgets.HTMLString(''.join(html))

def date_format_check(form, field):
    try:
        date_string = datetime.datetime.strptime(field.data, '%m/%d/%Y %H:%M')
    except ValueError:
        raise ValidationError('Date format should be month/date/year hours:minutes (24-hour style)')

def time_format_check(form, field):
    try:
        date_string = datetime.datetime.strptime(field.data, '%H:%M')
    except ValueError:
        raise ValidationError('Time format should be hours:minutes (24-hour style)')

def check_white_list(form, field):
    whitelist_data = field.data

    for data in whitelist_data:
        if data in form.movie_fields_blacklist.data:
            raise ValidationError('Fields in whitelist should not be in the blacklist')

def check_black_list(form, field):
    blacklist_data = field.data

    for data in blacklist_data:
        if data in form.movie_fields_whitelist.data:
            raise ValidationError('Fields in blacklist should not be in the whitelist')


class TimeField(Field):
    widget = widgets.TextInput()

    def _value(self):
        if self.data:
            return self.data.strftime('%H:%M')
        else:
            return ''

    def process_formdata(self, valuelist):
        if valuelist:
            self.data = [datetime.datetime.strptime(v, '%H:%M').time() for v in valuelist]

            if len(self.data) == 1:
                self.data = self.data[0]
        else:
            self.data = []


class FeedForm(Form):
    name = TextField('name', validators=[Required()])
    image_density = SelectField('Image Density', choices=[('mdpi', 'MDPI'),('hdpi','HDPI'),('hidpi','HiDPI'),('xhdpi','XHDPI')])
    movie_fields_whitelist = SelectMultipleField('Whitelist', option_widget=widgets.CheckboxInput(), widget=whitelist_blacklist_widget,
            choices=MOVIE_FIELDS, validators=[check_white_list])
    movie_fields_blacklist = SelectMultipleField('Blacklist', option_widget=widgets.CheckboxInput(), widget=whitelist_blacklist_widget,
            choices=MOVIE_FIELDS, validators=[check_black_list])
    allow_movie_creation = BooleanField('Allow Movie Creation')
    heuristics_priority = IntegerField('Heuristics Priority')


class FileUploadForm(Form):
    file_uploaded = FileField('File')


class EditReservationForm(Form):
    theater = SelectField('Theaters')
    schedule = SelectField('Schedule', coerce=int)
    time = SelectField('Start Times', coerce=convert_string_to_time)
    cinema = TextField('Cinema')
    seat_id = TextField('Seat ID', validators=[Required()])
    reservation_index = HiddenField('reservation_index')


class PesoPayPaymentSettingsForm(Form):
    theater = SelectField('Theater', validators=[Required()], description='Theater associated with payment setting. Note: This cannot be changed once created to prevent duplicates.')
    payment_identifier = TextField('Payment Indentifier', default='pesopay', validators=[Required()], description='Default value is pesopay')
    currency_code = TextField('Currency Code', default='608', validators=[Required()], description='Default value is 608')
    pay_type = TextField('Payment Type', default='N', validators=[Required()], description='Default value is N')
    merchand_id = TextField('Merchant ID', validators=[Required()])
    hash_secret = TextField('Hash Secret', validators=[Required()])
    default_form_target = TextField('Default Form Target', default='https://www.pesopay.com/b2c2/eng/payment/payForm.jsp',
            validators=[Required()], description='Default value is https://www.pesopay.com/b2c2/eng/payment/payForm.jsp')


class PesoPayPaymentSettingsEditForm(Form):
    payment_identifier = TextField('Payment Indentifier', default='pesopay', validators=[Required()], description='Default value is pesopay')
    currency_code = TextField('Currency Code', default='608', validators=[Required()], description='Default value is 608')
    pay_type = TextField('Payment Type', default='N', validators=[Required()], description='Default value is N')
    merchand_id = TextField('Merchant ID', validators=[Required()])
    hash_secret = TextField('Hash Secret', validators=[Required()])
    default_form_target = TextField('Default Form Target', default='https://www.pesopay.com/b2c2/eng/payment/payForm.jsp',
            validators=[Required()], description='Default value is https://www.pesopay.com/b2c2/eng/payment/payForm.jsp')


class PesoPayPaymentSettingsCinemaForm(Form):
    cinema = TextField('Cinema', validators=[Required()])
    currency_code = TextField('Currency Code', default='608', validators=[Required()], description='Default value is 608')
    pay_type = TextField('Payment Type', default='N', validators=[Required()], description='Default value is N')
    merchand_id = TextField('Merchant ID', validators=[Required()])
    hash_secret = TextField('Hash Secret', validators=[Required()])
    default_form_target = TextField('Default Form Target', default='https://www.pesopay.com/b2c2/eng/payment/payForm.jsp',
            validators=[Required()], description='Default value is https://www.pesopay.com/b2c2/eng/payment/payForm.jsp')


TheaterOrgForm = model_form(models.TheaterOrganization, base_class=Form, exclude=['convenience_fees',
                                                                                  'template',
                                                                                  'snackbar_fees',
                                                                                  'snackbar_fees_type',
                                                                                  'template_image',
                                                                                  'ticket_remarks',
                                                                                  'ticket_type'])

TheaterOrgForm.convenience_fee_creditcard = TextField('Credit-Card')
TheaterOrgForm.convenience_fee_mpass = TextField('M-Pass')
TheaterOrgForm.convenience_fee_reserve = TextField('Reserve')
#TheaterOrgForm.convenience_fee_snackbar_type = SelectField('Add-on Type', choices=SNACKBAR_TYPE_CHOICES)
#TheaterOrgForm.convenience_fee_snackbar = TextField('Add-on Fee')
TheaterOrgForm.ticket_type = SelectField('Ticket Type', choices=TICKET_TYPE_CHOICES)

TheaterForm = model_form(models.Theater, base_class=Form, exclude=['address_category', 'location', 'payment_options'])
TheaterForm.address_category = SelectField('Address Category', choices=zip(ADDRESS_CATEGORY_CHOICES, ADDRESS_CATEGORY_CHOICES))
TheaterForm.longitude = DecimalField('Longitude', places=7, validators=[Optional()])
TheaterForm.latitude = DecimalField('Latitude', places=7, validators=[Optional()])
TheaterForm.payment_options = SelectMultipleField('Payment Options', option_widget=widgets.CheckboxInput(),
        widget=payment_options_widget, choices=PAYMENT_OPTIONS_CHOICES)

CinemaForm = model_form(models.Cinema, base_class=Form, exclude=['location', 'seat_map'])
CinemaForm.longitude = DecimalField('Longitude', places=7, validators=[Optional()])
CinemaForm.latitude = DecimalField('Latitude', places=7, validators=[Optional()])
CinemaForm.seat_map = SelectMultipleField('Seat Rows',option_widget=widgets.CheckboxInput(),widget=widgets.ListWidget(prefix_label=False))
CinemaForm.addseats = TextField('Additional Seat Row')

MovieForm = model_form(models.Movie, base_class=Form, exclude=['cast', 'advisory_rating', 'is_showing',
        'ratings', 'release_date', 'runtime_mins', 'slottime_mins'])
MovieForm.cast_list = TextField('Cast')
MovieForm.advisory_rating = SelectField('Advisory Rating/Classification', choices=zip(MTRCB_ADVISORY_RATINGS_CHOICES, MTRCB_ADVISORY_RATINGS_CHOICES))
MovieForm.release_date = DateField('Release Date', format='%m/%d/%Y')
MovieForm.runtime_mins = FloatField('Runtime', validators=[Optional()])

ScheduleForm = model_form(models.Schedule, base_class=Form, exclude=['cinema', 'show_date'])
ScheduleForm.cinema = SelectField('Cinema')
ScheduleForm.show_date = DateField('Show Date', format='%m/%d/%Y')

ScheduleSlotForm = model_form(models.ScheduleTimeSlot, base_class=Form, exclude=['feed_code', 'start_time', 'movie', 'price'])
ScheduleSlotForm.start_time = TimeField('Start Time')
ScheduleSlotForm.movie_title = SelectField('Movie')
ScheduleSlotForm.price = DecimalField('Price')

ClientForm = model_form(models.Client, base_class=Form)
ClientForm.id = TextField('ID', validators=[Required()])

ClientEditForm = model_form(models.Client, base_class=Form)

SystemSettingsForm = model_form(SystemSettings, base_class=Form)
